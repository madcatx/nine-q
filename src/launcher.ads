with Ada.Interrupts;
with Ada.Interrupts.Names;
with AWS.Config;
with AWS.Server;

package Launcher is
  pragma Unreserve_All_Interrupts;

  procedure Launch;

private
  Server_Config: AWS.Config.Object;
  Web_Server: AWS.Server.HTTP;

  protected Signal_Handlers is
    procedure Shutdown_On_Signal;

    pragma Attach_Handler(Shutdown_On_Signal, Ada.Interrupts.Names.SIGINT);
  end Signal_Handlers;

end Launcher;


