with Ada.Exceptions;
with Ada.Strings.Fixed;
with Ada.Text_IO;
with Ada.Unchecked_Deallocation;
with Face_Generator;
with Logging_System;

package body Problem_Manager is

  function Display_Checked_Answer(UID: in Unique_ID; Answer: in Problem_Generator_Syswides.Answer_Info.Map; HTML: out HTML_Code; Pr_ID: in Problem_ID) return RetCode is
    Answer_Message: UB_Text;
    ARC: Problem_Generator_Syswides.Answer_RetCode;
    Assignment: Problem_Generator_Syswides.Assignment_Info.Map;
    FillIns: Problem_Generator_Syswides.Fillins_Map.Map;
    Parameters: Problem_Generator_Syswides.Parameters_Info.Map;
    Pr_Cat: Problem_Category;
    Ret: RetCode;
    Stored: Stored_Problem_All_Access;
  begin
    Stored := Active_Sessions.Get_Problem(UID, Pr_ID);
    if Stored = null then
      return E_NOTFOUND;
    end if;

    Pr_Cat := Stored.Category;
    Ret := Stored.Problem.Get_Parameters(Parameters);
    if Ret /= OK then
      Stored.Mutex.Unlock;
      return Face_Generator.Generate_Error_Face(HTML, ERRMSG_GET_PARAMETERS & " (" & RetCode'Image(Ret) & ")");
    end if;

    begin
      Ret := Stored.Problem.Get_Assignment(Assignment);
      if Ret /= OK then
	Logging_System.Log(ERRMSG_GET_ASSIGNMENT & " (" & RetCode'Image(Ret) & ")", Logging_System.ERROR);
	Stored.Mutex.Unlock;
	return Face_Generator.Generate_Error_Face(HTML, ERRMSG_GET_ASSIGNMENT & " (" & RetCode'Image(Ret) & ")");
      end if;
    exception
      when Ex: others =>
	Logging_System.Log(ERRMSG_UNHANDLED_EXCEPTION & " (" & Ada.Exceptions.Exception_Information(Ex) & ")", Logging_System.ERROR);
	Stored.Mutex.Unlock;
	return Face_Generator.Generate_Error_Face(HTML, ERRMSG_UNHANDLED_EXCEPTION & " (" & Ada.Exceptions.Exception_Information(Ex) & ")");
    end;

    begin
      ARC := Stored.Problem.Check_Answer(Answer, FillIns, Answer_Message);
    exception
      when Ex: others =>
	Logging_System.Log(ERRMSG_UNHANDLED_EXCEPTION & " (" & Ada.Exceptions.Exception_Information(Ex) & ")", Logging_System.ERROR);
	Stored.Mutex.Unlock;
	return Face_Generator.Generate_Error_Face(HTML, ERRMSG_UNHANDLED_EXCEPTION & " (" & Ada.Exceptions.Exception_Information(Ex) & ")");
    end;
    Ret := Face_Generator.Generate_Face_With_Answer(Assignment => Assignment, Answer_Message => Answer_Message,
						    Answer_Code => ARC, HTML => HTML,
						    Parameters => Parameters, Pr_ID => Problem_ID'Image(Pr_ID), Pr_Cat => Problem_Category'Image(Pr_Cat),
						    FillIns => FillIns);
    Stored.Mutex.Unlock;
    return Ret;
  end Display_Checked_Answer;

  function Display_Assignment(UID: in Unique_ID; HTML: out HTML_Code) return RetCode is
    Assignment: Problem_Generator_Syswides.Assignment_Info.Map;
    Parameters: Problem_Generator_Syswides.Parameters_Info.Map;
    Stored: Stored_Problem_All_Access;
    Pr_ID: Problem_ID;
    Pr_Cat: Problem_Category;
    Ret: RetCode;
  begin
    Stored := Active_Sessions.Get_Latest_Problem_With_ID(UID, Pr_ID);
    if Stored = null then
      return E_NOTFOUND;
    end if;

    Pr_Cat := Stored.Category;
    Ret := Stored.Problem.Get_Parameters(Parameters);
    if Ret /= OK then
      Stored.Mutex.Unlock;
      return Face_Generator.Generate_Error_Face(HTML, ERRMSG_GET_PARAMETERS & " (" & RetCode'Image(Ret) & ")");
    end if;

    -- Get assignment
    begin
      Ret := Stored.Problem.Get_Assignment(Assignment);
      if Ret /= OK then
	Logging_System.Log(ERRMSG_GET_ASSIGNMENT & " (" & RetCode'Image(Ret) & ")", Logging_System.ERROR);
	Stored.Mutex.Unlock;
	return Face_Generator.Generate_Error_Face(HTML, ERRMSG_GET_ASSIGNMENT & " (" & RetCode'Image(Ret) & ")");
      end if;
    exception
      when Ex: others =>
	Logging_System.Log(ERRMSG_UNHANDLED_EXCEPTION & " (" & Ada.Exceptions.Exception_Information(Ex) & ")", Logging_System.ERROR);
	Stored.Mutex.Unlock;
	return Face_Generator.Generate_Error_Face(HTML, ERRMSG_UNHANDLED_EXCEPTION & " (" & Ada.Exceptions.Exception_Information(Ex) & ")");
    end;

    Ret := Face_Generator.Generate_Face(Assignment => Assignment, HTML => HTML, Parameters => Parameters, Pr_ID => Problem_ID'Image(Pr_ID),
					Pr_Cat => Problem_Category'Image(Pr_Cat));
    Stored.Mutex.Unlock;
    return Ret;
  end Display_Assignment;

  function Display_Walkthrough(UID: in Unique_ID; HTML: out HTML_Code; Pr_ID: in Problem_ID) return RetCode is
    Assignment: Problem_Generator_Syswides.Assignment_Info.Map;
    Parameters: Problem_Generator_Syswides.Parameters_Info.Map;
    Walkthrough: Problem_Generator_Syswides.Walkthrough_Info.Map;
    Pr_Cat: Problem_Category;
    Ret: RetCode;
    Stored: Stored_Problem_All_Access;
  begin
    Stored := Active_Sessions.Get_Problem(UID, Pr_ID);
    if Stored = null then
      return E_NOTFOUND;
    end if;

    Pr_Cat := Stored.Category;
    Ret := Stored.Problem.Get_Parameters(Parameters);
    if Ret /= OK then
      Stored.Mutex.Unlock;
      return Face_Generator.Generate_Error_Face(HTML, ERRMSG_GET_PARAMETERS & " (" & RetCode'Image(Ret) & ")");
    end if;

    begin
      Ret := Stored.Problem.Get_Assignment(Assignment);
      if Ret /= OK then
	Logging_System.Log(ERRMSG_GET_ASSIGNMENT & " (" & RetCode'Image(Ret) & ")", Logging_System.ERROR);
	Stored.Mutex.Unlock;
	return Face_Generator.Generate_Error_Face(HTML, ERRMSG_GET_ASSIGNMENT & " (" & RetCode'Image(Ret) & ")");
      end if;
      Ret := Stored.Problem.Get_Walkthrough(Walkthrough);
      if Ret /= OK then
	Logging_System.Log(ERRMSG_GET_ASSIGNMENT & " (" & RetCode'Image(Ret) & ")", Logging_System.ERROR);
	Stored.Mutex.Unlock;
	return Face_Generator.Generate_Error_Face(HTML, ERRMSG_GET_ASSIGNMENT & " (" & RetCode'Image(Ret) & ")");
      end if;
    exception
      when Ex: others =>
	Logging_System.Log(ERRMSG_UNHANDLED_EXCEPTION & " (" & Ada.Exceptions.Exception_Information(Ex) & ")", Logging_System.ERROR);
	Stored.Mutex.Unlock;
	return Face_Generator.Generate_Error_Face(HTML, ERRMSG_UNHANDLED_EXCEPTION & " (" & Ada.Exceptions.Exception_Information(Ex) & ")");
    end;

    Ret := Face_Generator.Generate_Face_With_Walkthrough(Assignment => Assignment, HTML => HTML,
							 Parameters => Parameters, Pr_ID => Problem_ID'Image(Pr_ID), Pr_Cat => Problem_Category'Image(Pr_Cat),
							 Walkthrough => Walkthrough);
    Stored.Mutex.Unlock;
    return Ret;
  end Display_Walkthrough;

  function Get_UID(Raw_UID: in String; UID: out Unique_ID) return Boolean is
  begin
    begin
      UID := Unique_ID'Value(Raw_UID);
      return Active_Sessions.Contains(UID);
    exception
      when Constraint_Error =>
        return False;
    end;
  end Get_UID;

  function Prepare_Problem(UID: in Unique_ID; Raw_P_Cat: in String;
			   Parameters: in Problem_Generator_Syswides.Parameters_Info.Map := Problem_Generator_Syswides.Parameters_Info.Empty_Map) return RetCode is
    Problem: Chem_Problem_All_Access;
    Storage: Stored_Problem_All_Access;
    Pr_Cat: Problem_Category;
    Pr_ID: Problem_ID;
    Ret: RetCode;
  begin
    if Raw_P_Cat = Problem_Manager.Problem_Category'Image(Problem_Manager.Acidobazic) then
      Pr_Cat := Problem_Manager.Acidobazic;
    elsif Raw_P_Cat = Problem_Manager.Problem_Category'Image(Problem_Manager.Solubility) then
      Pr_Cat := Problem_Manager.Solubility;
    elsif Raw_P_Cat = Problem_Manager.Problem_Category'Image(Problem_Manager.Titration_Curve) then
      Pr_Cat := Problem_Manager.Titration_Curve;
    else
      return E_INVAL;
    end if;

    case Pr_Cat is
      when Acidobazic =>
	Problem := Problem_Generator.Get_Problem(Problem_Generator_Syswides.Acidobazic);
      when Solubility =>
	Problem := Problem_Generator.Get_Problem(Problem_Generator_Syswides.Solubility);
      when Titration_Curve =>
        Problem := Problem_Generator.Get_Problem(Problem_Generator_Syswides.Titration_Curve);
      when others =>
	return E_INVAL;
    end case;

    if Problem = null then
      return E_NULLPTR;
    end if;
    -- Initialize problem
    if Parameters.Is_Empty = False then
      Ret := Problem.Set_Parameters(Parameters);
      if Ret /= OK then
	Free_Chem_Problem(Problem);
	return Ret;
      end if;
    end if;

    begin
      Problem.New_Problem;
    exception
      when others =>
	Free_Chem_Problem(Problem);
	return E_UNKW;
    end;

    Storage := new Stored_Problem;
    Storage.Problem := Problem;
    Storage.Category := Pr_Cat;
    Active_Sessions.Add_Problem(UID, Storage, Pr_ID, Ret);

    if Ret /= OK then
      Free_Chem_Problem(Problem);
      Free_Stored_Problem(Storage);
    end if;

    Problem.Set_Resource_Prefix(Build_Resource_Prefix(UID, Pr_Cat, Pr_ID));
    return Ret;
  end Prepare_Problem;

  function Register_UID(UID: out Unique_ID) return RetCode is
    Ret: RetCode;
  begin
    Active_Sessions.Register_UID(UID, Ret);
    return Ret;
  end Register_UID;

  procedure Session_Expired(SID: in AWS.Session.ID) is
    Raw_UID: constant String := AWS.Session.Get(SID, "UID");
    UID: Unique_ID;
  begin
    if Get_UID(Raw_UID, UID) = False then
      return;
    end if;

    Active_Sessions.Remove_Session(UID);
  end Session_Expired;

  -- BEGIN: Private functions

  function Build_Resource_Prefix(UID: in Unique_ID; Pr_Cat: in Problem_Category; Pr_ID: in Problem_ID) return String is
  begin
    return "resources/resource_" & Ada.Strings.Fixed.Trim(Source => Unique_ID'Image(UID), Side => Ada.Strings.Left) & "_" & Ada.Strings.Fixed.Trim(Source => Problem_Category'Image(Pr_Cat), Side => Ada.Strings.Left) & "_" & Ada.Strings.Fixed.Trim(Source => Problem_ID'Image(Pr_ID), Side => Ada.Strings.Left) & "-";
  end Build_Resource_Prefix;

  procedure Free_Chem_Problem(Problem: in out Chem_Problem_All_Access) is
    procedure Free_Chem_Problem_Internal is new Ada.Unchecked_Deallocation(Object => Problem_Generator.Chem_Problem'Class, Name => Chem_Problem_All_Access);
  begin
    Free_Chem_Problem_Internal(Problem);
  end Free_Chem_Problem;

  procedure Free_Stored_Problem(Problem: in out Stored_Problem_All_Access) is
    procedure Free_Stored_Problem_Internal is new Ada.Unchecked_Deallocation(Object => Stored_Problem, Name => Stored_Problem_All_Access);
  begin
    Free_Stored_Problem_Internal(Problem);
  end Free_Stored_Problem;

  protected body Active_Sessions is

    procedure Add_Problem(UID: in Unique_ID; Problem: in Stored_Problem_All_Access; Pr_ID: out Problem_ID; Ret: out RetCode) is
      use Problem_Storage;
      use Session_Keeping;
      use Ada.Containers;

      C: Problem_Storage.Cursor;
      Success: Boolean;
      USD: User_Session_All_Access;
    begin
      if Sessions.Find(UID) = Session_Keeping.No_Element then
	Ret := E_NOTFOUND;
	return;
      end if;
      USD := Sessions.Element(UID);

      USD.Problems.Insert(USD.Last_Problem_ID, Problem, C, Success);
      if Success = False then
	Ret := E_NOMEM;
	return;
      end if;
      Pr_ID := USD.Last_Problem_ID;

      USD.Last_Problem_ID := USD.Last_Problem_ID + 1;
      -- Delete old problems
      declare
	Del_ID: Problem_ID;
      begin
	if USD.Last_Problem_ID - 1 < MAX_STORED_PROBLEMS then
	  Del_ID := Problem_ID'Last - (MAX_STORED_PROBLEMS - (USD.Last_Problem_ID - 1));
	else
	  Del_ID := (USD.Last_Problem_ID - 1) - MAX_STORED_PROBLEMS;
	end if;

	if USD.Problems.Find(Del_ID) /= Problem_Storage.No_Element then
	  declare
	    SP: Stored_Problem_All_Access := USD.Problems(Del_ID);
	  begin
	    -- Delete pointer to the problem, grab a lock to make sure that the pointer is not used
	    -- anywhere else while we are deleting it
	    SP.Mutex.Lock;
	    Free_Chem_Problem(SP.Problem);
	    SP.Mutex.Unlock;
	    Free_Stored_Problem(SP);
	    USD.Problems.Delete(Del_ID);
	  end;
	end if;
      end;
      Ret := OK;
    end Add_Problem;

    function Contains(UID: in Unique_ID) return Boolean is
      use Session_Keeping;
    begin
      if Sessions.Find(UID) = Session_Keeping.No_Element then
	return False;
      else
	return True;
      end if;
    end Contains;

    procedure Check_Free_And_Register(UID: in Unique_ID; Ret: out RetCode; Stop: out Boolean) is
      use Session_Keeping;

      C: Session_Keeping.Cursor;
      NUSD: User_Session_All_Access;
      Success: Boolean;
    begin
      if Sessions.Find(UID) = Session_Keeping.No_Element then
	-- We have a free slot
	NUSD := new User_Session_Data;
	if NUSD = null then
	  -- Session data not allocated
	  Ret := E_NOMEM;
	  Stop := True;
	  return;
	end if;
	Sessions.Insert(UID, NUSD, C, Success);
	if Success then
	  Last_UID := UID;
	  -- Registration successful
	  Ret := OK;
	  Stop := True;
	  return;
	else
	  -- Registration failed
	  Ret := E_NOMEM;
	  Stop := True;
	  return;
	end if;
      end if;
      -- Slot occupied, keep looking
      Ret := OK;
      Stop := False;
    end Check_Free_And_Register;

    procedure Free_Session_Data(Data: in out User_Session_All_Access) is
      procedure Free_Session_Data_Internal is new Ada.Unchecked_Deallocation(Object => User_Session_Data, Name => User_Session_All_Access);
    begin
      Free_Session_Data_Internal(Data);
    end Free_Session_Data;

    function Get_Problem(UID: in Unique_ID; Pr_ID: in Problem_ID) return Stored_Problem_All_Access is
      use Problem_Storage;
      use Session_Keeping;

      USD: User_Session_All_Access;
      S: Stored_Problem_All_Access;
    begin
      if Sessions.Find(UID) = Session_Keeping.No_Element then
	return null;
      end if;
      USD := Sessions.Element(UID);

      if USD.Problems.Find(Pr_ID) = Problem_Storage.No_Element then
	return null;
      end if;


      S := USD.Problems.Element(Pr_ID);
      -- Check if the problem has not been deleted
      if S = null then
	return null;
      end if;
      if S.Problem = null then
	return null;
      end if;

      S.Mutex.Lock;
      return S;
    end Get_Problem;

    function Get_Latest_Problem_With_ID(UID: in Unique_ID; Pr_ID: out Problem_ID) return Stored_Problem_All_Access is
      use Session_Keeping;

    begin
      if Sessions.Find(UID) = Session_Keeping.No_Element then
	return null;
      end if;

      Pr_ID := Sessions.Element(UID).Last_Problem_ID - 1;
      return Get_Problem(UID, Pr_ID);
    end Get_Latest_Problem_With_ID;

    procedure Register_UID(UID: out Unique_ID; Ret: out RetCode) is
      Stop: Boolean;
    begin
      -- Look for an available UID slot
      for Idx in Last_UID .. Unique_ID'Last loop
	Check_Free_And_Register(Idx, Ret, Stop);
        if Stop then
	  UID := Idx;
	  return;
	end if;
      end loop;

      -- We found no free slot above, search the area below Last_UID
      for Idx in Unique_ID'First .. Last_UID loop
	Check_Free_And_Register(Idx, Ret, Stop);
        if Stop then
	  UID := Idx;
	  return;
	end if;
      end loop;

      -- There are no free slots available
      Ret := E_NOMEM;
    end Register_UID;

    procedure Remove_Session(UID: in Unique_ID) is
      use Problem_Storage;
      use Session_Keeping;

      USD: User_Session_All_Access;
    begin
      if Sessions.Find(UID) = Session_Keeping.No_Element then
	return;
      end if;

      USD := Sessions.Element(UID);

      while USD.Problems.Is_Empty = False loop
	declare
	  SP: Stored_Problem_All_Access := USD.Problems.First_Element;
	begin
	  SP.Mutex.Lock;
	  Free_Chem_Problem(SP.Problem);
	  SP.Mutex.Unlock;
	  Free_Stored_Problem(SP);
	  USD.Problems.Delete_First;
	end;
      end loop;

      Free_Session_Data(USD);

      Sessions.Delete(UID);
    end Remove_Session;

  end Active_Sessions;

end Problem_Manager;
