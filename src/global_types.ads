with Ada.Containers;
with Ada.Strings.Unbounded;

package Global_Types is

  type Problem_ID is new Ada.Containers.Count_Type;
  type RetCode is (OK, E_NOTFOUND, E_UNKW, E_INVAL, E_NOMEM, E_NULLPTR, E_FAIL, E_NOTIMPL);
  type Unique_ID is new Ada.Containers.Count_Type;
  subtype HTML_Code is Ada.Strings.Unbounded.Unbounded_String;
  subtype UB_Text is Ada.Strings.Unbounded.Unbounded_String;

  procedure Append_HTML(Source: in out HTML_Code; New_Item: in HTML_Code);
  procedure Append_UB_Text(Source: in out UB_Text; New_Item: in UB_Text);
  procedure Append_UB_Text(Source: in out UB_Text; New_Item: in String);
  function HTML_To_Fixed_String(HTML: in HTML_Code) return String;
  function To_HTML_Code(S: in String) return HTML_Code;
  function To_UB_Text(S: in String) return UB_Text;
  function UB_Text_To_Fixed_String(Text: in UB_Text) return String;

  -- Overloaded operators
  function "&" (Left: in UB_Text; Right: in UB_Text) return UB_Text;

  protected type Simple_Mutex is
    entry Lock;
    entry Unlock;
  private
    Locked: Boolean := False;
  end Simple_Mutex;
  type Simple_Mutex_All_Access is access all Simple_Mutex;

end Global_Types;
