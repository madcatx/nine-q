with Ada.Text_IO;
with AWS.Config.Ini;

with Global_Types;
with Handlers;
with Logging_System;

use Global_Types;

package body Launcher is

  procedure Launch is
    RC: RetCode;
  begin
    RC := Logging_System.Initialize;
    if RC /= OK then
      Ada.Text_IO.Put_Line("Nine-Q will not start without a log file");
      return;
    end if;

    AWS.Config.Ini.Read(Server_Config, "nine_q_config.ini");

    AWS.Server.Start(Web_Server => Web_Server,
		     Dispatcher => Handlers.Get_Dispatchers,
		     Config => Server_Config);

    Ada.Text_IO.Put_Line("Nine-Q server started");
    Logging_System.Log("Nine-Q server started", Logging_System.DEBUG);

    AWS.Server.Wait(AWS.Server.No_Server);
    Logging_System.Log("Nine-Q server shut down", Logging_System.DEBUG);
    Ada.Text_IO.Put_Line("Nine-Q server shut down");

    Logging_System.Close;
  end Launch;

  protected body Signal_Handlers is

    procedure Shutdown_On_Signal is
    begin
      AWS.Server.Shutdown(Web_Server);
    end Shutdown_On_Signal;

  end Signal_Handlers;

end Launcher;
