package body Global_Types is

  procedure Append_HTML(Source: in out HTML_Code; New_Item: in HTML_Code) is
    use Ada.Strings.Unbounded;
  begin
    Append(Source => Source, New_Item => New_Item);
  end Append_HTML;

  procedure Append_UB_Text(Source: in out UB_Text; New_Item: in UB_Text) is
    use Ada.Strings.Unbounded;
  begin
    Append(Source => Source, New_Item => New_Item);
  end Append_UB_Text;

  procedure Append_UB_Text(Source: in out UB_Text; New_Item: in String) is
    use Ada.Strings.Unbounded;
  begin
    Append(Source => Source, New_Item => To_Unbounded_String(New_Item));
  end Append_UB_Text;

  function HTML_To_Fixed_String(HTML: in HTML_Code) return String is
    use Ada.Strings.Unbounded;
  begin
    return To_String(HTML);
  end HTML_To_Fixed_String;

  function To_HTML_Code(S: in String) return HTML_Code is
    use Ada.Strings.Unbounded;
  begin
    return To_Unbounded_String(S);
  end To_HTML_Code;

  function To_UB_Text(S: in String) return UB_Text is
    use Ada.Strings.Unbounded;
  begin
    return To_Unbounded_String(S);
  end To_UB_Text;

  function UB_Text_To_Fixed_String(Text: in UB_Text) return String is
    use Ada.Strings.Unbounded;
  begin
    return To_String(Text);
  end UB_Text_To_Fixed_String;

  -- Overloaded operators
  function "&" (Left: in UB_Text; Right: in UB_Text) return UB_Text is
    use Ada.Strings.Unbounded;

    T: UB_Text := Left;
  begin
    Append_UB_Text(Source => T, New_Item => Right);
    return T;
  end "&";

  protected body Simple_Mutex is
    entry Lock when Locked = False is
    begin
      Locked := True;
    end Lock; 

    entry Unlock when Locked is
    begin
      Locked := False;
    end Unlock;
  end Simple_Mutex;

end Global_Types;
