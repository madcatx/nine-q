with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Strings.Fixed;
--with Ada.Strings.Unbounded;
--with Ada.Text_IO;

package body Formatting_Helpers is
 
  function String_To_Float(S: in String) return FH_Float is
    Idx: Natural;
    SS: String := S;
  begin
    -- Replace "," with "." as decimal seaprator
    Idx := Ada.Strings.Fixed.Index(Source => SS, Pattern => ",", From => 1);
    if Idx > 0 then
      Ada.Strings.Fixed.Replace_Slice(Source => SS, Low => Idx, High => Idx, By => ".");
    end if;

    begin
      return FH_Float'Value(SS);
    exception
      when Constraint_Error =>
	raise Constraint_Error;
    end;
  end String_To_Float;
 
  function Round_To_Valid_Nums(Num: in FH_Float; Decimals: in Natural) return FH_Float is
    package FHEF is new Ada.Numerics.Generic_Elementary_Functions(FH_Float);
    use FHEF;

    ExpDecimals: constant FH_Float := 10.0 ** Decimals;
    Log_Arg_Floor: FH_Float;
    Temp: FH_Float;
  begin
    Log_Arg_Floor := FH_Float'Floor(Log(Base => 10.0, X => Num));
    Temp := Num / (10.0 ** Log_Arg_Floor);
    Temp := FH_Float'Rounding(Temp * ExpDecimals);
    return (Temp / ExpDecimals) * (10.0 ** Log_Arg_Floor);
  end Round_To_Valid_Nums;

  procedure Split_Integer_Decimal_Unscaled_Strs(Num: in FH_Float; Decimals: in Natural; Integer_Part: out UB_Text; Decimal_Part: out UB_Text) is
    Integer_Part_F: FH_Float;
    Decimal_Part_F: FH_Float;
    Decimal_Part_I: Integer;
    ExpDecimals: constant FH_Float := 10.0 ** Decimals;
    MNum: FH_Float := Num;
  begin
    declare
      EPSILON: constant FH_Float := 10.0 ** (-(Decimals + 1));
      FPE: constant FH_Float := FH_Float'Floor(MNum + EPSILON);
      F: constant FH_Float := FH_Float'Floor(MNum);
    begin
      if FPE > F then
	Integer_Part_F := FPE;
	MNum := MNum + EPSILON;
      else
	Integer_Part_F := F;
      end if;
    end;
    Decimal_Part_F := (MNum - Integer_Part_F) * ExpDecimals;
    Decimal_Part_I := Integer(Decimal_Part_F);

    Integer_Part := To_UB_Text(Ada.Strings.Fixed.Trim(Source => Integer'Image(Integer(Integer_Part_F)), Side => Ada.Strings.Left));
    Decimal_Part := To_UB_Text(Ada.Strings.Fixed.Trim(Source => Integer'Image(Decimal_Part_I), Side => Ada.Strings.Left));
    Prepend_Zeros_To_Text(Decimal_Part_F, Decimals, Decimal_Part);
  end Split_Integer_Decimal_Unscaled_Strs;

  procedure Split_Integer_Decimal_Exponent_Nums(Num: in FH_Float; Decimals: in Natural; Integer_Part: out Integer; Decimal_Part: out FH_Float; Exponent_Part: out Integer) is
    package FHEF is new Ada.Numerics.Generic_Elementary_Functions(FH_Float);
    use FHEF;

    ExpDecimals: constant FH_Float := 10.0 ** Decimals;
    PNum: FH_Float;
    Expanded: FH_Float;
    Integer_Part_F: FH_Float;
    Log_Arg_Floored: FH_Float;
    Negative: Boolean;
  begin
    if Num = 0.0 then 
      Integer_Part := 0;
      Decimal_Part := 0.0;
      Exponent_Part := 0;
      return;
    end if;

    if Num < 0.0 then
      Negative := True;
      PNum := Num * (-1.0);
    else
      PNum := Num;
      Negative := False;
    end if;

    Log_Arg_Floored := FH_Float'Floor(Log(Base => 10.0, X => Num));
    Expanded := 10.0 ** Log_Arg_Floored;
    Integer_Part_F := Get_Integer_Part(PNum, Decimals);
    --Integer_Part_F := FH_Float'Floor(PNum / Expanded);
    --Ada.Text_IO.Put_Line("((PNum / Expanded) - Integer_Part_F) * Expanded = " & FH_Float'Image(((PNum / Expanded) - Integer_Part_F) * Expanded));
    Decimal_Part := ((PNum / Expanded) - Integer_Part_F) * ExpDecimals;
    Decimal_Part := FH_Float'Rounding(Decimal_Part);

    Exponent_Part := Integer(Log_Arg_Floored);
    Integer_Part := Integer(Integer_Part_F);

    if Negative then
      Integer_Part := Integer_Part * (-1);
    end if;
    --Ada.Text_IO.Put_Line(FH_Float'Image(Num) & " --- " & Integer'Image(Integer_Part) & "," & FH_Float'Image(Decimal_Part) & "e " & Integer'Image(Exponent_Part));
    --Ada.Text_IO.Put_Line("---");
  end Split_Integer_Decimal_Exponent_Nums;

  procedure Split_Integer_Decimal_Exponent_Strs(Num: in FH_Float; Decimals: in Natural; Integer_Part: out UB_Text; Decimal_Part: out UB_Text;
						Exponent_Part: out UB_Text) is
    package FHEF is new Ada.Numerics.Generic_Elementary_Functions(FH_Float);
    use FHEF;

    Integer_Part_I: Integer;
    Decimal_Part_F: FH_Float;
    Decimal_Part_I: Integer;
    Exponent_Part_I: Integer;
  begin
    Split_Integer_Decimal_Exponent_Nums(Num, Decimals, Integer_Part_I, Decimal_Part_F, Exponent_Part_I);
    Decimal_Part_I := Integer(Decimal_Part_F);

    Integer_Part := To_UB_Text(Ada.Strings.Fixed.Trim(Source => Integer'Image(Integer_Part_I), Side => Ada.Strings.Left));
    Decimal_Part := To_UB_Text(Ada.Strings.Fixed.Trim(Source => Integer'Image(Decimal_Part_I), Side => Ada.Strings.Left));
    Prepend_Zeros_To_Text(Decimal_Part_F, Decimals, Decimal_Part);
    Exponent_Part := To_UB_Text(Ada.Strings.Fixed.Trim(Source => Integer'Image(Exponent_Part_I), Side => Ada.Strings.Left));
  end Split_Integer_Decimal_Exponent_Strs;

  -- BEGIN: Private functions

  function Get_Integer_Part(Num: in out FH_Float; Decimals: in Natural) return FH_Float is
    package FHEF is new Ada.Numerics.Generic_Elementary_Functions(FH_Float);
    use FHEF;

    EPSILON: constant FH_Float := 10.0 ** (-(Decimals + 1));
    Log_Arg_Floored: FH_Float;
    Expanded: FH_Float;
    Integer_Part_F: FH_Float;
  begin
    Log_Arg_Floored := FH_Float'Floor(Log(Base => 10.0, X => Num));
    Expanded := 10.0 ** Log_Arg_Floored;
    --PNum := Round_To_Valid_Nums(PNum, Decimals);

      --Ada.Text_IO.Put_Line("EP: " & FH_Float'Image((PNum / Expanded) + EPSILON) & " N: " & FH_Float'Image(PNum / Expanded));
    declare
      FPE: constant FH_Float := FH_Float'Floor((Num / Expanded) + EPSILON);
      F: constant FH_Float := FH_Float'Floor(Num / Expanded);
    begin
      --Ada.Text_IO.Put_Line("FPE: " & FH_Float'Image(FPE) & " F: " & FH_Float'Image(F));
      if FPE > F then
	Num := Num + (EPSILON * Expanded);
	Integer_Part_F := FPE;
      else
	Integer_Part_F := F;
      end if;
    end;

    return Integer_Part_F;
  end Get_Integer_Part;

  procedure Prepend_Zeros_To_Text(Num: in FH_Float; Decimals: in Natural; Text: in out UB_Text) is
    package FHEF is new Ada.Numerics.Generic_Elementary_Functions(FH_Float);
    use FHEF;

    EPSILON: constant FH_Float := 10.0 ** (-Decimals);
    VDIV: constant FH_Float := 10.0 ** Decimals;
    Log_Num: FH_Float;
    Diff: Integer;
    Zero_String: UB_Text;
    RNum: FH_Float := Num + EPSILON;
  begin
    --
    -- FIXME!!!
    -- Dear yourself from the future. If you are reading this, it means that the
    -- number formatting function has glitched out again! Do not try to fix it no matter
    -- how generous supply of coffee and beer you have at hand. Look up how the actually
    -- smart people have solved this in the sixites and just adapt their solution for fuck's sake!
    --
    -- Also, did anything "useful" come out of this? (you know what I'm talking about)...
    --
    if Num = 0.0 then
      Log_Num := 1.0;
    else
      Log_Num := FH_Float'Ceiling(Log(Base => 10.0, X => RNum));
      --Ada.Text_IO.Put_Line("Log_Num: " & FH_Float'Image(Log_Num));
      --Ada.Text_IO.Put_Line("Num: " & FH_Float'Image(Num) & " Rem: " & FH_Float'Image(FH_Float'Remainder(RNum , VDIV / 10.0)));
      if Abs(FH_Float'Remainder(RNum, VDIV / 10.0)) <= EPSILON then
	--Ada.Text_IO.Put_Line("Fixing up zeros - divisible by 100");
	Log_Num := Log_Num + 1.0;
      end if;
    end if;
    Diff := Decimals - Integer(Log_Num);
    --Ada.Text_IO.Put_Line("Diff: " & Integer'Image(Diff));
    if Diff <= 0 then
      return;
    end if;

    for I in 1 .. Diff loop
      Append_UB_Text(Source => Zero_String, New_Item => "0");
    end loop;

    Append_UB_Text(Source => Zero_String, New_Item => Text);
    Text := Zero_String;
  end Prepend_Zeros_To_Text;

end Formatting_Helpers;
