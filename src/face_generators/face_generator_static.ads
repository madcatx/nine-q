with Global_Types;

use Global_Types;
package Face_Generator_Static is
  function Display_Static_Page(Path: in String) return HTML_Code;
  function Error_Display_Answer return HTML_Code;
  function Error_Display_Assignment return HTML_Code;
  function Error_Display_Walkthrough return HTML_Code;
  function Error_UID_Registration return HTML_Code;
  function Error_Prepare_Problem return HTML_Code;
  function Error_Problem_Category return HTML_Code;
  function Error_Problem_ID return HTML_Code;
end Face_Generator_Static;
