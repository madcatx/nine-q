with Global_Types;
with Problem_Generator_Syswides;
with AWS.Templates;

use Global_Types;
package Face_Generator is
  function Generate_Error_Face(HTML: out HTML_Code; Message: in String) return RetCode;
  function Generate_Index_Face(HTML: out HTML_Code) return RetCode;
  function Generate_Face(Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
			 Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
			 HTML: out HTML_Code;
			 Pr_ID: in String; Pr_Cat: in String) return RetCode;

  function Generate_Face_With_Answer(Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				     Answer_Message: in UB_Text;
				     Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				     Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				     HTML: out HTML_Code;
				     Pr_ID: in String; Pr_Cat: in String;
				     FillIns: in Problem_Generator_Syswides.FillIns_Map.Map) return RetCode;

  function Generate_Face_With_Walkthrough(Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
					  Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
					  HTML: out HTML_Code; Pr_ID: in String; Pr_Cat: in String;
					  Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode;

private
  type Display_Mode is (Default_Mode, Answer_Mode, Walkthrough_Mode);

  function Generate_Face_Internal(Mode: in Display_Mode;
				  Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				  Answer_Message: in UB_Text;
				  Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				  Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				  HTML: out HTML_Code;
				  Pr_ID: in String; Pr_Cat: in String;
				  FillIns: in Problem_Generator_Syswides.FillIns_Map.Map;
				  Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode;

  procedure Add_Answer_Section(Translations: in out AWS.Templates.Translate_Set; Answer_Message: in UB_Text;
			       AR: in Problem_Generator_Syswides.Answer_RetCode);

  function Add_Walkthrough_Acidobazic(Translations: in out AWS.Templates.Translate_Set;
				      Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode;

  function Add_Walkthrough_Solubility_V_FROM_G_KS(Translations: in out AWS.Templates.Translate_Set;
						  Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode;

  function Generate_Face_Acidobazic(Mode: in Display_Mode;
				    Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				    Answer_Message: in UB_Text;
				    Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				    Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				    HTML: out HTML_Code;
				    Pr_ID: in String; Pr_Cat: in String;
				    FillIns: in Problem_Generator_Syswides.FillIns_Map.Map;
				    Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode;

  function Generate_Face_Solubility(Mode: in Display_Mode;
				    Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				    Answer_Message: in UB_Text;
				    Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				    Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				    HTML: out HTML_Code;
				    Pr_ID: in String; Pr_Cat: in String;
				    FillIns: in Problem_Generator_Syswides.FillIns_Map.Map;
				    Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode;

  function Generate_Face_Titration_Curve(Mode: in Display_Mode;
					 Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				         Answer_Message: in UB_Text;
				         Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				         Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				         HTML: out HTML_Code;
				         Pr_ID: in String; Pr_Cat: in String;
					 FillIns: in Problem_Generator_Syswides.FillIns_Map.Map;
					 Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode;

  ERROR_MESSAGE_KEY: constant String := "ERROR_MESSAGE";
  HEADER_CAPTION_KEY: constant String := "HEADER_CAPTION";
  HINTS_SECTION_KEY: constant String := "HINTS_SECTION";

end Face_Generator;
