with Ada.Strings.Unbounded;

package body Face_Generator is

  function Generate_Error_Face(HTML: out HTML_Code; Message: in String) return RetCode is
    use AWS.Templates;

    Temp: HTML_Code;
    Translations_Hdr: Translate_Set;
    Translations: Translate_Set;
  begin
    Insert(Translations_Hdr, Assoc(HEADER_CAPTION_KEY, "Oops..."));
    HTML := Parse(Filename => "templates/header.html", Translations => Translations_Hdr);

    Insert(Translations, Assoc(ERROR_MESSAGE_KEY, Message));
    Temp := Parse(Filename => "templates/error.html", Translations => Translations);
    Append_HTML(Source => HTML, New_Item => Temp);

    Temp := Parse(Filename => "templates/footer.html");
    Append_HTML(Source => HTML, New_Item => Temp);

    return OK;
  end Generate_Error_Face;

  function Generate_Index_Face(HTML: out HTML_Code) return RetCode is
    use AWS.Templates;

    Temp: HTML_Code;
    Translations_Hdr: Translate_Set;
  begin
    Insert(Translations_Hdr, Assoc(HEADER_CAPTION_KEY, "..."));
    Temp := Parse(Filename => "templates/header.html", Translations => Translations_Hdr, Cached => True);
    Append_HTML(Source => HTML, New_Item => Temp);

    Temp := Parse(Filename => "templates/face_index.html", Cached => True);
    Append_HTML(Source => HTML, New_Item => Temp);

    Temp := Parse(Filename => "templates/footer.html", Cached => True);
    Append_HTML(Source => HTML, New_Item => Temp);
    return OK;
  end Generate_Index_Face;

  function Generate_Face(Assignment: in Problem_Generator_Syswides.Assignment_Info.Map; Parameters:
			 in Problem_Generator_Syswides.Parameters_Info.Map;
			 HTML: out HTML_Code;
			 Pr_ID: in String; Pr_Cat: in String) return RetCode is
    use Problem_Generator_Syswides;
    FillIns: constant FillIns_Map.Map := FillIns_Map.Empty_Map;
    Walkthrough: constant Walkthrough_Info.Map := Walkthrough_Info.Empty_Map;
  begin
    return Generate_Face_Internal(Mode => Default_Mode,
				  Assignment => Assignment, Parameters => Parameters, HTML => HTML,
				  Answer_Message => To_UB_Text(""), Answer_Code => Problem_Generator_Syswides.Invalid_Answer,
				  Pr_ID => Pr_ID, Pr_Cat => Pr_Cat,
				  FillIns => FillIns,
				  Walkthrough => Walkthrough);
  end Generate_Face;

  function Generate_Face_With_Answer(Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				     Answer_Message: in UB_Text;
				     Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				     Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				     HTML: out HTML_Code; Pr_ID: in String; Pr_Cat: in String;
				     FillIns: in Problem_Generator_Syswides.FillIns_Map.Map) return RetCode is
    use Problem_Generator_Syswides;
    Walkthrough: constant Walkthrough_Info.Map := Walkthrough_Info.Empty_Map;
  begin
    return Generate_Face_Internal(Mode => Answer_Mode,
				  Assignment => Assignment, Parameters => Parameters, HTML => HTML,
				  Answer_Message => Answer_Message, Answer_Code => Answer_Code,
				  Pr_ID => Pr_ID, Pr_Cat => Pr_Cat,
				  FillIns => FillIns,
				  Walkthrough => Walkthrough);
  end Generate_Face_With_Answer;

  function Generate_Face_With_Walkthrough(Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
					  Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
					  HTML: out HTML_Code; Pr_ID: in String; Pr_Cat: in String;
					  Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode is
    use Problem_Generator_Syswides;
    FillIns: constant FillIns_Map.Map := FillIns_Map.Empty_Map;
  begin
    return Generate_Face_Internal(Mode => Walkthrough_Mode,
				  Assignment => Assignment, Parameters => Parameters, HTML => HTML,
				  Answer_Message => To_UB_Text(""), Answer_Code => Problem_Generator_Syswides.Invalid_Answer,
				  Pr_ID => Pr_ID, Pr_Cat => Pr_Cat,
				  FillIns => FillIns,
				  Walkthrough => Walkthrough);
  end Generate_Face_With_Walkthrough;
  -- BEGIN: Private functions

  function Generate_Face_Internal(Mode: in Display_Mode;
				  Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				  Answer_Message: in UB_Text;
				  Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				  Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				  HTML: out HTML_Code; Pr_ID: in String; Pr_Cat: in String;
				  FillIns: in Problem_Generator_Syswides.FillIns_Map.Map;
				  Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode is
    use Problem_Generator_Syswides;
    use Problem_Generator_Syswides.Assignment_Info;
  begin
    if Assignment.Find(PROBLEM_TYPE_KEY) = Assignment_Info.No_Element then
      return E_NOTFOUND;
    end if;

    declare
      Problem_Type_Str: constant String := Assignment.Element(PROBLEM_TYPE_KEY);
    begin
      if Problem_Type_Str = PROBLEM_TYPE_ACIDOBAZIC then
	return Generate_Face_Acidobazic(Mode, Assignment, Answer_Message, Answer_Code, Parameters, HTML, Pr_ID, Pr_Cat, FillIns, Walkthrough);
      elsif Problem_Type_Str = PROBLEM_TYPE_SOLUBILITY then
	return Generate_Face_Solubility(Mode, Assignment, Answer_Message, Answer_Code, Parameters, HTML, Pr_ID, Pr_Cat, FillIns, Walkthrough);
      elsif Problem_Type_Str = PROBLEM_TYPE_TITRATION_CURVE then
	return Generate_Face_Titration_Curve(Mode, Assignment, Answer_Message, Answer_Code, Parameters, HTML, Pr_ID, Pr_Cat, FillIns, Walkthrough);
      else
	return E_INVAL;
      end if;
    end;
  end Generate_Face_Internal;

  procedure Add_Answer_Section(Translations: in out AWS.Templates.Translate_Set; Answer_Message: in UB_Text;
			       AR: in Problem_Generator_Syswides.Answer_RetCode) is
    use AWS.Templates;
    use Problem_Generator_Syswides;

    Temp: HTML_Code;
    Translations_Answer: Translate_Set;
  begin
    case AR is
      when Correct_Answer =>
	Insert(Translations_Answer, Assoc(ANSWER_KIND_KEY, ANSWER_KIND_GOOD));
	Insert(Translations_Answer, Assoc(ANSWER_MESSAGE_KEY, UB_Text_To_Fixed_String(Answer_Message)));
	Temp := Parse(Filename => "templates/answer_section.html", Translations => Translations_Answer);
	Insert(Translations, Assoc(ANSWER_SECTION_KEY, HTML_To_Fixed_String(Temp)));
      when Wrong_Answer | Malformed_Answer =>
	Insert(Translations_Answer, Assoc(ANSWER_KIND_KEY, ANSWER_KIND_BAD));
	Insert(Translations_Answer, Assoc(ANSWER_MESSAGE_KEY, UB_Text_To_Fixed_String(Answer_Message)));
	Temp := Parse(Filename => "templates/answer_section.html", Translations => Translations_Answer);
	Insert(Translations, Assoc(ANSWER_SECTION_KEY, HTML_To_Fixed_String(Temp)));
      when others =>
	Insert(Translations, Assoc(ANSWER_SECTION_KEY, ""));
    end case;
  end Add_Answer_Section;

  function Add_Walkthrough_Acidobazic(Translations: in out AWS.Templates.Translate_Set;
				      Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode is
    use AWS.Templates;
    use Problem_Generator_Syswides;
    use Problem_Generator_Syswides.Acidobazic_Suite;
    use Problem_Generator_Syswides.Walkthrough_Info;

    WTrans: Translate_Set;
    HTML: HTML_Code;
  begin
    if Walkthrough.Find(WT_CONC_ION_FORMULA_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_CONC_ION_FORMULA_KEY, Walkthrough.Element(WT_CONC_ION_FORMULA_KEY)));
    if Walkthrough.Find(WT_CONC_ION_RESULT_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_CONC_ION_RESULT_KEY, Walkthrough.Element(WT_CONC_ION_RESULT_KEY)));
    --
    if Walkthrough.Find(WT_IGN_ATPR_FORMULA_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_IGN_ATPR_FORMULA_KEY, Walkthrough.Element(WT_IGN_ATPR_FORMULA_KEY)));
    if Walkthrough.Find(WT_IGN_ATPR_RESULT_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_IGN_ATPR_RESULT_KEY, Walkthrough.Element(WT_IGN_ATPR_RESULT_KEY)));
    --
    if Walkthrough.Find(WT_IGN_DISSOC_FORMULA_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_IGN_DISSOC_FORMULA_KEY, Walkthrough.Element(WT_IGN_DISSOC_FORMULA_KEY)));
    if Walkthrough.Find(WT_IGN_DISSOC_RESULT_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_IGN_DISSOC_RESULT_KEY, Walkthrough.Element(WT_IGN_DISSOC_RESULT_KEY)));
    --
    Insert(WTrans, Assoc(WT_IGN_CONCLUSION_KEY, Walkthrough.Element(WT_IGN_CONCLUSION_KEY)));

    if Walkthrough.Find(WT_DISSOC_CORR_FORMULA_KEY) /= Walkthrough_Info.No_Element then
      declare
	DC_HTML: HTML_Code;
	DCTrans: Translate_Set;
      begin
	if Walkthrough.Find(WT_DISSOC_CORR_FORMULA_KEY) = Walkthrough_Info.No_Element then
	  return E_INVAL;
	end if;
	Insert(DCTrans, Assoc(WT_DISSOC_CORR_FORMULA_KEY, Walkthrough.Element(WT_DISSOC_CORR_FORMULA_KEY)));
	if Walkthrough.Find(WT_DISSOC_CORR_RESULT_KEY) = Walkthrough_Info.No_Element then
	  return E_INVAL;
	end if;
	Insert(DCTrans, Assoc(WT_DISSOC_CORR_RESULT_KEY, Walkthrough.Element(WT_DISSOC_CORR_RESULT_KEY)));
	DC_HTML := Parse(Filename => "templates/walkthrough_acidobazic_dissoc.html", Translations => DCTrans);

	Insert(WTrans, Assoc(WT_DISSOC_CALCULATION_KEY, HTML_To_Fixed_String(DC_HTML)));
      end;
    end if;

    if Walkthrough.Find(WT_FINAL_RESULT_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_FINAL_RESULT_KEY, Walkthrough.Element(WT_FINAL_RESULT_KEY)));
    HTML := Parse(Filename => "templates/walkthrough_acidobazic.html", Translations => WTrans);

    Insert(Translations, Assoc(WALKTHROUGH_SECTION_KEY, HTML_To_Fixed_String(HTML)));

    return OK;
  end Add_Walkthrough_Acidobazic;

  function Add_Walkthrough_Solubility_V_FROM_G_KS(Translations: in out AWS.Templates.Translate_Set;
						  Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode is
    use AWS.Templates;
    use Problem_Generator_Syswides;
    use Problem_Generator_Syswides.Solubility_Suite;
    use Problem_Generator_Syswides.Walkthrough_Info;

    WTrans: Translate_Set;
    HTML: HTML_Code;
  begin
    if Walkthrough.Find(WT_V_PLUG_C_RESULT_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_V_PLUG_C_RESULT_KEY, Walkthrough.Element(WT_V_PLUG_C_RESULT_KEY)));

    if Walkthrough.Find(WT_V_PRE_RADEX_RESULT_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_V_PRE_RADEX_RESULT_KEY, Walkthrough.Element(WT_V_PRE_RADEX_RESULT_KEY)));

    if Walkthrough.Find(WT_V_DISS_C_RESULT_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_V_DISS_C_RESULT_KEY, Walkthrough.Element(WT_V_DISS_C_RESULT_KEY)));

    if Walkthrough.Find(WT_V_PLUG_VOLUME_RESULT_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_V_PLUG_VOLUME_RESULT_KEY, Walkthrough.Element(WT_V_PLUG_VOLUME_RESULT_KEY)));

    if Walkthrough.Find(WT_FINAL_RESULT_KEY) = Walkthrough_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(WTrans, Assoc(WT_FINAL_RESULT_KEY, Walkthrough.Element(WT_FINAL_RESULT_KEY)));

    HTML := Parse(Filename => "templates/walkthrough_solubility_vksgmw.html", Translations => WTrans);
    Insert(Translations, Assoc(WALKTHROUGH_SECTION_KEY, HTML_To_Fixed_String(HTML)));
    return OK;
  end Add_Walkthrough_Solubility_V_FROM_G_KS;

  function Generate_Face_Acidobazic(Mode: in Display_Mode;
				    Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				    Answer_Message: in UB_Text;
				    Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				    Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				    HTML: out HTML_Code; Pr_ID: in String; Pr_Cat: in String;
				    FillIns: in Problem_Generator_Syswides.FillIns_Map.Map;
				    Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode is
    use AWS.Templates;
    use Problem_Generator_Syswides;
    use Problem_Generator_Syswides.Assignment_Info;
    use Problem_Generator_Syswides.FillIns_Map;
    use Problem_Generator_Syswides.Parameters_Info;

    Translations_Hdr: Translate_Set;
    Translations: Translate_Set;
    Temp: HTML_Code;
  begin
    Insert(Translations_Hdr, Assoc(HEADER_CAPTION_KEY, "&lt;&nbsp;" & Acidobazic_Suite.PROBLEM_NAME_READABLE));
    HTML := Parse(Filename => "templates/header.html", Translations => Translations_Hdr);
    -- Add JavaScripts
    Temp := Parse(Filename => "scripts/expand_collapse.js", Cached => True);
    Append_HTML(Source => HTML, New_Item => Temp);

    -- Mandatory hidden parameters
    Insert(Translations, Assoc(RESERVED_PROBLEM_ID_KEY, RESERVED_PROBLEM_ID_KEY));
    Insert(Translations, Assoc(RESERVED_PROBLEM_ID_VAL_KEY, Pr_ID));
    Insert(Translations, Assoc(RESERVED_PROBLEM_CATEGORY_KEY, RESERVED_PROBLEM_CATEGORY_KEY));
    Insert(Translations, Assoc(RESERVED_PROBLEM_CATEGORY_VAL_KEY, Pr_Cat));
    if Assignment.Find(Acidobazic_Suite.CONCENTRATION_INT_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Acidobazic_Suite.CONCENTRATION_INT_KEY, Assignment.Element(Acidobazic_Suite.CONCENTRATION_INT_KEY)));

    if Assignment.Find(Acidobazic_Suite.CONCENTRATION_DEC_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Acidobazic_Suite.CONCENTRATION_DEC_KEY, Assignment.Element(Acidobazic_Suite.CONCENTRATION_DEC_KEY)));

    if Assignment.Find(Acidobazic_Suite.CONCENTRATION_EXP_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Acidobazic_Suite.CONCENTRATION_EXP_KEY, Assignment.Element(Acidobazic_Suite.CONCENTRATION_EXP_KEY)));

    if Assignment.Find(Acidobazic_Suite.PKX_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Acidobazic_Suite.PKX_KEY, Assignment.Element(Acidobazic_Suite.PKX_KEY)));

    if Assignment.Find(Acidobazic_Suite.PKX_VALUE_INT_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Acidobazic_Suite.PKX_VALUE_INT_KEY, Assignment.Element(Acidobazic_Suite.PKX_VALUE_INT_KEY)));

    if Assignment.Find(Acidobazic_Suite.PKX_VALUE_DEC_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Acidobazic_Suite.PKX_VALUE_DEC_KEY, Assignment.Element(Acidobazic_Suite.PKX_VALUE_DEC_KEY)));

    if Assignment.Find(Acidobazic_Suite.SUBSTANCE_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Acidobazic_Suite.SUBSTANCE_KEY, Assignment.Element(Acidobazic_Suite.SUBSTANCE_KEY)));

    Insert(Translations, Assoc("ANSWER_PH", Acidobazic_Suite.ANSWER_PH_KEY));
    Insert(Translations, Assoc("ANSWER_SIMPLIFICATION", Acidobazic_Suite.ANSWER_SIMPLIFICATION_KEY));
    Insert(Translations, Assoc("ANSWER_OPTION_SIMPL_ATPR", Acidobazic_Suite.Simplification'Image(Acidobazic_Suite.Autoprotolysis)));
    Insert(Translations, Assoc("ANSWER_OPTION_SIMPL_BOTH", Acidobazic_Suite.Simplification'Image(Acidobazic_Suite.Both)));
    Insert(Translations, Assoc("ANSWER_OPTION_SIMPL_DISSOC", Acidobazic_Suite.Simplification'Image(Acidobazic_Suite.Dissociation)));
    Insert(Translations, Assoc("PARAMETER_NO_BOTH_SIMPLIFICATIONS", Acidobazic_Suite.PARAMETER_NO_BOTH_SIMPLIFICATIONS_KEY));
    if Parameters.Find(Acidobazic_Suite.PARAMETER_NO_BOTH_SIMPLIFICATIONS_KEY) = Parameters_Info.No_Element then
      Insert(Translations, Assoc("PARAMETER_NO_BOTH_SIMPLIFICATIONS_CHECKED", ""));
    else
      Insert(Translations, Assoc("PARAMETER_NO_BOTH_SIMPLIFICATIONS_CHECKED", "checked"));
    end if;

    case Mode is
      when Answer_Mode =>
	Add_Answer_Section(Translations, Answer_Message, Answer_Code);
      when Walkthrough_Mode =>
	declare
	  Ret: RetCode;
	begin
	  Ret := Add_Walkthrough_Acidobazic(Translations, Walkthrough);
	  if Ret /= OK then
	    return Ret;
	  end if;
	end;
      when Default_Mode =>
	null;
    end case;

    -- Add FillIns
    if FillIns.Is_Empty = False then
      if FillIns.Find(Acidobazic_Suite.FILLIN_ANSWER_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Acidobazic_Suite.FILLIN_ANSWER_KEY, FillIns.Element(Acidobazic_Suite.FILLIN_ANSWER_KEY)));
    end if;

    -- Generate hints
    if Assignment.Element(Acidobazic_Suite.PKX_KEY) = Acidobazic_Suite.PKX_PKA_TEXT then
      Temp := Parse(Filename => "templates/hints_acidobazic_acid.html", Cached => True);
      Insert(Translations, Assoc(HINTS_SECTION_KEY, Temp));
    elsif Assignment.Element(Acidobazic_Suite.PKX_KEY) = Acidobazic_Suite.PKX_PKB_TEXT then
      Temp := Parse(Filename => "templates/hints_acidobazic_base.html", Cached => True);
      Insert(Translations, Assoc(HINTS_SECTION_KEY, Temp));
    end if;

    Temp := Parse(Filename => "templates/face_acidobazic.html", Translations => Translations);
    Append_HTML(Source => HTML, New_Item => Temp);


    Temp := Parse(Filename => "templates/footer.html");
    Append_HTML(Source => HTML, New_Item => Temp);

    return OK;
  end Generate_Face_Acidobazic;

  function Generate_Face_Solubility(Mode: in Display_Mode;
				    Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				    Answer_Message: in UB_Text;
				    Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				    Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				    HTML: out HTML_Code; Pr_ID: in String; Pr_Cat: in String;
				    FillIns: in Problem_Generator_Syswides.FillIns_Map.Map;
				    Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode is
    use Ada.Strings.Unbounded;
    use AWS.Templates;
    use Problem_Generator_Syswides;
    use Problem_Generator_Syswides.Assignment_Info;
    use Problem_Generator_Syswides.FillIns_Map;
    use Problem_Generator_Syswides.Parameters_Info;

    Translations_Hdr: Translate_Set;
    Translations_Params: Translate_Set;
    Translations_Submit: Translate_Set;
    Translations: Translate_Set;
    Temp, Hints: HTML_Code;
    Params_Code: HTML_Code;

    P_Subtype: UB_Text;
  begin
    P_Subtype := To_UB_Text(Parameters.Element(Solubility_Suite.PARAMETER_PROBLEM_SUBTYPE_KEY));
    Insert(Translations_Hdr, Assoc(HEADER_CAPTION_KEY, "&lt;&nbsp;" & Solubility_Suite.PROBLEM_NAME_READABLE));
    HTML := Parse(Filename => "templates/header.html", Translations => Translations_Hdr);
    -- Add JavaScripts
    Temp := Parse(Filename => "scripts/expand_collapse.js", Cached => True);
    Append_HTML(Source => HTML, New_Item => Temp);

    case Mode is
      when Answer_Mode =>
	Add_Answer_Section(Translations, Answer_Message, Answer_Code);
      when Walkthrough_Mode =>
	declare
	  Ret: RetCode;
	begin
          if P_Subtype = Solubility_Suite.PROBLEM_SUBTYPE_V_FROM_G_KS then
	    Ret := Add_Walkthrough_Solubility_V_FROM_G_KS(Translations, Walkthrough);
	  else
	    return E_NOTIMPL;
	  end if;

	  if Ret /= OK then
	    return Ret;
	  end if;
	end;
      when others =>
	null;
    end case;

    -- Add submit section
    -- - Mandatory parameters
    Insert(Translations_Submit, Assoc(RESERVED_PROBLEM_ID_KEY, RESERVED_PROBLEM_ID_KEY));
    Insert(Translations_Submit, Assoc(RESERVED_PROBLEM_ID_VAL_KEY, Pr_ID));
    -- Add FillIns
    if FillIns.Is_Empty = False then
      if FillIns.Find(Solubility_Suite.FILLIN_ANSWER_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations_Submit, Assoc(Solubility_Suite.FILLIN_ANSWER_KEY, FillIns.Element(Solubility_Suite.FILLIN_ANSWER_KEY)));
    end if;
    --
    Temp := Parse(Filename => "templates/face_solubility_submit.html", Translations => Translations_Submit);
    Insert(Translations, Assoc("SUBMIT_FORM", HTML_To_Fixed_String(Temp)));

    -- Add parameters section
    -- - Mandatory parameters
    Insert(Translations_Params, Assoc(RESERVED_PROBLEM_CATEGORY_KEY, RESERVED_PROBLEM_CATEGORY_KEY));
    Insert(Translations_Params, Assoc(RESERVED_PROBLEM_CATEGORY_VAL_KEY, Pr_Cat));
    --
    Insert(Translations_Params, Assoc(Solubility_Suite.PARAMETER_IONIC_STRENGTH_KEY, Solubility_Suite.PARAMETER_IONIC_STRENGTH_KEY));
    if Parameters.Find(Solubility_Suite.PARAMETER_IONIC_STRENGTH_KEY) /= Parameters_Info.No_Element then
      Insert(Translations_Params, Assoc("PARAMETER_IONIC_STRENGTH_CHECKED", "checked=""checked"""));
    end if;
    if Parameters.Find(Solubility_Suite.PARAMETER_PROBLEM_SUBTYPE_KEY) = Parameters_Info.No_Element then
      -- This parameter must be always present
      return E_INVAL;
    end if;
    Insert(Translations_Params, Assoc(Solubility_Suite.PARAMETER_PROBLEM_SUBTYPE_KEY, Solubility_Suite.PARAMETER_PROBLEM_SUBTYPE_KEY));
    Insert(Translations_Params, Assoc(Solubility_Suite.PROBLEM_SUBTYPE_V_FROM_G_KS, Solubility_Suite.PROBLEM_SUBTYPE_V_FROM_G_KS));
    Insert(Translations_Params, Assoc(Solubility_Suite.PROBLEM_SUBTYPE_KS_FROM_G_V, Solubility_Suite.PROBLEM_SUBTYPE_KS_FROM_G_V));
    Insert(Translations_Params, Assoc(Solubility_Suite.PROBLEM_SUBTYPE_C_FROM_KS_DIFFERENT_IONS, Solubility_Suite.PROBLEM_SUBTYPE_C_FROM_KS_DIFFERENT_IONS));
    Insert(Translations_Params, Assoc(Solubility_Suite.PROBLEM_SUBTYPE_C_FROM_KS_SHARED_ION, Solubility_Suite.PROBLEM_SUBTYPE_C_FROM_KS_SHARED_ION));
    Insert(Translations_Params, Assoc(UB_Text_To_Fixed_String(P_Subtype) & "_SELECTED", "selected=""selected"""));
    -- Load and parse parameters template
    Params_Code := Parse(Filename => "templates/face_solubility_params.html", Translations => Translations_Params);
    -- Put the processed parameters template into the face
    Insert(Translations, Assoc("PARAMETERS_FORM", HTML_To_Fixed_String(Params_Code)));

      if P_Subtype = Solubility_Suite.PROBLEM_SUBTYPE_V_FROM_G_KS then
	-- Check that we have all necessary fields in the assignment
	if Assignment.Find(Solubility_Suite.X_STOCHIO_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.Z_STOCHIO_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.KS_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.KS_DEC_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.KS_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.SAMPLE_WEIGHT_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.SAMPLE_WEIGHT_DEC_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.MOLAR_MASS_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.MOLAR_MASS_DEC_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;

	Insert(Translations, Assoc(Solubility_Suite.X_STOCHIO_KEY, Assignment.Element(Solubility_Suite.X_STOCHIO_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.Z_STOCHIO_KEY, Assignment.Element(Solubility_Suite.Z_STOCHIO_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.KS_INT_KEY, Assignment.Element(Solubility_Suite.KS_INT_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.KS_DEC_KEY, Assignment.Element(Solubility_Suite.KS_DEC_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.KS_EXP_KEY, Assignment.Element(Solubility_Suite.KS_EXP_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.SAMPLE_WEIGHT_INT_KEY, Assignment.Element(Solubility_Suite.SAMPLE_WEIGHT_INT_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.SAMPLE_WEIGHT_DEC_KEY, Assignment.Element(Solubility_Suite.SAMPLE_WEIGHT_DEC_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.MOLAR_MASS_INT_KEY, Assignment.Element(Solubility_Suite.MOLAR_MASS_INT_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.MOLAR_MASS_DEC_KEY, Assignment.Element(Solubility_Suite.MOLAR_MASS_DEC_KEY)));
	
	Hints := Parse(Filename => "templates/hints_solubility_v_ksgmw.html", Cached => True);
	Insert(Translations, Assoc(HINTS_SECTION_KEY, HTML_To_Fixed_String(Hints)));

	Temp := Parse(Filename => "templates/face_solubility_v_f_g_ks.html", Translations => Translations);
	Append_HTML(Source => HTML, New_Item => Temp);
      elsif P_Subtype = Solubility_Suite.PROBLEM_SUBTYPE_KS_FROM_G_V then
	-- Check that we have all necessary fields in the assignment
	if Assignment.Find(Solubility_Suite.X_STOCHIO_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.Z_STOCHIO_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.SAMPLE_VOLUME_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.SAMPLE_VOLUME_DEC_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.SAMPLE_VOLUME_EXP_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.SAMPLE_WEIGHT_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.SAMPLE_WEIGHT_DEC_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.MOLAR_MASS_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.MOLAR_MASS_DEC_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;

	Insert(Translations, Assoc(Solubility_Suite.X_STOCHIO_KEY, Assignment.Element(Solubility_Suite.X_STOCHIO_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.Z_STOCHIO_KEY, Assignment.Element(Solubility_Suite.Z_STOCHIO_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.SAMPLE_VOLUME_INT_KEY, Assignment.Element(Solubility_Suite.SAMPLE_VOLUME_INT_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.SAMPLE_VOLUME_DEC_KEY, Assignment.Element(Solubility_Suite.SAMPLE_VOLUME_DEC_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.SAMPLE_VOLUME_EXP_KEY, Assignment.Element(Solubility_Suite.SAMPLE_VOLUME_EXP_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.SAMPLE_WEIGHT_INT_KEY, Assignment.Element(Solubility_Suite.SAMPLE_WEIGHT_INT_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.SAMPLE_WEIGHT_DEC_KEY, Assignment.Element(Solubility_Suite.SAMPLE_WEIGHT_DEC_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.MOLAR_MASS_INT_KEY, Assignment.Element(Solubility_Suite.MOLAR_MASS_INT_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.MOLAR_MASS_DEC_KEY, Assignment.Element(Solubility_Suite.MOLAR_MASS_DEC_KEY)));

	Hints := Parse(Filename => "templates/hints_solubility_ks_gmwv.html", Cached => True);
	Insert(Translations, Assoc(HINTS_SECTION_KEY, HTML_To_Fixed_String(Hints)));

	Temp := Parse(Filename => "templates/face_solubility_ks_f_g_v.html", Translations => Translations);
	Append_HTML(Source => HTML, New_Item => Temp);
      elsif P_Subtype = Solubility_Suite.PROBLEM_SUBTYPE_C_FROM_KS_DIFFERENT_IONS or P_Subtype = Solubility_Suite.PROBLEM_SUBTYPE_C_FROM_KS_SHARED_ION then
	-- Check that we have all necessary fields in the assignment
	if Assignment.Find(Solubility_Suite.X_STOCHIO_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.Z_STOCHIO_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.EC_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.EC_DEC_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.EC_EXP_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.KS_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.KS_DEC_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;
	if Assignment.Find(Solubility_Suite.KS_INT_KEY) = Assignment_Info.No_Element then
	  return E_INVAL;
	end if;

	Insert(Translations, Assoc(Solubility_Suite.X_STOCHIO_KEY, Assignment.Element(Solubility_Suite.X_STOCHIO_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.Z_STOCHIO_KEY, Assignment.Element(Solubility_Suite.Z_STOCHIO_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.EC_INT_KEY, Assignment.Element(Solubility_Suite.EC_INT_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.EC_DEC_KEY, Assignment.Element(Solubility_Suite.EC_DEC_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.EC_EXP_KEY, Assignment.Element(Solubility_Suite.EC_EXP_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.KS_INT_KEY, Assignment.Element(Solubility_Suite.KS_INT_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.KS_DEC_KEY, Assignment.Element(Solubility_Suite.KS_DEC_KEY)));
	Insert(Translations, Assoc(Solubility_Suite.KS_EXP_KEY, Assignment.Element(Solubility_Suite.KS_EXP_KEY)));

	if P_Subtype = Solubility_Suite.PROBLEM_SUBTYPE_C_FROM_KS_DIFFERENT_IONS then 
	  Hints := Parse(Filename => "templates/hints_solubility_diff_ion.html", Cached => True);
	  Insert(Translations, Assoc(HINTS_SECTION_KEY, HTML_To_Fixed_String(Hints)));
	  Temp := Parse(Filename => "templates/face_solubility_c_f_ks_diff.html", Translations => Translations);
	else
	  Hints := Parse(Filename => "templates/hints_solubility_shared_ion.html", Cached => True);
	  Insert(Translations, Assoc(HINTS_SECTION_KEY, HTML_To_Fixed_String(Hints)));
	  Temp := Parse(Filename => "templates/face_solubility_c_f_ks_shared.html", Translations => Translations);
	end if;
	Append_HTML(Source => HTML, New_Item => Temp);
      else
	return E_INVAL;
      end if;

    Temp := Parse(Filename => "templates/footer.html");
    Append_HTML(Source => HTML, New_Item => Temp);

    return OK;
  end Generate_Face_Solubility;

  function Generate_Face_Titration_Curve(Mode: in Display_Mode;
					 Assignment: in Problem_Generator_Syswides.Assignment_Info.Map;
				         Answer_Message: in UB_Text;
				         Answer_Code: in Problem_Generator_Syswides.Answer_RetCode;
				         Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
				         HTML: out HTML_Code; Pr_ID: in String; Pr_Cat: in String;
					 FillIns: in Problem_Generator_Syswides.FillIns_Map.Map;
					 Walkthrough: in Problem_Generator_Syswides.Walkthrough_Info.Map) return RetCode is
    use AWS.Templates;
    use Problem_Generator_Syswides;
    use Problem_Generator_Syswides.Assignment_Info;
    use Problem_Generator_Syswides.FillIns_Map;
    use Problem_Generator_Syswides.Parameters_Info;

    Translations_Hdr: Translate_Set;
    Translations: Translate_Set;
    Translations_Answer: Translate_Set;
    Temp: HTML_Code;
  begin
    Insert(Translations_Hdr, Assoc(HEADER_CAPTION_KEY, "&lt;&nbsp;" & Titration_Curve_Suite.PROBLEM_NAME_READABLE));
    HTML := Parse(Filename => "templates/header.html", Translations => Translations_Hdr);
    -- Add JavaScripts
    Temp := Parse(Filename => "scripts/expand_collapse.js", Cached => True);
    Append_HTML(Source => HTML, New_Item => Temp);

    -- Mandatory hidden parameters
    Insert(Translations, Assoc(RESERVED_PROBLEM_ID_KEY, RESERVED_PROBLEM_ID_KEY));
    Insert(Translations, Assoc(RESERVED_PROBLEM_ID_VAL_KEY, Pr_ID));
    Insert(Translations, Assoc(RESERVED_PROBLEM_CATEGORY_KEY, RESERVED_PROBLEM_CATEGORY_KEY));
    Insert(Translations, Assoc(RESERVED_PROBLEM_CATEGORY_VAL_KEY, Pr_Cat));
    -- Assignment
    if Assignment.Find(Titration_Curve_Suite.SAMPLE_CONC_INT_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.SAMPLE_CONC_INT_KEY, Assignment.Element(Titration_Curve_Suite.SAMPLE_CONC_INT_KEY)));

    if Assignment.Find(Titration_Curve_Suite.SAMPLE_CONC_DEC_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.SAMPLE_CONC_DEC_KEY, Assignment.Element(Titration_Curve_Suite.SAMPLE_CONC_DEC_KEY)));

    if Assignment.Find(Titration_Curve_Suite.SAMPLE_CONC_EXP_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.SAMPLE_CONC_EXP_KEY, Assignment.Element(Titration_Curve_Suite.SAMPLE_CONC_EXP_KEY)));

    if Assignment.Find(Titration_Curve_Suite.SAMPLE_TYPE_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.SAMPLE_TYPE_KEY, Assignment.Element(Titration_Curve_Suite.SAMPLE_TYPE_KEY)));

    if Assignment.Find(Titration_Curve_Suite.TITRANT_TYPE_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.TITRANT_TYPE_KEY, Assignment.Element(Titration_Curve_Suite.TITRANT_TYPE_KEY)));

    if Assignment.Find(Titration_Curve_Suite.SAMPLE_VOLUME_INT_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.SAMPLE_VOLUME_INT_KEY, Assignment.Element(Titration_Curve_Suite.SAMPLE_VOLUME_INT_KEY)));

    if Assignment.Find(Titration_Curve_Suite.SAMPLE_VOLUME_DEC_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.SAMPLE_VOLUME_DEC_KEY, Assignment.Element(Titration_Curve_Suite.SAMPLE_VOLUME_DEC_KEY)));

    if Assignment.Find(Titration_Curve_Suite.SAMPLE_VOLUME_EXP_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.SAMPLE_VOLUME_EXP_KEY, Assignment.Element(Titration_Curve_Suite.SAMPLE_VOLUME_EXP_KEY)));

    if Assignment.Find(Titration_Curve_Suite.TITRANT_CONC_INT_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.TITRANT_CONC_INT_KEY, Assignment.Element(Titration_Curve_Suite.TITRANT_CONC_INT_KEY)));

    if Assignment.Find(Titration_Curve_Suite.TITRANT_CONC_DEC_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.TITRANT_CONC_DEC_KEY, Assignment.Element(Titration_Curve_Suite.TITRANT_CONC_DEC_KEY)));

    if Assignment.Find(Titration_Curve_Suite.TITRANT_CONC_EXP_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.TITRANT_CONC_EXP_KEY, Assignment.Element(Titration_Curve_Suite.TITRANT_CONC_EXP_KEY)));

    if Assignment.Find(Titration_Curve_Suite.PKX1_INT_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.PKX1_INT_KEY, Assignment.Element(Titration_Curve_Suite.PKX1_INT_KEY)));

    if Assignment.Find(Titration_Curve_Suite.PKX1_DEC_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.PKX1_DEC_KEY, Assignment.Element(Titration_Curve_Suite.PKX1_DEC_KEY)));

    if Assignment.Find(Titration_Curve_Suite.PKX2_INT_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.PKX2_INT_KEY, Assignment.Element(Titration_Curve_Suite.PKX2_INT_KEY)));

    if Assignment.Find(Titration_Curve_Suite.PKX2_DEC_KEY) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.PKX2_DEC_KEY, Assignment.Element(Titration_Curve_Suite.PKX2_DEC_KEY)));
    if (Assignment.Find(Titration_Curve_Suite.PKX_TYPE_KEY)) = Assignment_Info.No_Element then
      return E_INVAL;
    end if;
    Insert(Translations, Assoc(Titration_Curve_Suite.PKX_TYPE_KEY, Assignment.Element(Titration_Curve_Suite.PKX_TYPE_KEY)));
    --
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_PH_START_KEY, Titration_Curve_Suite.ANSWER_PH_START_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_VOLUME_START_KEY, Titration_Curve_Suite.ANSWER_VOLUME_FIRST_HALF_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_PH_FIRST_HALF_KEY, Titration_Curve_Suite.ANSWER_PH_FIRST_HALF_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_VOLUME_FIRST_HALF_KEY, Titration_Curve_Suite.ANSWER_VOLUME_FIRST_HALF_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_PH_FIRST_EQUIV_KEY, Titration_Curve_Suite.ANSWER_PH_FIRST_EQUIV_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_VOLUME_FIRST_EQUIV_KEY, Titration_Curve_Suite.ANSWER_VOLUME_FIRST_EQUIV_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_PH_SECOND_HALF_KEY, Titration_Curve_Suite.ANSWER_PH_SECOND_HALF_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_VOLUME_SECOND_HALF_KEY, Titration_Curve_Suite.ANSWER_VOLUME_SECOND_HALF_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_PH_SECOND_EQUIV_KEY, Titration_Curve_Suite.ANSWER_PH_SECOND_EQUIV_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_VOLUME_SECOND_EQUIV_KEY, Titration_Curve_Suite.ANSWER_VOLUME_SECOND_EQUIV_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_PH_OVER_SECOND_EQUIV_KEY, Titration_Curve_Suite.ANSWER_PH_OVER_SECOND_EQUIV_KEY));
    Insert(Translations, Assoc(Titration_Curve_Suite.ANSWER_VOLUME_OVER_SECOND_EQUIV_KEY, Titration_Curve_Suite.ANSWER_VOLUME_OVER_SECOND_EQUIV_KEY));

    if Assignment.Element(Titration_Curve_Suite.SAMPLE_TYPE_KEY) = Titration_Curve_Suite.SAMPLE_TYPE_ACID then
      Temp := Parse(Filename => "templates/titration_curve_hints_acid.html", Cached => True);
    else
      Temp := Parse(Filename => "templates/titration_curve_hints_base.html", Cached => True);
    end if;
    Insert(Translations, Assoc(HINTS_SECTION_KEY, HTML_To_Fixed_String(Temp)));

    case Mode is
      when Answer_Mode =>
	case Answer_Code is
	  when Correct_Answer =>
	    if Assignment.Find(Titration_Curve_Suite.TITRATION_CURVE_IMAGE_PATH_KEY) = Assignment_Info.No_Element then
	      return E_INVAL;
	    end if;

	    Insert(Translations_Answer, Assoc(ANSWER_KIND_KEY, ANSWER_KIND_GOOD));
	    Insert(Translations_Answer, Assoc(ANSWER_MESSAGE_KEY, UB_Text_To_Fixed_String(Answer_Message)));
	    Insert(Translations_Answer, Assoc(Titration_Curve_Suite.TITRATION_CURVE_IMAGE_PATH_KEY, Assignment.Element(Titration_Curve_Suite.TITRATION_CURVE_IMAGE_PATH_KEY)));
	    Insert(Translations_Answer, Assoc(Titration_Curve_Suite.TITRATION_CURVE_IMAGE_CLASS_KEY, Titration_Curve_Suite.TITRATION_CURVE_IMAGE_CLASS_NORM));
	    Temp := Parse(Filename => "templates/titration_curve_answer_section.html", Translations => Translations_Answer);
	    Insert(Translations, Assoc(ANSWER_SECTION_KEY, HTML_To_Fixed_String(Temp)));
	  when Wrong_Answer | Malformed_Answer =>
	    Insert(Translations_Answer, Assoc(ANSWER_KIND_KEY, ANSWER_KIND_BAD));
	    Insert(Translations_Answer, Assoc(ANSWER_MESSAGE_KEY, UB_Text_To_Fixed_String(Answer_Message)));
	    Insert(Translations_Answer, Assoc(Titration_Curve_Suite.TITRATION_CURVE_IMAGE_PATH_KEY, "/resources/noimage.png"));
	    Insert(Translations_Answer, Assoc(Titration_Curve_Suite.TITRATION_CURVE_IMAGE_CLASS_KEY, Titration_Curve_Suite.TITRATION_CURVE_IMAGE_CLASS_NODISP));
	    Temp := Parse(Filename => "templates/titration_curve_answer_section.html", Translations => Translations_Answer);
	    Insert(Translations, Assoc(ANSWER_SECTION_KEY, HTML_To_Fixed_String(Temp)));
	  when others =>
	  Insert(Translations, Assoc(ANSWER_SECTION_KEY, ""));
	end case;
      when others =>
	Insert(Translations, Assoc(ANSWER_SECTION_KEY, ""));
    end case;

    -- Add FillIns
    if FillIns.Is_Empty = False then
      if FillIns.Find(Titration_Curve_Suite.FILLIN_1_PH_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_1_PH_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_1_PH_KEY)));
      if FillIns.Find(Titration_Curve_Suite.FILLIN_2_PH_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_2_PH_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_2_PH_KEY)));
      if FillIns.Find(Titration_Curve_Suite.FILLIN_3_PH_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_3_PH_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_3_PH_KEY)));
      if FillIns.Find(Titration_Curve_Suite.FILLIN_4_PH_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_4_PH_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_4_PH_KEY)));
      if FillIns.Find(Titration_Curve_Suite.FILLIN_5_PH_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_5_PH_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_5_PH_KEY)));
      if FillIns.Find(Titration_Curve_Suite.FILLIN_6_PH_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_6_PH_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_6_PH_KEY)));
      --
      if FillIns.Find(Titration_Curve_Suite.FILLIN_2_VOL_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_2_VOL_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_2_VOL_KEY)));
      if FillIns.Find(Titration_Curve_Suite.FILLIN_3_VOL_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_3_VOL_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_3_VOL_KEY)));
      if FillIns.Find(Titration_Curve_Suite.FILLIN_4_VOL_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_4_VOL_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_4_VOL_KEY)));
      if FillIns.Find(Titration_Curve_Suite.FILLIN_5_VOL_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_5_VOL_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_5_VOL_KEY)));
      if FillIns.Find(Titration_Curve_Suite.FILLIN_6_VOL_KEY) = FillIns_Map.No_Element then
	return E_INVAL;
      end if;
      Insert(Translations, Assoc(Titration_Curve_Suite.FILLIN_6_VOL_KEY, FillIns.Element(Titration_Curve_Suite.FILLIN_6_VOL_KEY)));
    end if;


    Temp := Parse(Filename => "templates/face_titration_curve.html", Translations => Translations);

    Append_HTML(Source => HTML, New_Item => Temp);

    Temp := Parse(Filename => "templates/footer.html");
    Append_HTML(Source => HTML, New_Item => Temp);

    return OK;
  end Generate_Face_Titration_Curve;


end Face_Generator;
