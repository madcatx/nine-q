with AWS.Templates;

package body Face_Generator_Static is

  function Display_Static_Page(Path: in String) return HTML_Code is
    use AWS.Templates;

    Translations: Translate_Set;
    HTML: HTML_Code;
    Temp: HTML_Code;
  begin
    Insert(Translations, Assoc("HEADER_CAPTION", "..."));
    HTML := Parse(Filename => "templates/header.html", Translations => Translations);

    Temp := Parse(Filename => Path, Cached => True);
    Append_HTML(Source => HTML, New_Item => Temp);

    Temp := Parse(Filename => "templates/footer.html");
    Append_HTML(Source => HTML, New_Item => Temp);

    return HTML;
  end Display_Static_Page;

  function Error_Display_Answer return HTML_Code is
    use AWS.Templates;
  begin
    return Parse(Filename => "templates/static/error_display_answer.html");
  end Error_Display_Answer;

  function Error_Display_Assignment return HTML_Code is
    use AWS.Templates;
  begin
    return Parse(Filename => "templates/static/error_display_assignment.html");
  end Error_Display_Assignment;

  function Error_Display_Walkthrough return HTML_Code is
    use AWS.Templates;
  begin
    return Parse(Filename => "templates/static/error_display_walkthrough.html");
  end Error_Display_Walkthrough;

  function Error_UID_Registration return HTML_Code is
    use AWS.Templates;
  begin
    return Parse(Filename => "templates/static/error_uid_registration.html");
  end Error_UID_Registration;

  function Error_Prepare_Problem return HTML_Code is
    use AWS.Templates;
  begin
    return Parse(Filename => "templates/static/error_prepare_problem.html");
  end Error_Prepare_Problem;

  function Error_Problem_Category return HTML_Code is
    use AWS.Templates;
  begin
    return Parse(Filename => "templates/static/error_problem_category.html");
  end Error_Problem_Category;

  function Error_Problem_ID return HTML_Code is
    use AWS.Templates;
  begin
    return Parse(Filename => "templates/static/error_problem_id.html");
  end Error_Problem_ID;

end Face_Generator_Static;
