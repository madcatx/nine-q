with Ada.Numerics.Discrete_Random;
with Ada.Numerics.Float_Random;
with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Strings.Fixed;
with Ada.Text_IO;
with Formatting_Helpers;
-- For walkthrough generator
with AWS.Templates;


separate(Problem_Generator)

package body Acidobazic_Suite is
  -- BEGIN: Inherited functions
  function Create return access Acidobazic_Problem is
    Parameters: Acidobazic_Parameters;
    Problem: access Acidobazic_Problem;
  begin
    Problem := new Acidobazic_Problem;

    Parameters := (No_Both_Simplifications => False);
    Problem.Parameters := Parameters;
    Problem.Walkthrough_Generated := False;

    return Problem;
  end Create;

  function Check_Answer(Problem: in out Acidobazic_Problem; Answer: in Answer_Info.Map; FillIns: in out FillIns_Map.Map;
			Message: out UB_Text) return Answer_RetCode is
    package FH is new Formatting_Helpers(pH_Float);
    use Answer_Info;

    pH: pH_Float;
    pH_Answered: pH_Float;
  begin
    pH := Problem.Answer;
    -- Verify answer data
    if Answer.Find(ANSWER_PH_KEY) = Answer_Info.No_Element then
      return Malformed_Answer;
    end if;
    if Answer.Find(ANSWER_SIMPLIFICATION_KEY) = Answer_Info.No_Element then
      return Malformed_Answer;
    end if;

    declare
      pH_Answered_S: constant String := Answer.Element(ANSWER_PH_KEY);
    begin
      pH_Answered := FH.String_To_Float(pH_Answered_S);
    exception
      when Constraint_Error =>
	Message := To_UB_Text("Nesprávně zadaná hodnota pH");
	return Malformed_Answer;
    end;

    -- Prepare FillIns
    FillIns.Insert(FILLIN_ANSWER_KEY, Answer.Element(ANSWER_PH_KEY));

    -- Check correctness of simplification
    declare
      Simplification_Str: constant String := Answer.Element(ANSWER_SIMPLIFICATION_KEY);
    begin
      if Simplification_Str /= Simplification'Image(Problem.Simpl) then
	Message := To_UB_Text("Nesprávné zanedbání");
	return Wrong_Answer;
      end if;
    end;

    -- Check correctness of the result
    --pH := FH.Round_To_Valid_Nums(pH, Decimals);
    --pH_Answered := FH.Round_To_Valid_Nums(pH_Answered, Decimals);

    declare
      pH_Diff: pH_Float := Abs(pH - pH_Answered);
      pH_Str_Int: UB_Text;
      pH_Str_Dec: UB_Text;
      pH_A_Str_Int: UB_Text;
      pH_A_Str_Dec: UB_Text;
      pH_Diff_Str_Int: UB_Text;
      pH_Diff_Str_Dec: UB_Text;
    begin
      FH.Split_Integer_Decimal_Unscaled_Strs(pH, DECIMALS, pH_Str_Int, pH_Str_Dec);
      FH.Split_Integer_Decimal_Unscaled_Strs(pH_Answered, DECIMALS, pH_A_Str_Int, pH_A_Str_Dec);
      FH.Split_Integer_Decimal_Unscaled_Strs(pH_Diff, DECIMALS, pH_Diff_Str_Int, pH_Diff_Str_Dec);

      Message := To_UB_Text("pH vypočtené programem = ") & pH_Str_Int & To_UB_Text(",") & pH_Str_Dec & To_UB_Text(" - Vaše odpověď = ") & pH_A_Str_Int & To_UB_Text(",") & pH_A_Str_Dec & To_UB_Text(" - (rozdíl = ") & pH_Diff_Str_Int & To_UB_Text(",") & pH_Diff_Str_Dec & To_UB_Text(")");

      return Correct_Answer;
    end;
  end Check_Answer;

  function Get_Assignment(Problem: in out Acidobazic_Problem; Assignment: in out Assignment_Info.Map) return RetCode is
    C: Assignment_Info.Cursor;
    Success: Boolean;
    pKx: pH_Float;
  begin
    Assignment.Insert(PROBLEM_TYPE_KEY, PROBLEM_TYPE_ACIDOBAZIC, C, Success);
    if Success = False then
      return E_NOMEM;
    end if;
    case Problem.Subst_Type is
      when Acid =>
	Assignment.Insert(SUBSTANCE_KEY, "kyseliny", C, Success);
	Assignment.Insert(PKX_KEY, PKX_PKA_TEXT, C, Success);
      when Base =>
	Assignment.Insert(SUBSTANCE_KEY, "báze", C, Success);
	Assignment.Insert(PKX_KEY, PKX_PKB_TEXT, C, Success);
    end case;

    pKx := X_To_pX(Problem.Kx);
    -- Print pKx in nn.nnn format
    declare
      package FH is new Formatting_Helpers(pH_Float);

      Int_S: UB_Text;
      Dec_S: UB_Text;
    begin
      FH.Split_Integer_Decimal_Unscaled_Strs(pKx, Decimals, Int_S, Dec_S);
      Assignment.Insert(PKX_VALUE_INT_KEY, UB_Text_To_Fixed_String(Int_S), C, Success);
      Assignment.Insert(PKX_VALUE_DEC_KEY, UB_Text_To_Fixed_String(Dec_S), C, Success);
      if Success = False then
        return E_NOMEM;
      end if;
    end;

    -- Print concentration in N.nnn 10^n format
    declare
      package FH is new Formatting_Helpers(pH_Float);

      Int_S: UB_Text;
      Dec_S: UB_Text;
      Exp_S: UB_Text;
    begin
      FH.Split_Integer_Decimal_Exponent_Strs(Problem.cX, Decimals, Int_S, Dec_S, Exp_S);
      Assignment.Insert(CONCENTRATION_INT_KEY, UB_Text_To_Fixed_String(Int_S), C, Success);
      Assignment.Insert(CONCENTRATION_DEC_KEY, UB_Text_To_Fixed_String(Dec_S), C, Success);
      Assignment.Insert(CONCENTRATION_EXP_KEY, UB_Text_To_Fixed_String(Exp_S), C, Success);
      if Success = False then
	return E_NOMEM;
      end if;
    end;

    return OK;
  end Get_Assignment;

  function Get_Parameters(Problem: in out Acidobazic_Problem; Parameters: out Parameters_Info.Map) return RetCode is
    C: Parameters_Info.Cursor;
    Success: Boolean;
  begin
    if Problem.Parameters.No_Both_Simplifications then
      Parameters.Insert(PARAMETER_NO_BOTH_SIMPLIFICATIONS_KEY, "True", C, Success);
      if Success = False then
	return E_NOMEM;
      end if;
    end if;
    return OK;
  end Get_Parameters;

  function Get_Walkthrough(Problem: in out Acidobazic_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode is
    function Fix_Str(T: in UB_Text) return String is
    begin
      return UB_Text_To_Fixed_String(T);
    end Fix_Str;

    package FH is new Formatting_Helpers(pH_Float);
    use AWS.Templates;

    Atpr_Check: constant pH_Float := Walkthrough_Check_Autoprotolysis(Problem);
    Dissoc_Check: constant pH_Float := Walkthrough_Check_Dissociation(Problem);
    c_Ion: pH_Float := Walkthrough_Calculate_Concentration(Problem);
    Trans_CI: Translate_Set;
    Trans_AC: Translate_Set;
    Trans_DC: Translate_Set;
    c_Ion_I: UB_Text;
    c_Ion_D: UB_Text;
    c_Ion_E: UB_Text;
    Kx_I: UB_Text;
    Kx_D: UB_Text;
    Kx_E: UB_Text;
    cX_I: UB_Text;
    cX_D: UB_Text;
    cX_E: UB_Text;
    Atpr_I: UB_Text;
    Atpr_D: UB_Text;
    Atpr_E: UB_Text;
    Dissoc_I: UB_Text;
    Dissoc_D: UB_Text;
    Dissoc_E: UB_Text;
    TeXCode: UB_Text;

    Ret: RetCode;
  begin
    if Problem.Walkthrough_Generated then
      Walkthrough := Problem.Walkthrough_Stored;
      return OK;
    end if;

    FH.Split_Integer_Decimal_Exponent_Strs(c_Ion, DECIMALS, c_Ion_I, c_Ion_D, c_Ion_E);
    FH.Split_Integer_Decimal_Exponent_Strs(Problem.Kx, DECIMALS, Kx_I, Kx_D, Kx_E);
    FH.Split_Integer_Decimal_Exponent_Strs(Problem.cX, DECIMALS, cX_I, cX_D, cX_E);
    FH.Split_Integer_Decimal_Exponent_Strs(Dissoc_Check, DECIMALS, Dissoc_I, Dissoc_D, Dissoc_E);
    FH.Split_Integer_Decimal_Exponent_Strs(Atpr_Check, DECIMALS, Atpr_I, Atpr_D, Atpr_E);

    Insert(Trans_CI, Assoc(CONC_ION_INT_KEY, c_Ion_I));
    Insert(Trans_CI, Assoc(CONC_ION_DEC_KEY, c_Ion_D));
    Insert(Trans_CI, Assoc(CONC_ION_EXP_KEY, c_Ion_E));

    Insert(Trans_CI, Assoc(CONC_SUBST_INT_KEY, cX_I));
    Insert(Trans_CI, Assoc(CONC_SUBST_DEC_KEY, cX_D));
    Insert(Trans_CI, Assoc(CONC_SUBST_EXP_KEY, cX_E));

    Insert(Trans_CI, Assoc(KX_INT_KEY, Kx_I));
    Insert(Trans_CI, Assoc(KX_DEC_KEY, Kx_D));
    Insert(Trans_CI, Assoc(KX_EXP_KEY, Kx_E));

    TeXCode := Parse(Filename => WT_T_PREFIX & "ion_conc.ttex", Translations => Trans_CI);
    Ret := Problem_Generator.TeX_To_PNG(TeXCode, WT_F_ION_CONC, Problem.Resource_Prefix);
    if Ret /= OK then
      return Ret;
    end if;
    Problem.Add_Tracked_Resource(WT_F_ION_CONC & WT_F_EXTENSION);
    case Problem.Subst_Type is
      when Acid =>
	Walkthrough.Insert(WT_CONC_ION_FORMULA_KEY, "/images/h3o_calc.png");
      when Base =>
	Walkthrough.Insert(WT_CONC_ION_FORMULA_KEY, "/images/oh_calc.png");
    end case;
    Walkthrough.Insert(WT_CONC_ION_RESULT_KEY, Fix_Str(Problem.Resource_Prefix) & WT_F_ION_CONC & WT_F_EXTENSION);


    Insert(Trans_DC, Assoc(CONC_ION_INT_KEY, c_Ion_I));
    Insert(Trans_DC, Assoc(CONC_ION_DEC_KEY, c_Ion_D));
    Insert(Trans_DC, Assoc(CONC_ION_EXP_KEY, c_Ion_E));

    Insert(Trans_DC, Assoc(CONC_SUBST_INT_KEY, cX_I));
    Insert(Trans_DC, Assoc(CONC_SUBST_DEC_KEY, cX_D));
    Insert(Trans_DC, Assoc(CONC_SUBST_EXP_KEY, cX_E));

    Insert(Trans_DC, Assoc(CHECK_DISSOC_INT_KEY, Dissoc_I));
    Insert(Trans_DC, Assoc(CHECK_DISSOC_DEC_KEY, Dissoc_D));
    Insert(Trans_DC, Assoc(CHECK_DISSOC_EXP_KEY, Dissoc_E));

    TeXCode := Parse(Filename => WT_T_PREFIX & "check_dissoc.ttex", Translations => Trans_DC);
    Ret := Problem_Generator.TeX_To_PNG(TeXCode, WT_F_CHECK_DISSOC, Problem.Resource_Prefix);
    if Ret /= OK then
      return Ret;
    end if;
    Problem.Add_Tracked_Resource(WT_F_CHECK_DISSOC & WT_F_EXTENSION);
    case Problem.Subst_Type is
      when Acid =>
	Walkthrough.Insert(WT_IGN_DISSOC_FORMULA_KEY, "/images/ign_dissoc_acid.png");
      when Base =>
	Walkthrough.Insert(WT_IGN_DISSOC_FORMULA_KEY, "/images/ign_dissoc_base.png");
    end case;
    Walkthrough.Insert(WT_IGN_DISSOC_RESULT_KEY, Fix_Str(Problem.Resource_Prefix) & WT_F_CHECK_DISSOC & WT_F_EXTENSION);


    Insert(Trans_AC, Assoc(CONC_ION_INT_KEY, c_Ion_I));
    Insert(Trans_AC, Assoc(CONC_ION_DEC_KEY, c_Ion_D));
    Insert(Trans_AC, Assoc(CONC_ION_EXP_KEY, c_Ion_E));

    Insert(Trans_AC, Assoc(CHECK_ATPR_INT_KEY, Atpr_I));
    Insert(Trans_AC, Assoc(CHECK_ATPR_DEC_KEY, Atpr_D));
    Insert(Trans_AC, Assoc(CHECK_ATPR_EXP_KEY, Atpr_E));

    TeXCode := Parse(Filename => WT_T_PREFIX & "check_atpr.ttex", Translations => Trans_AC);
    Ret := Problem_Generator.TeX_To_PNG(TeXCode, WT_F_CHECK_ATPR, Problem.Resource_Prefix);
    if Ret /= OK then
      return Ret;
    end if;
    Problem.Add_Tracked_Resource(WT_F_CHECK_ATPR & WT_F_EXTENSION);
    case Problem.Subst_Type is
      when Acid =>
	Walkthrough.Insert(WT_IGN_ATPR_FORMULA_KEY, "/images/ign_atpr_acid.png");
      when Base =>
	Walkthrough.Insert(WT_IGN_ATPR_FORMULA_KEY, "/images/ign_atpr_base.png");
    end case;
    Walkthrough.Insert(WT_IGN_ATPR_RESULT_KEY, Fix_Str(Problem.Resource_Prefix) & WT_F_CHECK_ATPR & WT_F_EXTENSION);


    case Problem.Simpl is
      when Both =>
        declare
	  pH_I: UB_Text;
	  pH_D: UB_Text;
	  Trans_A: Translate_Set;
	begin
	  FH.Split_Integer_Decimal_Unscaled_Strs(Problem.Answer, DECIMALS, pH_I, pH_D);
	  Insert(Trans_A, Assoc(WT_ANSWER_PH_INT_KEY, pH_I));
	  Insert(Trans_A, Assoc(WT_ANSWER_PH_DEC_KEY, pH_D));
	  Insert(Trans_A, Assoc(CONC_SUBST_INT_KEY, cX_I));
	  Insert(Trans_A, Assoc(CONC_SUBST_DEC_KEY, cX_D));
	  Insert(Trans_A, Assoc(CONC_SUBST_EXP_KEY, cX_E));
	  Insert(Trans_A, Assoc(KX_INT_KEY, Kx_I));
	  Insert(Trans_A, Assoc(KX_DEC_KEY, Kx_D));
	  Insert(Trans_A, Assoc(KX_EXP_KEY, Kx_E));

	  case Problem.Subst_Type is
	    when Acid =>
	      TeXCode := Parse(Filename => WT_T_PREFIX & "result_acid.ttex", Translations => Trans_A);
	    when Base =>
	      TeXCode := Parse(Filename => WT_T_PREFIX & "result_base.ttex", Translations => Trans_A);
	  end case;
	  Ret := Problem_Generator.TeX_To_PNG(TeXCode, WT_F_RESULT, Problem.Resource_Prefix);
	  if Ret /= OK then
	    return Ret;
	  end if;
	  Problem.Add_Tracked_Resource(WT_F_RESULT & WT_F_EXTENSION);
	  Walkthrough.Insert(WT_FINAL_RESULT_KEY, Fix_Str(Problem.Resource_Prefix) & WT_F_RESULT & WT_F_EXTENSION);

	  Walkthrough.Insert(WT_IGN_CONCLUSION_KEY, "Lze zanedbat vše");
	end;

      when Autoprotolysis =>
	declare
	  pH_I: UB_Text;
	  pH_D: UB_Text;
	  Trans_PA: Translate_Set;
	  Trans_A: Translate_Set;
	begin
	  c_Ion := Walkthrough_Calculate_Concentration_With_Dissoc(Problem);

	  FH.Split_Integer_Decimal_Unscaled_Strs(Problem.Answer, DECIMALS, pH_I, pH_D);
	  FH.Split_Integer_Decimal_Exponent_Strs(c_Ion, DECIMALS, c_Ion_I, c_Ion_D, c_Ion_E);

	  Insert(Trans_PA, Assoc(CONC_ION_INT_KEY, c_Ion_I));
	  Insert(Trans_PA, Assoc(CONC_ION_DEC_KEY, c_Ion_D));
	  Insert(Trans_PA, Assoc(CONC_ION_EXP_KEY, c_Ion_E));
	  Insert(Trans_PA, Assoc(CONC_SUBST_INT_KEY, cX_I));
	  Insert(Trans_PA, Assoc(CONC_SUBST_DEC_KEY, cX_D));
	  Insert(Trans_PA, Assoc(CONC_SUBST_EXP_KEY, cX_E));
	  Insert(Trans_PA, Assoc(KX_INT_KEY, Kx_I));
	  Insert(Trans_PA, Assoc(KX_DEC_KEY, Kx_D));
	  Insert(Trans_PA, Assoc(KX_EXP_KEY, Kx_E));
	  TeXCode := Parse(Filename => WT_T_PREFIX & "with_dissoc.ttex", Translations => Trans_PA);
	  Ret := Problem_Generator.TeX_To_PNG(TeXCode, WT_F_WITH_DISSOC, Problem.Resource_Prefix);
	  if Ret /= OK then
	    return Ret;
	  end if;
	  Problem.Add_Tracked_Resource(WT_F_WITH_DISSOC & WT_F_EXTENSION);
	  case Problem.Subst_Type is
	    when Acid =>
	      Walkthrough.Insert(WT_DISSOC_CORR_FORMULA_KEY, "/images/h3o_calc_dissoc_acid.png");
	    when Base =>
	      Walkthrough.Insert(WT_DISSOC_CORR_FORMULA_KEY, "/images/oh_calc_dissoc_base.png");
	  end case;
	  Walkthrough.Insert(WT_DISSOC_CORR_RESULT_KEY, Fix_Str(Problem.Resource_Prefix) & WT_F_WITH_DISSOC & WT_F_EXTENSION);


	  Insert(Trans_A, Assoc(WT_ANSWER_PH_INT_KEY, pH_I));
	  Insert(Trans_A, Assoc(WT_ANSWER_PH_DEC_KEY, pH_D));
	  Insert(Trans_A, Assoc(CONC_ION_INT_KEY, c_Ion_I));
	  Insert(Trans_A, Assoc(CONC_ION_DEC_KEY, c_Ion_D));
	  Insert(Trans_A, Assoc(CONC_ION_EXP_KEY, c_Ion_E));
	  case Problem.Subst_Type is
	    when Acid =>
	      TeXCode := Parse(Filename => WT_T_PREFIX & "result_acid2.ttex", Translations => Trans_A);
	    when Base =>
	      TeXCode := Parse(Filename => WT_T_PREFIX & "result_base2.ttex", Translations => Trans_A);
	  end case;

	  Ret := Problem_Generator.TeX_To_PNG(TeXCode, WT_F_RESULT, Problem.Resource_Prefix);
	  if Ret /= OK then
	    return Ret;
	  end if;
	  Problem.Add_Tracked_Resource(WT_F_RESULT & WT_F_EXTENSION);
	  Walkthrough.Insert(WT_FINAL_RESULT_KEY, Fix_Str(Problem.Resource_Prefix) & WT_F_RESULT & WT_F_EXTENSION);

	  Walkthrough.Insert(WT_IGN_CONCLUSION_KEY, "Lze zanedbat autoprotolýzu");
	end;
      when Dissociation =>
	declare
	  pH_I: UB_Text;
	  pH_D: UB_Text;
	  Trans_A: Translate_Set;
	begin
	  FH.Split_Integer_Decimal_Unscaled_Strs(Problem.Answer, DECIMALS, pH_I, pH_D);
	  Insert(Trans_A, Assoc(WT_ANSWER_PH_INT_KEY, pH_I));
	  Insert(Trans_A, Assoc(WT_ANSWER_PH_DEC_KEY, pH_D));
	  Insert(Trans_A, Assoc(CONC_SUBST_INT_KEY, cX_I));
	  Insert(Trans_A, Assoc(CONC_SUBST_DEC_KEY, cX_D));
	  Insert(Trans_A, Assoc(CONC_SUBST_EXP_KEY, cX_E));
	  Insert(Trans_A, Assoc(KX_INT_KEY, Kx_I));
	  Insert(Trans_A, Assoc(KX_DEC_KEY, Kx_D));
	  Insert(Trans_A, Assoc(KX_EXP_KEY, Kx_E));

	  case Problem.Subst_Type is
	    when Acid =>
	      TeXCode := Parse(Filename => WT_T_PREFIX & "result_acid_atpr.ttex", Translations => Trans_A);
	    when Base =>
	      TeXCode := Parse(Filename => WT_T_PREFIX & "result_base_atpr.ttex", Translations => Trans_A);
	  end case;
	  if Ret /= OK then
	    return Ret;
	  end if;
	  Ret := Problem_Generator.TeX_To_PNG(TeXCode, WT_F_RESULT, Problem.Resource_Prefix);
	  Problem.Add_Tracked_Resource(WT_F_RESULT & WT_F_EXTENSION);
	  Walkthrough.Insert(WT_FINAL_RESULT_KEY, Fix_Str(Problem.Resource_Prefix) & WT_F_RESULT & WT_F_EXTENSION);

	  Walkthrough.Insert(WT_IGN_CONCLUSION_KEY, "Lze zanedbat úbytek disociací");
	end;
    end case;

    Problem.Walkthrough_Stored := Walkthrough;  -- Store the walkthrough for possible future use
    Problem.Walkthrough_Generated := True;
    return OK;
  end Get_Walkthrough;

  procedure New_Problem(Problem: in out Acidobazic_Problem) is
    package Random_Substance_Type_Gen is new Ada.Numerics.Discrete_Random(Result_Subtype => Substance_Type);

    ST_G: Random_Substance_Type_Gen.Generator;

    cX_Min: pH_Float;
    cX_Max: pH_Float;
  begin
    -- Substance type (acid or base)
    Random_Substance_Type_Gen.Reset(Gen => ST_G);
    Problem.Subst_Type := Random_Substance_Type_Gen.Random(Gen => ST_G);
    -- Get random dissociation constant
    Problem.Kx := Random_Kx;
    -- What simplification to use
    Problem.Simpl := Random_Simplification(Problem.Kx, Problem.Parameters.No_Both_Simplifications);

    Calculate_Concentration_Limits(cX_Min, cX_Max, Problem.Kx, Problem.Simpl);
    Problem.cX := Random_cX(cX_Min, cX_Max);

    Problem.Answer := Calculate_Solution(Problem);
  end New_Problem;

  function Set_Parameters(Problem: in out Acidobazic_Problem; Parameters: in Parameters_Info.Map) return RetCode is
    use Parameters_Info;
  begin
    if Parameters.Find(PARAMETER_NO_BOTH_SIMPLIFICATIONS_KEY) = Parameters_Info.No_Element then
      Problem.Parameters.No_Both_Simplifications := False;
    else
      Problem.Parameters.No_Both_Simplifications := True;
    end if;

    return OK;
  end Set_Parameters;
  -- END: Inherited functions

  -- BEGIN: Private functions
  function Autoprotolysis_Limit(Kx: in pH_Float) return pH_Float is
    Ratio: constant pH_Float := 2.0E-13;
  begin
    return Ratio / Kx;
  end Autoprotolysis_Limit;

  function Dissociation_Limit(Kx: in pH_Float) return pH_Float is
    Ratio_Squared: constant pH_Float := 0.0025;
  begin
    return Kx / Ratio_Squared;
  end Dissociation_Limit;

  procedure Calculate_Concentration_Limits(Min: out pH_Float; Max: out pH_Float; Kx: in pH_Float; S: in Simplification) is
  begin
    case S is
      -- We are ignoring autoprotolysis but taking dissociation into account
      when Autoprotolysis =>
	Min := Autoprotolysis_Limit(Kx);
	Max := Dissociation_Limit(Kx);
      -- We are ignoring dissociation but taking autoprotolysis into account
      when Dissociation =>
	Min := Dissociation_Limit(Kx);
	Max := Autoprotolysis_Limit(Kx);
      when Both =>
	declare
	  ATPR: constant pH_Float := Autoprotolysis_Limit(Kx);
	  Dissoc: constant pH_Float := Dissociation_Limit(Kx);
	begin
	  Min := pH_Float'Max(ATPR, Dissoc);
	  Max := Min + MAX_CONCENTRATION_DIFF;
	end;
    end case;

    -- Apply hard limit on minimum concentration, approximation we use does not work reliably below this limit
    if Min < CONCENTRATION_HARD_MIN_LIMIT then
      Min := CONCENTRATION_HARD_MIN_LIMIT;
    else
      Min := Correct_Up(Min);
    end if;
    Max := Correct_Down(Max);
  end Calculate_Concentration_Limits;

  function Calculate_Solution(Problem: in Acidobazic_Problem) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;

    package pH_Float_IO is new Ada.Text_IO.Float_IO(pH_Float);
    use pH_Float_IO;
    use Ada.Text_IO;
  begin
    case Problem.Simpl is
      -- We are ignoring everything
      when Both =>
	declare
	  pX: constant pH_Float := X_To_pX((Problem.Kx * Problem.cX) ** 0.5);
	begin
	  case Problem.Subst_Type is
	    when Acid =>
	      return pX;
	  when Base =>
	    return 14.0 - pX;
	  end case;
	end;
      -- We are ignoring autoprotolysis and taking dissociation into account
      when Autoprotolysis =>
	declare
	  pX: pH_Float;
	begin
	  pX := Walkthrough_Calculate_Concentration_With_Dissoc(Problem);
	  pX := X_To_pX(pX);
	  if Problem.Subst_Type = Base then
	    return 14.0 - pX;
	  else
	    return pX;
	  end if;
	end;
	-- We are ignoring dissociation and taking autoprotolysis into account
      when Dissociation =>
	declare
	  pX: constant pH_Float := X_To_pX((Problem.Kx * Problem.cX + K_W) ** 0.5);
	begin
	  case Problem.Subst_Type is
	    when Acid =>
	      return pX;
	    when Base =>
	      return 14.0 - pX;
	  end case;
	end;
    end case;

  end Calculate_Solution;

  function Correct_Down(Num: in pH_Float) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;

    F: constant pH_Float := Correction_Exponent(Num);
  begin
    return Num - 10.0 ** F;
  end Correct_Down;

  function Correct_Up(Num: in pH_Float) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;

    F: constant pH_Float := Correction_Exponent(Num);
  begin
    return Num + 10.0 ** F;
  end Correct_Up;

  function Correction_Exponent(Num: in pH_Float) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;

    F_Log_Num: constant pH_Float := pH_Float'Floor(Log(Base => 10.0, X => Num));
    F_Log_Dec: constant pH_Float := pH_Float'Floor(Log(Base => 10.0, X => pH_Float(DECIMALS)));
  begin
    return F_Log_Num - F_Log_Dec;
  end Correction_Exponent;

  function pX_To_X(pX: in pH_Float) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;
  begin
    return 10.0 ** (-pX);
  end pX_To_X;

  function Random_cX(Min: in pH_Float; Max: in pH_Float) return pH_Float is
    package FH is new Formatting_Helpers(pH_Float);
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;
    use Ada.Numerics.Float_Random;

    Seed: Generator;
    Rand: Float;
    cX_Rand: pH_Float;
    cX: pH_Float;
    Log_Min: constant pH_Float := Log(Base => 10.0, X => Min);
    Log_Max: constant pH_Float := Log(Base => 10.0, X => Max);
    Concentration_Scale: constant pH_Float := Log_Max - Log_Min;
  begin
    Reset(Gen => Seed);
    Rand := Random(Gen => Seed);
    cX_Rand := pH_Float(Rand);

    cX := (Concentration_Scale * cX_Rand) + Log_Min;
    cX := 10.0 ** cX;
    return FH.Round_To_Valid_Nums(cX, Decimals);
  end Random_cX;

  function Random_Kx return pH_Float is
    package FH is new Formatting_Helpers(pH_Float);
    use Ada.Numerics.Float_Random;

    type pH_Region is (Acidic, Basic);
    package pH_Region_Random is new Ada.Numerics.Discrete_Random(Result_Subtype => pH_Region);

    pH_Seed: pH_Region_Random.Generator;
    Seed: Generator;
    Region: pH_Region;
    Rand: Float;
    pH_Rand: pH_Float;
    pKx: pH_Float;

    Acid_Scale: constant pH_Float := Acidic_Max_pH - Acidic_Min_pH;
    Base_Scale: constant pH_Float := Basic_Max_pH - Basic_Min_pH;

  begin
    pH_Region_Random.Reset(Gen => pH_Seed);
    Region := pH_Region_Random.Random(Gen => pH_Seed);

    Reset(Seed);
    Rand := Random(Gen => Seed);
    pH_Rand := pH_Float(Rand);

    -- There is no need to normalize the scale because the generator already returns
    -- a normalized range
    case Region is
      when Acidic =>
	pKx := (Acid_Scale * pH_Rand) + Acidic_Min_pH;
      when Basic =>
	pKx := (Base_Scale * pH_Rand) + Basic_Min_pH;
    end case;

    pKx := FH.Round_To_Valid_Nums(pKx, Decimals);
    return pX_To_X(pKx);
  end Random_Kx;

  function Random_Simplification(Kx: in pH_Float; No_Both_Simplifications: in Boolean) return Simplification is
    package Random_Simplification_Gen is new Ada.Numerics.Discrete_Random(Result_Subtype => Boolean);
    SIM_G: Random_Simplification_Gen.Generator;
    B: Boolean;
    pKx: constant pH_Float := X_To_pX(Kx);
  begin
    if pKx <= Acidic_Max_pH then
      if No_Both_Simplifications then
	return Autoprotolysis; -- Ignore autoprotolysis, take dissociation into account
      else
	Random_Simplification_Gen.Reset(Gen => SIM_G);
	B := Random_Simplification_Gen.Random(Gen => SIM_G);
	if B then
	  return Autoprotolysis; -- Ignore autoprotolysis, take dissociation into account
	else
	  return Both; -- Ignore everything
	end if;
      end if;
    elsif pKx >= Basic_Min_pH then
      if No_Both_Simplifications then
	return Dissociation; -- Ignore dissociation, take autoprotolysis into account
      else
	Random_Simplification_Gen.Reset(Gen => SIM_G);
	B := Random_Simplification_Gen.Random(Gen => SIM_G);
	if B then
	  return Dissociation; -- Ignore dissociation, take autoprotolysis into account
	else
	  return Both; -- Ignore everything
	end if;
      end if;
    else
      raise Constraint_Error;
    end if;
  end Random_Simplification;

  function X_To_pX(X: in pH_Float) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
  begin
    return MEF.Log(Base => 10.0, X => X) * (-1.0);
  end X_To_pX;

  -- Walkthrough functiokns
  function Walkthrough_Calculate_Concentration(Problem: in Acidobazic_Problem) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;
  begin
    return (Problem.Kx * Problem.cX) ** (0.5);
  end Walkthrough_Calculate_Concentration;

  function Walkthrough_Calculate_Concentration_With_Dissoc(Problem: in Acidobazic_Problem) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;

    D: pH_Float;
    X_1: pH_Float;
    X_2: pH_Float;
  begin
    -- Solve the quadratic equation
    D := (Problem.Kx ** 2.0) + (4.0 * Problem.Kx * Problem.cX);
    if D < 0.0 then
      raise Constraint_Error;
    end if;
    D := D ** 0.5;
    X_1 := (-Problem.Kx + D) / 2.0;
    X_2 := (-Problem.Kx - D) / 2.0;
    return pH_Float'Max(X_1, X_2);
  end Walkthrough_Calculate_Concentration_With_Dissoc;

  function Walkthrough_Check_Autoprotolysis(Problem: in Acidobazic_Problem) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;
  begin
    return K_W / (Problem.Kx * Problem.cX);
  end Walkthrough_Check_Autoprotolysis;

  function Walkthrough_Check_Dissociation(Problem: in Acidobazic_Problem) return pH_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(pH_Float);
    use MEF;
  begin
    return ((Problem.Kx * Problem.cX) ** (0.5)) / Problem.cX;
  end Walkthrough_Check_Dissociation;

end Acidobazic_Suite;
