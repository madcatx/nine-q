with Ada.Directories;
with Ada.Text_IO;
with Interfaces.C;

package body Problem_Generator is

  function Get_Problem(P_Type: in Problem_Type) return access Chem_Problem'Class is
  begin
    case P_Type is
      when Acidobazic =>
	return Acidobazic_Suite.Create;
      when Solubility =>
	return Solubility_Suite.Create;
      when Titration_Curve =>
        return Titration_Curve_Suite.Create;
    end case;
  end Get_Problem;

  function TeX_To_PNG(TeXCode: in UB_Text; Filename: in String; Resource_Prefix: in Ada.Strings.Unbounded.Unbounded_String) return RetCode is
    function Sys (Arg: Interfaces.C.Char_Array) return Integer;
    pragma Import(C, Sys, "system");

    Output_File_Param: constant String := Ada.Strings.Unbounded.To_String(Resource_Prefix) & Filename;
    TeXCode_Param: constant String := Ada.Strings.Unbounded.To_String(TeXCode);
    Ret_Val: Integer;
  begin
    Ret_Val := Sys(Interfaces.C.To_C("./tex2ima -o " & Output_File_Param & " -s '" & TeXCode_Param & "'"));
    if Ret_Val /= 0 then
      return E_FAIL;
    else
      return OK;
    end if;
  end TeX_To_PNG;

  procedure Add_Tracked_Resource(Problem: in out Chem_Problem; Name: in String) is
    Full_Name: constant String := Ada.Strings.Unbounded.To_String(Problem.Resource_Prefix) & Name;
  begin
    Problem.TRL.Append(Full_Name);
  end Add_Tracked_Resource;

  procedure Free_Tracked_Resources(Problem: in out Chem_Problem) is
    procedure Delete_Tracked_File(C: in Resource_List.Cursor) is
      Path: constant String := Resource_List.Element(C);
      File: Ada.Text_IO.File_Type;
    begin
      Ada.Text_IO.Open(File => File, Mode => Ada.Text_IO.In_File, Name => Path);
      Ada.Text_IO.Close(File);
      Ada.Directories.Delete_File(Path);
    exception -- No such file, skip it
      when Ada.Text_IO.Name_Error =>
        null;
    end Delete_Tracked_File;
  begin
    Problem.TRL.Iterate(Delete_Tracked_File'Access);
  end Free_Tracked_Resources;

  procedure Set_Resource_Prefix(Problem: in out Chem_Problem; Prefix: in String) is
  begin
    Problem.Resource_Prefix := Ada.Strings.Unbounded.To_Unbounded_String(Prefix);
  end Set_Resource_Prefix;

  procedure Finalize(Problem: in out Chem_Problem) is
  begin
    Free_Tracked_Resources(Problem);
  end Finalize;

  package body Acidobazic_Suite is separate;
  package body Solubility_Suite is separate;
  package body Titration_Curve_Suite is separate;

end Problem_Generator;
