with Ada.Numerics.Discrete_Random;
with Ada.Numerics.Float_Random;
with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Strings.Fixed;
with Ada.Text_IO;
with AWS.Templates;
with Formatting_Helpers;

separate(Problem_Generator)

package body Solubility_Suite is

  -- BEGIN: Inherited funcions
  function Create return access Solubility_Problem is
    Problem: access Solubility_Problem;
  begin
    Problem := new Solubility_Problem;
    Problem.Parameters := Solubility_Parameters'(Ionic_Strength => False, P_Subtype => V_FROM_G_KS);
    Problem.Walkthrough_Generated := False;
    return Problem;
  end;

  function Check_Answer(Problem: in out Solubility_Problem; Answer: in Answer_Info.Map; FillIns: in out FillIns_Map.Map; Message: out UB_Text) return Answer_RetCode is
    package FH is new Formatting_Helpers(SS_Float);
    use Answer_Info;
    use FH;

    AF: SS_Float;
    Int_Part_A_Str: UB_Text;
    Dec_Part_A_Str: UB_Text;
    Exp_Part_A_Str: UB_Text;
    Int_Part_C_Str: UB_Text;
    Dec_Part_C_Str: UB_Text;
    Exp_Part_C_Str: UB_Text;
    VType_Str: UB_Text;
    Units_Str: UB_Text;
  begin
    -- Check that there is a valid answer in the map
    if Answer.Find(ANSWER_NUM_KEY) = Answer_Info.No_Element then
      return No_Answer;
    end if;

    begin
      AF := String_To_Float(Answer.Element(ANSWER_NUM_KEY));
    exception
      when Constraint_Error =>
	Message := To_UB_Text("Nesprávně zadaná odpověď");
	return Malformed_Answer;
    end;
    -- Prepare FillIns
    FillIns.Insert(FILLIN_ANSWER_KEY, Answer.Element(ANSWER_NUM_KEY));

    Split_Integer_Decimal_Exponent_Strs(AF, DECIMALS, Int_Part_A_Str, Dec_Part_A_Str, Exp_Part_A_Str);
    Split_Integer_Decimal_Exponent_Strs(Problem.Answer_Num, DECIMALS, Int_Part_C_Str, Dec_Part_C_Str, Exp_Part_C_Str);

    case Problem.Parameters.P_Subtype is
      when V_FROM_G_KS =>
	VType_Str := To_UB_Text("V = &nbsp;");
	Units_Str := To_UB_Text("&nbsp;dm<span class=""exponent"">3</span>");
      when KS_FROM_G_V =>
	VType_Str := To_UB_Text("K<span class=""subscript"">s</span> = &nbsp;");
	Units_Str := To_UB_Text("");
      when C_FROM_KS_DIFFERENT_IONS | C_FROM_KS_SHARED_ION =>
	VType_Str := To_UB_Text("c = &nbsp;");
	Units_Str := To_UB_Text("&nbsp;mol/dm<span class=""exponent"">3</span>");
    end case;

    Message := To_UB_Text("Odpověď vypočtená programem: ") & VType_Str & Int_Part_C_Str & To_UB_Text(",") & Dec_Part_C_Str & To_UB_Text("&nbsp;.&nbsp;10") & To_UB_Text("<span class=""exponent"">") & Exp_Part_C_Str & To_UB_Text("</span>") & Units_Str & To_UB_Text(" - Vaše odpověď: ") & VType_Str &  Int_Part_A_Str & To_UB_Text(",") & Dec_Part_A_Str & To_UB_Text("&nbsp;.&nbsp;10") & To_UB_Text("<span class=""exponent"">") & Exp_Part_A_Str & To_UB_Text("</span>") & Units_Str;

    return Correct_Answer;
  end Check_Answer;

  function Get_Assignment(Problem: in out Solubility_Problem; Assignment: in out Assignment_Info.Map) return RetCode is
    package FH is new Formatting_Helpers(SS_Float);
    use FH;
  begin
    Assignment.Insert(PROBLEM_TYPE_KEY, PROBLEM_TYPE_SOLUBILITY);
    Assignment.Insert(PROBLEM_SUBTYPE_KEY, Problem_Subtype'Image(Problem.Parameters.P_Subtype));

    -- Stochiometry
    if (Problem.Prob_Data.M > 1.0) then
      Assignment.Insert(X_STOCHIO_KEY, Ada.Strings.Fixed.Trim(Source => Stochiometric_Count'Image(Stochiometric_Count(Problem.Prob_Data.M)), Side => Ada.Strings.Left));
    else
      Assignment.Insert(X_STOCHIO_KEY, "");
    end if;
    if (Problem.Prob_Data.N > 1.0) then
      Assignment.Insert(Z_STOCHIO_KEY, Ada.Strings.Fixed.Trim(Stochiometric_Count'Image(Stochiometric_Count(Problem.Prob_Data.N)), Side => Ada.Strings.Left));
    else
      Assignment.Insert(Z_STOCHIO_KEY, "");
    end if;

    case Problem.Parameters.P_Subtype is
      when V_FROM_G_KS =>
	declare
	  G_Str_Int: UB_Text;
	  G_Str_Dec: UB_Text;
	  Ks_Str_Int: UB_Text;
	  Ks_Str_Dec: UB_Text;
	  Ks_Str_Exp: UB_Text;
	  MW_Str_Int: UB_Text;
	  MW_Str_Dec: UB_Text;
	begin
	  Split_Integer_Decimal_Unscaled_Strs(Problem.Prob_Data.V_G, DECIMALS, G_Str_Int, G_Str_Dec);
	  Split_Integer_Decimal_Unscaled_Strs(Problem.Prob_Data.V_MW, DECIMALS_MW, MW_Str_Int, MW_Str_Dec);
	  Split_Integer_Decimal_Exponent_Strs(Problem.Prob_Data.V_Ks, DECIMALS, Ks_Str_Int, Ks_Str_Dec, Ks_Str_Exp);

	  Assignment.Insert(SAMPLE_WEIGHT_INT_KEY, UB_Text_To_Fixed_String(G_Str_Int));
	  Assignment.Insert(SAMPLE_WEIGHT_DEC_KEY, UB_Text_To_Fixed_String(G_Str_Dec));
	  Assignment.Insert(MOLAR_MASS_INT_KEY, UB_Text_To_Fixed_String(MW_Str_Int));
	  Assignment.Insert(MOLAR_MASS_DEC_KEY, UB_Text_To_Fixed_String(MW_Str_Dec));
	  Assignment.Insert(KS_INT_KEY, UB_Text_To_Fixed_String(Ks_Str_Int));
	  Assignment.Insert(KS_DEC_KEY, UB_Text_To_Fixed_String(Ks_Str_Dec));
	  Assignment.Insert(KS_EXP_KEY, UB_Text_To_Fixed_String(Ks_Str_Exp));

	  return OK;
	end;
      when KS_FROM_G_V =>
	declare
	  G_Str_Int: UB_Text;
	  G_Str_Dec: UB_Text;
	  MW_Str_Int: UB_Text;
	  MW_Str_Dec: UB_Text;
	  V_Str_Int: UB_Text;
	  V_Str_Dec: UB_Text;
	  V_Str_Exp: UB_Text;
	begin 
	  Split_Integer_Decimal_Unscaled_Strs(Problem.Prob_Data.Ks_G, DECIMALS, G_Str_Int, G_Str_Dec);
	  Split_Integer_Decimal_Unscaled_Strs(Problem.Prob_Data.Ks_MW, DECIMALS_MW, MW_Str_Int, MW_Str_Dec);
	  Split_Integer_Decimal_Exponent_Strs(Problem.Prob_Data.Ks_V, DECIMALS, V_Str_Int, V_Str_Dec, V_Str_Exp);

	  Assignment.Insert(SAMPLE_WEIGHT_INT_KEY, UB_Text_To_Fixed_String(G_Str_Int));
	  Assignment.Insert(SAMPLE_WEIGHT_DEC_KEY, UB_Text_To_Fixed_String(G_Str_Dec));
	  Assignment.Insert(MOLAR_MASS_INT_KEY, UB_Text_To_Fixed_String(MW_Str_Int));
	  Assignment.Insert(MOLAR_MASS_DEC_KEY, UB_Text_To_Fixed_String(MW_Str_Dec));
	  Assignment.Insert(SAMPLE_VOLUME_INT_KEY, UB_Text_To_Fixed_String(V_Str_Int));
	  Assignment.Insert(SAMPLE_VOLUME_DEC_KEY, UB_Text_To_Fixed_String(V_Str_Dec));
	  Assignment.Insert(SAMPLE_VOLUME_EXP_KEY, UB_Text_To_Fixed_String(V_Str_Exp));
	  return OK;
	end;
      when C_FROM_KS_DIFFERENT_IONS | C_FROM_KS_SHARED_ION =>
	declare
	  EC_Str_Int: UB_Text;
	  EC_Str_Dec: UB_Text;
	  EC_Str_Exp: UB_Text;
	  Ks_Str_Int: UB_Text;
	  Ks_Str_Dec: UB_Text;
	  Ks_Str_Exp: UB_Text;
	begin
	  Split_Integer_Decimal_Exponent_Strs(Problem.Prob_Data.C_EC, DECIMALS, EC_Str_Int, EC_Str_Dec, EC_Str_Exp);
	  Split_Integer_Decimal_Exponent_Strs(Problem.Prob_Data.C_Ks, DECIMALS, Ks_Str_Int, Ks_Str_Dec, Ks_Str_Exp);

	  Assignment.Insert(EC_INT_KEY, UB_Text_To_Fixed_String(EC_Str_Int));
	  Assignment.Insert(EC_DEC_KEY, UB_Text_To_Fixed_String(EC_Str_Dec));
	  Assignment.Insert(EC_EXP_KEY, UB_Text_To_Fixed_String(EC_Str_Exp));
	  Assignment.Insert(KS_INT_KEY, UB_Text_To_Fixed_String(Ks_Str_Int));
	  Assignment.Insert(KS_DEC_KEY, UB_Text_To_Fixed_String(Ks_Str_Dec));
	  Assignment.Insert(KS_EXP_KEY, UB_Text_To_Fixed_String(Ks_Str_Exp));
	  return OK;
	end;
    end case;
  end Get_Assignment;
  
  function Get_Parameters(Problem: in out Solubility_Problem; Parameters: out Parameters_Info.Map) return RetCode is
    C: Parameters_Info.Cursor;
    Success: Boolean;
  begin
    if Problem.Parameters.Ionic_Strength then
      Parameters.Insert(PARAMETER_IONIC_STRENGTH_KEY, "True", C, Success);
      if Success = False then
	return E_NOMEM;
      end if;
    end if;

    Parameters.Insert(PARAMETER_PROBLEM_SUBTYPE_KEY, Problem_Subtype'Image(Problem.Parameters.P_Subtype), C, Success);
    if Success = False then
      return E_NOMEM;
    end if;
    return OK;
  end Get_Parameters;

  procedure New_Problem(Problem: in out Solubility_Problem) is
    package FH is new Formatting_Helpers(SS_Float);
    package Random_Stochio_Count is new Ada.Numerics.Discrete_Random(Stochiometric_Count);
    use FH;
    use Ada.Numerics.Float_Random;

    Stochio_RGen: Random_Stochio_Count.Generator;
    Float_RGen: Generator;

    M: Stochiometric_Count;	      -- Number of cations
    N: Stochiometric_Count;	      -- Number of anions
    Answer_Num: SS_Float;	      -- Answer to this problem
    Prob_Data: Solubility_Problem_Data;  -- Assignment data
  begin
    Reset(Gen => Float_RGen);

    -- Generate stochiometry of the molecul
    Random_Stochio_Count.Reset(Gen => Stochio_RGen);
    M := Random_Stochio_Count.Random(Gen => Stochio_RGen);
    N := Random_Stochio_Count.Random(Gen => Stochio_RGen);

    -- Reduce molecule stochiometry to the lowest terms
    declare
      High: Stochiometric_Count := Stochiometric_Count'Max(M, N);
      Low: constant Stochiometric_Count := Stochiometric_Count'Min(M, N);
      Modulus: Natural;
    begin
      Modulus := High mod Low;
      if Modulus = 0 then
	High := High / Low;
	if M > N then
	  M := High;
	  N := 1;
	else
	  N := High;
	  M := 1;
	end if;
      end if;
    end;

    -- Generate the rest of the data based on the problem subtype and calulate the solution
    case Problem.Parameters.P_Subtype is
      when V_FROM_G_KS =>
	declare
	  MOLAR_WEIGHT_RANGE: constant SS_Float := MOLAR_WEIGHT_MAX - MOLAR_WEIGHT_MIN;
	  SAMPLE_WEIGHT_RANGE: constant SS_Float := SAMPLE_WEIGHT_MAX - SAMPLE_WEIGHT_MIN;
	  G: SS_Float;	-- Sample weight in grams
	  Ks: SS_Float;	-- Solubility product
	  MW: SS_Float;	-- Molar mass
	begin
	  -- Generate sample weight
	  G := (SS_Float(Random(Gen => Float_RGen)) * SAMPLE_WEIGHT_RANGE) + SAMPLE_WEIGHT_MIN;
	  G := Round_To_Valid_Nums(G, DECIMALS);
	  Ks := Round_To_Valid_Nums(Generate_Solubility_Product, DECIMALS);
	  MW := (SS_Float(Random(Gen => Float_RGen)) * MOLAR_WEIGHT_RANGE) + MOLAR_WEIGHT_MIN;
	  MW := SS_Float'Rounding(MW * SS_Float(DECIMALS_MW)) / SS_Float(DECIMALS_MW);
	  Prob_Data := (Option => V_FROM_G_KS,
			M => SS_Float(M), N => SS_Float(N),
			V_G => G, V_Ks => Ks, V_MW => MW);
	  Answer_Num := Calculate_Sample_Volume(Prob_Data, Problem.Parameters.Ionic_Strength);
	end;
      when KS_FROM_G_V =>
	declare
	  MOLAR_WEIGHT_RANGE: constant SS_Float := MOLAR_WEIGHT_MAX - MOLAR_WEIGHT_MIN;
	  SAMPLE_WEIGHT_RANGE: constant SS_Float := SAMPLE_WEIGHT_MAX - SAMPLE_WEIGHT_MIN;
	  G: SS_Float;	-- Sample weight in grams
	  MW: SS_Float;	-- Molar mass
	  V: SS_Float;	-- Sample volume in dm3
	begin
	  G := (SS_Float(Random(Gen => Float_RGen)) * SAMPLE_WEIGHT_RANGE) + SAMPLE_WEIGHT_MIN;
	  G := Round_To_Valid_Nums(G, DECIMALS);
	  MW := (SS_Float(Random(Gen => Float_RGen)) * MOLAR_WEIGHT_RANGE) + MOLAR_WEIGHT_MIN;
	  MW := SS_Float'Rounding(MW * SS_Float(DECIMALS_MW)) / SS_Float(DECIMALS_MW);
	  V := Round_To_Valid_Nums(Generate_Sample_Volume, DECIMALS);
	  Prob_Data := (Option => KS_FROM_G_V,
			M => SS_Float(M), N => SS_Float(N),
			Ks_G => G, Ks_MW => MW, Ks_V => V);
	  Answer_Num := Calculate_Solubility_Product(Prob_Data, Problem.Parameters.Ionic_Strength);
	end;
      when C_FROM_KS_DIFFERENT_IONS =>
	declare
	  EC: SS_Float;	-- Concentration of the other electrolyte if mol/dm3 which does *not* share an ion with the target electrolyte
	  Ks: SS_Float;	-- Solubility product
	begin
	  EC := Round_To_Valid_Nums(Generate_Electrolyte_Concentration, DECIMALS);
	  Ks := Round_To_Valid_Nums(Generate_Solubility_Product, DECIMALS);
	  Prob_Data := (Option => C_FROM_KS_DIFFERENT_IONS,
			M => SS_Float(M), N => SS_Float(N),
			C_EC => EC, C_Ks => Ks);
	  Answer_Num := Calculate_Sat_Concentration_Different(Prob_Data, Problem.Parameters.Ionic_Strength);
	end;
      when C_FROM_KS_SHARED_ION =>
	declare
	  EC: SS_Float;	-- Concentration of the other electrolyte if mol/dm3 which *does* share an ion with the target electrolyte
	  Ks: SS_Float;	-- Solubility product
	begin
	  EC := Round_To_Valid_Nums(Generate_Electrolyte_Concentration, DECIMALS);
	  Ks := Round_To_Valid_Nums(Generate_Solubility_Product, DECIMALS);
	  Prob_Data := (Option => C_FROM_KS_SHARED_ION,
			M => SS_Float(M), N => SS_Float(N),
			C_EC => EC, C_Ks => Ks);
	  Answer_Num := Calculate_Sat_Concentration_Shared(Prob_Data, Problem.Parameters.Ionic_Strength);
	end;
    end case;

    -- Set assignment data
    Problem.Prob_Data := Prob_Data;
    Problem.Answer_Num := Answer_Num;
  end New_Problem;

  function Get_Walkthrough(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode is
  begin
    case Problem.Prob_Data.Option is
      when V_FROM_G_KS =>
	return Gen_Walkthrough_V_FROM_G_KS(Problem, Walkthrough);
      when KS_FROM_G_V =>
	return Gen_Walkthrough_KS_FROM_G_V(Problem, Walkthrough);
      when C_FROM_KS_DIFFERENT_IONS =>
	return Gen_Walkthrough_C_FROM_KS_DIFFERENT_IONS(Problem, Walkthrough);
      when C_FROM_KS_SHARED_ION =>
	return Gen_Walkthrough_C_FROM_KS_SHARED_ION(Problem, Walkthrough);
    end case;
  end Get_Walkthrough;

  function Set_Parameters(Problem: in out Solubility_Problem; Parameters: in Parameters_Info.Map) return RetCode is
    use Parameters_Info;
  begin
    -- Change ionic strength settings
    if Parameters.Find(PARAMETER_IONIC_STRENGTH_KEY) /= Parameters_Info.No_Element then
      Problem.Parameters.Ionic_Strength := True;
    else
      Problem.Parameters.Ionic_Strength := False;
    end if;

    -- Change problem subtype
    if Parameters.Find(PARAMETER_PROBLEM_SUBTYPE_KEY) /= Parameters_Info.No_Element then
      declare
	PS_Str: constant String := Parameters.Element(PARAMETER_PROBLEM_SUBTYPE_KEY);
      begin
	if PS_Str = PROBLEM_SUBTYPE_V_FROM_G_KS then
	  Problem.Parameters.P_Subtype := V_FROM_G_KS;
	elsif PS_Str = PROBLEM_SUBTYPE_KS_FROM_G_V then
	  Problem.Parameters.P_Subtype := KS_FROM_G_V;
	elsif PS_Str = PROBLEM_SUBTYPE_C_FROM_KS_DIFFERENT_IONS then
	  Problem.Parameters.P_Subtype := C_FROM_KS_DIFFERENT_IONS;
	elsif PS_Str = PROBLEM_SUBTYPE_C_FROM_KS_SHARED_ION then
	  Problem.Parameters.P_Subtype := C_FROM_KS_SHARED_ION;
	else
	  return E_INVAL;
	end if;
      end;
    else
      -- This parameter must be always present
      return E_INVAL;
    end if;

    return OK;
  end Set_Parameters;

  -- END: Inherited functions

  -- BEGIN: Private functions

  function Calculate_Activity_Coefficient(Charge: in SS_Float; Ionic_Strength: in SS_Float) return SS_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    use MEF;

    Z_Squared: constant SS_Float := Charge ** 2.0;
    IS_Sqrt: constant SS_Float := Sqrt(Ionic_Strength);
    Log_AC: SS_Float;
  begin
    Log_AC := (0.509 * Z_Squared * IS_Sqrt) / (1.0 + IS_Sqrt);
    return 10.0 ** (-Log_AC);
  end Calculate_Activity_Coefficient;

  function Calculate_Ionic_Strength(Ions: in Ion_List) return SS_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    use MEF;

    I: SS_Float := 0.0;
  begin
    for Idx in Ions'Range loop
      I := I + (Ions(Idx).Concentration * SS_Float((Ions(Idx).Charge ** 2)));
    end loop;

    I := 0.5 * I;
    return I;
  end Calculate_Ionic_Strength;

  function Calculate_Sample_Volume(Prob_Data: in Solubility_Problem_Data; Ionic_Strength: in Boolean) return SS_Float is
    package SS_IO is new Ada.Text_IO.Float_IO(SS_Float);

    C: SS_Float;
  begin
    C := Calculate_Sat_Concentration(Prob_Data.M, Prob_Data.N, Prob_Data.V_Ks, 1.0, 1.0);

    if Ionic_Strength then
      declare
	I: SS_Float;    -- Ionic strength in mol/dm3
	GM: SS_Float;  -- Activity coefficent for the cation
	GN: SS_Float;  -- Activity coefficient for the anion

	Ions: Ion_List(1 .. 2);
      begin
	Ions(1) := (Concentration => C * Prob_Data.M, Charge => Prob_Data.N);
	Ions(2) := (Concentration => C * Prob_Data.N, Charge => Prob_Data.M);
	I := Calculate_Ionic_Strength(Ions);

	GM := Calculate_Activity_Coefficient(Prob_Data.N, I);
	GN := Calculate_Activity_Coefficient(Prob_Data.M, I);

	SS_IO.Put(I); Ada.Text_IO.New_Line;
	SS_IO.Put(GM); Ada.Text_IO.New_Line;
	SS_IO.Put(GN); Ada.Text_IO.New_Line;

	C := Calculate_Sat_Concentration(Prob_Data.M, Prob_Data.N, Prob_Data.V_Ks, GM, GN);
      end;
    end if;

    return Prob_Data.V_G / (Prob_Data.V_MW * C);
  end Calculate_Sample_Volume;

  function Calculate_Sat_Concentration(Cation_Count: in SS_Float; Anion_Count: in SS_Float; Ks: in SS_Float;
				       Cation_Gamma: in SS_Float; Anion_Gamma: in SS_Float) return SS_Float is 
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    use MEF;

    MPN: constant SS_Float := Cation_Count + Anion_Count;
    MM: constant SS_Float := Cation_Count ** Cation_Count;
    NN: constant SS_Float := Anion_Count ** Anion_Count;
  begin
    return (Ks / (MM * (Cation_Gamma ** Cation_Count) * NN * (Anion_Gamma ** Anion_Count))) ** (1.0 / MPN);
  end Calculate_Sat_Concentration;

  function Calculate_Sat_Concentration_Different(Prob_Data: in Solubility_Problem_Data; Ionic_Strength: in Boolean) return SS_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    use MEF;

    GM: SS_Float;
    GN: SS_Float;
    C: SS_Float;  -- Sample concentration
    I: SS_Float;  -- Ionic strength of the other electrolyte in mol/dm3
    Ions_Other: Ion_List(1 .. 2);
  begin
    -- Calculate ionic strength of the other electrolyte
    Ions_Other(1) := (Concentration => Prob_Data.C_EC, Charge => 1.0);
    Ions_Other(2) := (Concentration => Prob_Data.C_EC, Charge => 1.0);
    I := Calculate_Ionic_Strength(Ions_Other);
    GM := Calculate_Activity_Coefficient(Prob_Data.N, I);
    GN := Calculate_Activity_Coefficient(Prob_Data.M, I);
    C := Calculate_Sat_Concentration(Prob_Data.M, Prob_Data.N, Prob_Data.C_Ks, GM, GN);

    if Ionic_Strength then
      declare
	I_All: SS_Float;    -- Ionic strength of all ions in mol/dm3
	GM: SS_Float;	    -- Activity coefficent for the cation
	GN: SS_Float;	    -- Activity coefficient for the anion

	Ions_All: Ion_List(1 .. 4);
      begin
	Ions_All(1) := (Concentration => Prob_Data.C_EC, Charge => 1.0);
	Ions_All(2) := (Concentration => Prob_Data.C_EC, Charge => 1.0);
	Ions_All(3) := (Concentration => C * Prob_Data.M, Charge => Prob_Data.N);
	Ions_All(4) := (Concentration => C * Prob_Data.N, Charge => Prob_Data.M);
	I_All := Calculate_Ionic_Strength(Ions_All);

	GM := Calculate_Activity_Coefficient(Prob_Data.N, I_All);
	GN := Calculate_Activity_Coefficient(Prob_Data.M, I_All);

	C := Calculate_Sat_Concentration(Prob_Data.M, Prob_Data.N, Prob_Data.C_Ks, GM, GN);
      end;
    end if;

    return C;
  end Calculate_Sat_Concentration_Different;

  function Calculate_Sat_Concentration_Shared(Prob_Data: in Solubility_Problem_Data; Ionic_Strength: in Boolean) return SS_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    use MEF;

    GN: SS_Float := 1.0;  -- Activity coefficient of the anion if the other electrolyte
    EC_Pwrd: SS_Float;
    R: SS_Float;
  begin
    EC_Pwrd := Prob_Data.C_EC ** Prob_Data.N;

    if Ionic_Strength then
      declare
	I_EC: SS_Float; -- Ionic strength of the ions from the other electrolyte
	Ions: Ion_List(1 .. 2);
      begin
	Ions(1) := (Concentration => Prob_Data.C_EC, Charge => 1.0);
	Ions(2) := (Concentration => Prob_Data.C_EC, Charge => Prob_Data.M);
	I_EC := Calculate_Ionic_Strength(Ions);

	GN := Calculate_Activity_Coefficient(Prob_Data.M, I_EC);
      end;
    end if;

    R := Prob_Data.C_Ks / (EC_Pwrd * (GN ** Prob_Data.N));

    return (R ** (1.0 / Prob_Data.M)) / Prob_Data.M;
  end Calculate_Sat_Concentration_Shared;
  
  function Calculate_Solubility_Product(Prob_Data: in Solubility_Problem_Data; Ionic_Strength: in Boolean) return SS_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    use MEF;

    M: constant SS_Float := Prob_Data.M;  -- Stochiometry of the cation
    N: constant SS_Float := Prob_Data.N;  -- Stochiometry of the anion
    C: SS_Float; -- Concentration of the sample
    GM: SS_Float := 1.0;  -- Activity coefficent for the cation
    GN: SS_Float := 1.0;  -- Activity coefficient for the anion
  begin
    C := Prob_Data.Ks_G / (Prob_Data.Ks_MW * Prob_Data.Ks_V);

    if Ionic_Strength then
      declare
	I: SS_Float;  -- Ionic strength in mol/dm3

	Ions: Ion_List(1 .. 2);
      begin
	Ions(1) := (Concentration => C * Prob_Data.M, Charge => Prob_Data.N); -- Cation
	Ions(2) := (Concentration => C * Prob_Data.N, Charge => Prob_Data.M); -- Anion
        I := Calculate_Ionic_Strength(Ions);

        GM := Calculate_Activity_Coefficient(M, I);	-- Cation
        GN := Calculate_Activity_Coefficient(N, I);	-- Anion
      end;
    end if;

    return ((Prob_Data.M * C * GM) ** Prob_Data.M) * ((Prob_Data.N * C * GN) ** Prob_Data.N);
  end Calculate_Solubility_Product;

  function Gen_Walkthrough_V_FROM_G_KS(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    package FH is new Formatting_Helpers(SS_Float);
    use AWS.Templates;
    use MEF;

    PRP_Fixed: constant String := UB_Text_To_Fixed_String(Problem.Resource_Prefix);
    --
    Ks_Div_MPN: constant SS_Float := Problem.Prob_Data.V_Ks / ((Problem.Prob_Data.M ** Problem.Prob_Data.M) * (Problem.Prob_Data.N ** Problem.Prob_Data.N));
    -- Values used in context multiple formulas
    Diss_C: constant SS_Float := (Problem.Prob_Data.V_Ks / ((Problem.Prob_Data.M ** Problem.Prob_Data.M) * (Problem.Prob_Data.N ** Problem.Prob_Data.N))) ** (1.0 / (Problem.Prob_Data.M + Problem.Prob_Data.N));
    DC_Int_Str: UB_Text;
    DC_Dec_Str: UB_Text;
    DC_Exp_Str: UB_Text;

    M_Int: constant Integer := Integer(Problem.Prob_Data.M);
    N_Int: constant Integer := Integer(Problem.Prob_Data.N);
    TeXCode: UB_Text;
    Ret: RetCode;
  begin
    if Problem.Walkthrough_Generated then
      Walkthrough := Problem.Walkthrough_Stored;
      return OK;
    end if;
    -- Init multicontext data
    FH.Split_Integer_Decimal_Exponent_Strs(Diss_C, DECIMALS, DC_Int_Str, DC_Dec_Str, DC_Exp_Str);

    -- Plug the numbers into the raw equation for dissolved concentration
    declare
      M_Str: constant String := Integer'Image(M_Int);
      N_Str: constant String := Integer'Image(N_Int);
      Ks_Int_Str: UB_Text;
      Ks_Dec_Str: UB_Text;
      Ks_Exp_Str: UB_Text;

      Trans: Translate_Set;
    begin
      FH.Split_Integer_Decimal_Exponent_Strs(Problem.Prob_Data.V_Ks, DECIMALS, Ks_Int_Str, Ks_Dec_Str, Ks_Exp_Str);
      Insert(Trans, Assoc(WT_KS_INT_KEY, Ks_Int_Str));
      Insert(Trans, Assoc(WT_KS_DEC_KEY, Ks_Dec_Str));
      Insert(Trans, Assoc(WT_KS_EXP_KEY, Ks_Exp_Str));
      Insert(Trans, Assoc(WT_M_KEY, M_Str));
      Insert(Trans, Assoc(WT_N_KEY, N_Str));

      TeXCode := Parse(Filename => WT_T_PREFIX & "v_plug_c.ttex", Translations => Trans);
      Ret := TeX_To_PNG(TeXCode, WT_F_V_PLUG_C, Problem.Resource_Prefix);
      if Ret /= OK then
	return Ret;
      end if;
      Problem.Add_Tracked_Resource(WT_F_V_PLUG_C & WT_F_EXTENSION);
      Walkthrough.Insert(WT_V_PLUG_C_RESULT_KEY, PRP_Fixed & WT_F_V_PLUG_C & WT_F_EXTENSION);
    end;

    -- Show the result just before root extraction
    declare
      MPN_Str: constant String := Integer'Image(M_Int + N_Int);
      KDMN_Int_Str: UB_Text;
      KDMN_Dec_Str: UB_Text;
      KDMN_Exp_Str: UB_Text;

      Trans: Translate_Set;
    begin
      FH.Split_Integer_Decimal_Exponent_Strs(Ks_Div_MPN, DECIMALS, KDMN_Int_Str, KDMN_Dec_Str, KDMN_Exp_Str);
      Insert(Trans, Assoc(WT_KDMN_INT_KEY, KDMN_Int_Str));
      Insert(Trans, Assoc(WT_KDMN_DEC_KEY, KDMN_Dec_Str));
      Insert(Trans, Assoc(WT_KDMN_EXP_KEY, KDMN_Exp_Str));
      Insert(Trans, Assoc(WT_MPN_KEY, MPN_Str));

      TeXCode := Parse(Filename => WT_T_PREFIX & "v_pre_radex.ttex", Translations => Trans);
      Ret := TeX_To_PNG(TeXCode, WT_F_V_PRE_RADEX, Problem.Resource_Prefix);
      if Ret /= OK then
	return Ret;
      end if;
      Problem.Add_Tracked_Resource(WT_F_V_PRE_RADEX & WT_F_EXTENSION);
      Walkthrough.Insert(WT_V_PRE_RADEX_RESULT_KEY, PRP_Fixed & WT_F_V_PRE_RADEX & WT_F_EXTENSION);
    end;

    -- Dissolved concentration
    declare

      Trans: Translate_Set;
    begin
      Insert(Trans, Assoc(WT_DC_INT_KEY, DC_Int_Str));
      Insert(Trans, Assoc(WT_DC_DEC_KEY, DC_Dec_Str));
      Insert(Trans, Assoc(WT_DC_EXP_KEY, DC_Exp_Str));

      TeXCode := Parse(Filename => WT_T_PREFIX & "v_diss_c.ttex", Translations => Trans);
      Ret := TeX_To_PNG(TeXCode, WT_F_V_DISS_C, Problem.Resource_Prefix);
      if Ret /= OK then
	return Ret;
      end if;
      Problem.Add_Tracked_Resource(WT_F_V_DISS_C & WT_F_EXTENSION);
      Walkthrough.Insert(WT_V_DISS_C_RESULT_KEY, PRP_Fixed & WT_F_V_DISS_C & WT_F_EXTENSION);
    end;

    -- Plug the numbers into the volume calculation
    declare
      G_Int_Str: UB_Text;
      G_Dec_Str: UB_Text;
      MW_Int_Str: UB_Text;
      MW_Dec_Str: UB_Text;

      Trans: Translate_Set;
    begin
      FH.Split_Integer_Decimal_Unscaled_Strs(Problem.Prob_Data.V_G, DECIMALS, G_Int_Str, G_Dec_Str);
      FH.Split_Integer_Decimal_Unscaled_Strs(Problem.Prob_Data.V_MW, DECIMALS_MW, MW_Int_Str, MW_Dec_Str);

      Insert(Trans, Assoc(WT_G_INT_KEY, G_Int_Str));
      Insert(Trans, Assoc(WT_G_DEC_KEY, G_Dec_Str));
      Insert(Trans, Assoc(WT_MW_INT_KEY, MW_Int_Str));
      Insert(Trans, Assoc(WT_MW_DEC_KEY, MW_Dec_Str));
      Insert(Trans, Assoc(WT_DC_INT_KEY, DC_Int_Str));
      Insert(Trans, Assoc(WT_DC_DEC_KEY, DC_Dec_Str));
      Insert(Trans, Assoc(WT_DC_EXP_KEY, DC_Exp_Str));

      TeXCode := Parse(Filename => WT_T_PREFIX & "v_plug_volume.ttex", Translations => Trans);
      Ret := TeX_To_PNG(TeXCode, WT_F_V_PLUG_VOLUME, Problem.Resource_Prefix);
      if Ret /= OK then
	return Ret;
      end if;
      Problem.Add_Tracked_Resource(WT_F_V_PLUG_VOLUME & WT_F_EXTENSION);
      Walkthrough.Insert(WT_V_PLUG_VOLUME_RESULT_KEY, PRP_Fixed & WT_F_V_PLUG_VOLUME & WT_F_EXTENSION);
    end;

    -- Display the final result
    declare
      Res_Int_Str: UB_Text;
      Res_Dec_Str: UB_Text;
      Res_Exp_Str: UB_Text;

      Trans: Translate_Set;
    begin
      FH.Split_Integer_Decimal_Exponent_Strs(Problem.Answer_Num, DECIMALS, Res_Int_Str, Res_Dec_Str, Res_Exp_Str);

      Insert(Trans, Assoc(WT_RES_INT_KEY, Res_Int_Str));
      Insert(Trans, Assoc(WT_RES_DEC_KEY, Res_Dec_Str));
      Insert(Trans, Assoc(WT_RES_EXP_KEY, Res_Exp_Str));

      TeXCode := Parse(Filename => WT_T_PREFIX & "v_answer.ttex", Translations => Trans);
      Ret := TeX_To_PNG(TeXCode, WT_F_ANSWER, Problem.Resource_Prefix);
      if Ret /= OK then
	return Ret;
      end if;
      Problem.Add_Tracked_Resource(WT_F_ANSWER & WT_F_EXTENSION);
      Walkthrough.Insert(WT_FINAL_RESULT_KEY, PRP_Fixed & WT_F_ANSWER & WT_F_EXTENSION);
    end;

    Problem.Walkthrough_Generated := True; 
    return OK;
  end Gen_Walkthrough_V_FROM_G_KS;

  function Gen_Walkthrough_KS_FROM_G_V(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode is
  begin
    return E_NOTIMPL;
  end Gen_Walkthrough_KS_FROM_G_V;

  function Gen_Walkthrough_C_FROM_KS_DIFFERENT_IONS(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode is
  begin
    return E_NOTIMPL;
  end Gen_Walkthrough_C_FROM_KS_DIFFERENT_IONS;

  function Gen_Walkthrough_C_FROM_KS_SHARED_ION(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode is
  begin
    return E_NOTIMPL;
  end Gen_Walkthrough_C_FROM_KS_SHARED_ION;

  function Generate_Electrolyte_Concentration return SS_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    use Ada.Numerics.Float_Random;
    use MEF;

    CONC_RANGE: constant SS_Float := ELECTROLYTE_CONCENTRATION_LOG_MAX - ELECTROLYTE_CONCENTRATION_LOG_MIN;
 
    Float_RGen: Generator;
    R: SS_Float;
  begin
    Reset(Gen => Float_RGen);
    R := (SS_Float(Random(Gen => Float_RGen)) * CONC_RANGE) + ELECTROLYTE_CONCENTRATION_LOG_MIN;

    return 10.0 ** R;
  end Generate_Electrolyte_Concentration;

  function Generate_Sample_Volume return SS_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    use Ada.Numerics.Float_Random;
    use MEF;

    VOL_RANGE: constant SS_Float := SAMPLE_VOLUME_LOG_MAX - SAMPLE_VOLUME_LOG_MIN;
    Float_RGen: Generator;
    R: SS_Float;
  begin
    Reset(Gen => Float_RGen);
    R := (SS_Float(Random(Gen => Float_RGen)) * VOL_RANGE) + SAMPLE_VOLUME_LOG_MIN;

    return 10.0 ** R;
  end Generate_Sample_Volume;

  function Generate_Solubility_Product return SS_Float is
    package MEF is new Ada.Numerics.Generic_Elementary_Functions(SS_Float);
    use Ada.Numerics.Float_Random;
    use MEF;

    PKS_RANGE: constant SS_Float := PKS_MAX - PKS_MIN;
    Float_RGen: Generator;
    R: SS_Float;
  begin
    Reset(Gen => Float_RGen);
    R := (SS_Float(Random(Gen => Float_RGen)) * PKS_RANGE) + PKS_MIN;

    return 10.0 ** (-R);
  end Generate_Solubility_Product;

  procedure Get_Precise_Answer_String(Message: in out UB_Text; P_Subtype: in Problem_Subtype; Answer: in SS_Float) is
    package FH is new Formatting_Helpers(SS_Float);
    use FH;

    Ans_Int_Str: UB_Text;
    Ans_Dec_Str: UB_Text;
    Ans_Exp_Str: UB_Text;
  begin
    Split_Integer_Decimal_Exponent_Strs(Answer, DECIMALS, Ans_Int_Str, Ans_Dec_Str, Ans_Exp_Str);

    case P_Subtype is
      when V_FROM_G_KS =>
	Append_UB_Text(Source => Message, New_Item => "V = ");
      when KS_FROM_G_V =>
	Append_UB_Text(Source => Message, New_Item => "K<span class=""subscript"">s</span> = ");
      when C_FROM_KS_DIFFERENT_IONS | C_FROM_KS_SHARED_ION =>
	Append_UB_Text(Source => Message, New_Item => "c = ");
    end case;

    Append_UB_Text(Source => Message, New_Item => Ans_Int_Str); Append_UB_Text(Source => Message, New_Item => ","); Append_UB_Text(Source => Message, New_Item => Ans_Dec_Str);
    Append_UB_Text(Source => Message, New_Item => "&nbsp;.&nbsp;10<span class=""exponent"">"); Append_UB_Text(Source => Message, New_Item => Ans_Exp_Str); Append_UB_Text(Source => Message, New_Item => "</span>&nbsp;");

    case P_Subtype is
      when V_FROM_G_KS =>
	Append_UB_Text(Source => Message, New_Item => "dm<span class=""exponent"">3</span>");
      when KS_FROM_G_V =>
	null;
      when C_FROM_KS_DIFFERENT_IONS | C_FROM_KS_SHARED_ION =>
	Append_UB_Text(Source => Message, New_Item => "mol/dm<span class=""exponent"">3</span>");
    end case;
  end Get_Precise_Answer_String;
    

end Solubility_Suite;
