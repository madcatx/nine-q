with Ada.Containers.Indefinite_Ordered_Maps;

package Problem_Generator_Syswides is

  type Answer_RetCode is (Invalid_Answer, No_Answer, Correct_Answer, Wrong_Answer, Malformed_Answer);
  type Problem_Type is (Acidobazic, Solubility, Titration_Curve);
  package Answer_Info is new Ada.Containers.Indefinite_Ordered_Maps(String, String);
  package Assignment_Info is new Ada.Containers.Indefinite_Ordered_Maps(String, String);
  package FillIns_Map is new Ada.Containers.Indefinite_Ordered_Maps(String, String);
  package Parameters_Info is new Ada.Containers.Indefinite_Ordered_Maps(String, String);
  package Walkthrough_Info is new Ada.Containers.Indefinite_Ordered_Maps(String, String);

  ANSWER_KIND_KEY: constant String := "ANSWER_KIND";
    ANSWER_KIND_GOOD: constant String := "answer_kind_good";
    ANSWER_KIND_BAD: constant String := "answer_kind_bad";
  ANSWER_MESSAGE_KEY: constant String := "ANSWER_MESSAGE";
  ANSWER_SECTION_KEY: constant String := "ANSWER_SECTION";
  --
  WALKTHROUGH_SECTION_KEY: constant String := "WALKTHROUGH_SECTION";
  --
  PARAMETER_TRUE: constant String := "True";
  PARAMETER_FALSE: constant String := "False";
  --
  PROBLEM_TYPE_KEY: constant String := "PROBLEM_TYPE";
  PROBLEM_TYPE_ACIDOBAZIC: constant String := Problem_Type'Image(Acidobazic);
  PROBLEM_TYPE_SOLUBILITY: constant String := Problem_Type'Image(Solubility);
  PROBLEM_TYPE_TITRATION_CURVE: constant String := Problem_Type'Image(Titration_Curve);
  --
  RESERVED_PROBLEM_ID_KEY: constant String := "RESERVED__PROBLEM_ID";
    RESERVED_PROBLEM_ID_VAL_KEY: constant String := "RESERVED__PROBLEM_ID_VAL";
  RESERVED_PROBLEM_CATEGORY_KEY: constant String := "RESERVED__PROBLEM_CATEGORY";
    RESERVED_PROBLEM_CATEGORY_VAL_KEY: constant String := "RESERVED__PROBLEM_CATEGORY_VAL";
  --
  WT_FINAL_RESULT_KEY: constant String := "WT_FINAL_RESULT";

  package Acidobazic_Suite is
    -- What effect to ignore in calculations?
    type Simplification is (Autoprotolysis, Dissociation, Both);

    PROBLEM_NAME_READABLE: constant String := "pH jednosytné kyseliny/báze";
    CONCENTRATION_INT_KEY: constant String := "CONCENTRATION_INT";
    CONCENTRATION_DEC_KEY: constant String := "CONCENTRATION_DEC";
    CONCENTRATION_EXP_KEY: constant String := "CONCENTRATION_EXP";
    PKX_KEY: constant String := "PKX";
      PKX_PKA_TEXT: constant String := "pKa";
      PKX_PKB_TEXT: constant String := "pKb";
    PKX_VALUE_DEC_KEY: constant String := "PKX_VALUE_DEC";
    PKX_VALUE_INT_KEY: constant String := "PKX_VALUE_INT";
    SIMPLIFICATION_KEY: constant String := "SIMPLIFICATION";
    SUBSTANCE_KEY: constant String := "SUBSTANCE";
    --
    ANSWER_PH_KEY: constant String := "ANSWER_PH";
    ANSWER_SIMPLIFICATION_KEY: constant String := "ANSWER_SIMPLIFICATION";
    --
    PARAMETER_NO_BOTH_SIMPLIFICATIONS_KEY: constant String := "PARAMETER_NO_BOTH_SIMPLIFICATIONS";
    --
    FILLIN_ANSWER_KEY: constant String := "FILLIN_ANSWER";
    --
    WT_CONC_ION_FORMULA_KEY: constant String := "WT_CONC_ION_FORMULA";
    WT_CONC_ION_RESULT_KEY: constant String := "WT_CONC_ION_RESULT";
    WT_DISSOC_CALCULATION_KEY: constant String := "WT_DISSOC_CALCULATION";
    WT_DISSOC_CORR_FORMULA_KEY: constant String := "WT_DISSOC_CORR_FORMULA";
    WT_DISSOC_CORR_RESULT_KEY: constant String := "WT_DISSOC_CORR_RESULT";
    WT_IGN_ATPR_FORMULA_KEY: constant String := "WT_IGN_ATPR_FORMULA";
    WT_IGN_ATPR_RESULT_KEY: constant String := "WT_IGN_ATPR_RESULT";
    WT_IGN_CONCLUSION_KEY: constant String := "IGN_CONCLUSION";
    WT_IGN_DISSOC_FORMULA_KEY: constant String := "WT_IGN_DISSOC_FORMULA";
    WT_IGN_DISSOC_RESULT_KEY: constant String := "WT_IGN_DISSOC_RESULT";
  end Acidobazic_Suite;

  package Solubility_Suite is
    type Problem_Subtype is (V_FROM_G_KS, KS_FROM_G_V, C_FROM_KS_DIFFERENT_IONS, C_FROM_KS_SHARED_ION);

    PROBLEM_NAME_READABLE: constant String := "Srážecí rovnováhy";
    PROBLEM_SUBTYPE_KEY: constant String := "PROBLEM_SUBTYPE";
      PROBLEM_SUBTYPE_V_FROM_G_KS: constant String := Problem_Subtype'Image(V_FROM_G_KS);
      PROBLEM_SUBTYPE_KS_FROM_G_V: constant String := Problem_Subtype'Image(KS_FROM_G_V);
      PROBLEM_SUBTYPE_C_FROM_KS_DIFFERENT_IONS: constant String := Problem_Subtype'Image(C_FROM_KS_DIFFERENT_IONS);
      PROBLEM_SUBTYPE_C_FROM_KS_SHARED_ION: constant String := Problem_Subtype'Image(C_FROM_KS_SHARED_ION);
    ANSWER_NUM_KEY: constant String := "ANSWER_NUM";
    --
    EC_INT_KEY: constant String := "EC_INT";
    EC_DEC_KEY: constant String := "EC_DEC";
    EC_EXP_KEY: constant String := "EC_EXP";
    KS_INT_KEY: constant String := "KS_INT";
    KS_DEC_KEY: constant String := "KS_DEC";
    KS_EXP_KEY: constant String := "KS_EXP";
    MOLAR_MASS_INT_KEY: constant String := "MOLAR_MASS_INT";
    MOLAR_MASS_DEC_KEY: constant String := "MOLAR_MASS_DEC";
    SAMPLE_VOLUME_INT_KEY: constant String := "SAMPLE_VOLUME_INT";
    SAMPLE_VOLUME_DEC_KEY: constant String := "SAMPLE_VOLUME_DEC";
    SAMPLE_VOLUME_EXP_KEY: constant String := "SAMPLE_VOLUME_EXP";
    SAMPLE_WEIGHT_INT_KEY: constant String := "SAMPLE_WEIGHT_INT";
    SAMPLE_WEIGHT_DEC_KEY: constant String := "SAMPLE_WEIGHT_DEC";
    X_STOCHIO_KEY: constant String := "X_STOCHIO";
    Z_STOCHIO_KEY: constant String := "Z_STOCHIO";
    --
    PARAMETER_IONIC_STRENGTH_KEY: constant String := "PARAMETER_IONIC_STRENGTH";
    PARAMETER_PROBLEM_SUBTYPE_KEY: constant String := "PARAMETER_PROBLEM_SUBTYPE";
    --
    FILLIN_ANSWER_KEY: constant String := "FILLIN_ANSWER";
    --
    WT_V_DISS_C_RESULT_KEY: constant String := "WT_V_DISS_C_RESULT"; 
    WT_V_PLUG_C_RESULT_KEY: constant String := "WT_V_PLUG_C_RESULT";
    WT_V_PLUG_VOLUME_RESULT_KEY: constant String := "WT_V_PLUG_VOLUME_RESULT";
    WT_V_PRE_RADEX_RESULT_KEY: constant String := "WT_V_PRE_RADEX_RESULT";

  end Solubility_Suite;

  package Titration_Curve_Suite is
    type Sample_Type is (ACID, BASE);

    PROBLEM_NAME_READABLE: constant String := "Titrační křivka";
    SAMPLE_TYPE_KEY: constant String := "SAMPLE_TYPE";
      SAMPLE_TYPE_ACID: constant String := "kyseliny";
      SAMPLE_TYPE_BASE: constant String := "báze";
    --
    --
    SAMPLE_CONC_INT_KEY: constant String := "SAMPLE_CONC_INT";
    SAMPLE_CONC_DEC_KEY: constant String := "SAMPLE_CONC_DEC";
    SAMPLE_CONC_EXP_KEY: constant String := "SAMPLE_CONC_EXP";
    --
    TITRANT_TYPE_KEY: constant String := "TITRANT_TYPE";
      TITRANT_TYPE_ACID: constant String := "kyseliny";
      TITRANT_TYPE_BASE: constant String  := "báze";
    --
    TITRANT_CONC_INT_KEY: constant String := "TITRANT_CONC_INT";
    TITRANT_CONC_DEC_KEY: constant String := "TITRANT_CONC_DEC";
    TITRANT_CONC_EXP_KEY: constant String := "TITRANT_CONC_EXP";
    --
    SAMPLE_VOLUME_INT_KEY: constant String := "SAMPLE_VOLUME_INT";
    SAMPLE_VOLUME_DEC_KEY: constant String := "SAMPLE_VOLUME_DEC";
    SAMPLE_VOLUME_EXP_KEY: constant String := "SAMPLE_VOLUME_EXP";
    --
    PKX_TYPE_KEY: constant String := "PKX_TYPE";
      PKX_TYPE_ACID: constant String := "pKa";
      PKX_TYPE_BASE: constant String := "pKb";
    PKX1_INT_KEY: constant String := "PKX1_INT";
    PKX1_DEC_KEY: constant String := "PKX1_DEC";
    PKX2_INT_KEY: constant String := "PKX2_INT";
    PKX2_DEC_KEY: constant String := "PKX2_DEC";
    --
    --
    PH_START_KEY_INT_KEY: constant String := "PH_START_INT";
    PH_START_KEY_DEC_KEY: constant String := "PH_START_DEC";
    PH_START_KEY_EXP_KEY: constant String := "PH_START_EXP";
    --
    PH_FIRST_HALF_INT_KEY: constant String := "PH_FIRST_HALF_INT";
    PH_FIRST_HALF_DEC_KEY: constant String := "PH_FIRST_HALF_DEC";
    PH_FIRST_HALF_EXP_KEY: constant String := "PH_FIRST_HALF_EXP";
    --
    PH_FIRST_EQUIV_INT_KEY: constant String := "PH_FIRST_EQUIV_INT";
    PH_FIRST_EQUIV_DEC_KEY: constant String := "PH_FIRST_EQUIV_DEC";
    PH_FIRST_EQUIV_EXP_KEY: constant String := "PH_FIRST_EQUIV_EXP";
    --
    PH_SECOND_HALF_INT_KEY: constant String := "PH_SECOND_HALF_INT";
    PH_SECOND_HALF_DEC_KEY: constant String := "PH_SECOND_HALF_DEC";
    PH_SECOND_HALF_EXP_KEY: constant String := "PH_SECOND_HALF_EXP";
    --
    PH_SECOND_EQUIV_INT_KEY: constant String := "PH_SECOND_EQUIV_INT";
    PH_SECOND_EQUIV_DEC_KEY: constant String := "PH_SECOND_EQUIV_DEC";
    PH_SECOND_EQUIV_EXP_KEY: constant String := "PH_SECOND_EQUIV_EXP";
    --
    PH_OVER_EQUIV_INT_KEY: constant String := "PH_OVER_EQUIV_INT";
    PH_OVER_EQUIV_DEC_KEY: constant String := "PH_OVER_EQUIV_DEC";
    PH_OVER_EQUIV_EXP_KEY: constant String := "PH_OVER_EQUIV_EXP";
    --
    --
    ANSWER_PH_START_KEY: constant String := "ANSWER_PH_START";
    ANSWER_VOLUME_START_KEY: constant String := "ANSWER_VOLUME_START";
    ANSWER_PH_FIRST_HALF_KEY: constant String := "ANSWER_PH_FIRST_HALF";
    ANSWER_VOLUME_FIRST_HALF_KEY: constant String := "ANSWER_VOLUME_FIRST_HALF";
    ANSWER_PH_FIRST_EQUIV_KEY: constant String := "ANSWER_PH_FIRST_EQUIV";
    ANSWER_VOLUME_FIRST_EQUIV_KEY: constant String := "ANSWER_VOLUME_FIRST_EQUIV";
    ANSWER_PH_SECOND_HALF_KEY: constant String := "ANSWER_PH_SECOND_HALF";
    ANSWER_VOLUME_SECOND_HALF_KEY: constant String := "ANSWER_VOLUME_SECOND_HALF";
    ANSWER_PH_SECOND_EQUIV_KEY: constant String := "ANSWER_PH_SECOND_EQUIV";
    ANSWER_VOLUME_SECOND_EQUIV_KEY: constant String := "ANSWER_VOLUME_SECOND_EQUIV";
    ANSWER_PH_OVER_SECOND_EQUIV_KEY: constant String := "ANSWER_PH_OVER_SECOND_EQUIV";
    ANSWER_VOLUME_OVER_SECOND_EQUIV_KEY: constant String := "ANSWER_VOLUME_OVER_SECOND_EQUIV";
    --
    TITRATION_CURVE_IMAGE_CLASS_KEY: constant String := "TITRATION_CURVE_IMAGE_CLASS";
      TITRATION_CURVE_IMAGE_CLASS_NODISP: constant String := "hidden_image";
      TITRATION_CURVE_IMAGE_CLASS_NORM: constant String := "titration_curve_image";
    TITRATION_CURVE_IMAGE_PATH_KEY: constant String := "TITRATION_CURVE_IMAGE_PATH";
    --
    FILLIN_1_PH_KEY: constant String := "FILLIN_1_PH";
    FILLIN_2_PH_KEY: constant String := "FILLIN_2_PH";
    FILLIN_2_VOL_KEY: constant String := "FILLIN_2_VOL";
    FILLIN_3_PH_KEY: constant String := "FILLIN_3_PH";
    FILLIN_3_VOL_KEY: constant String := "FILLIN_3_VOL";
    FILLIN_4_PH_KEY: constant String := "FILLIN_4_PH";
    FILLIN_4_VOL_KEY: constant String := "FILLIN_4_VOL";
    FILLIN_5_PH_KEY: constant String := "FILLIN_5_PH";
    FILLIN_5_VOL_KEY: constant String := "FILLIN_5_VOL";
    FILLIN_6_PH_KEY: constant String := "FILLIN_6_PH";
    FILLIN_6_VOL_KEY: constant String := "FILLIN_6_VOL";

  end Titration_Curve_Suite;

end Problem_Generator_Syswides;
