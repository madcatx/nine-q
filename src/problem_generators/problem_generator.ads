with Global_Types;
with Problem_Generator_Syswides;
with Ada.Containers.Indefinite_Doubly_Linked_Lists;
with Ada.Finalization;
with Ada.Strings.Unbounded;
with Cairo;
with Glib;

use Global_Types;
use Problem_Generator_Syswides;
package Problem_Generator is
  type Chem_Problem is abstract limited new Ada.Finalization.Limited_Controlled with private;

  function Create return access Chem_Problem is abstract;
  function Check_Answer(Problem: in out Chem_Problem; Answer: in Answer_Info.Map; FillIns: in out FillIns_Map.Map; Message: out UB_Text) return Answer_RetCode is abstract;
  function Get_Assignment(Problem: in out Chem_Problem; Assignment: in out Assignment_Info.Map) return RetCode is abstract;
  function Get_Parameters(Problem: in out Chem_Problem; Parameters: out Parameters_Info.Map) return RetCode is abstract;
  function Get_Walkthrough(Problem: in out Chem_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode is abstract;
  procedure New_Problem(Problem: in out Chem_Problem) is abstract;
  function Set_Parameters(Problem: in out Chem_Problem; Parameters: in Parameters_Info.Map) return RetCode is abstract;
  procedure Set_Resource_Prefix(Problem: in out Chem_Problem; Prefix: in String);
  --
  function Get_Problem(P_Type: in Problem_Type) return access Chem_Problem'Class;

private
  function TeX_To_PNG(TeXCode: in UB_Text; Filename: in String; Resource_Prefix: in Ada.Strings.Unbounded.Unbounded_String) return RetCode;

  package Resource_List is new Ada.Containers.Indefinite_Doubly_Linked_Lists(Element_Type => String);
  type Chem_Problem is abstract limited new Ada.Finalization.Limited_Controlled with
    record
      Resource_Prefix: Ada.Strings.Unbounded.Unbounded_String;
      TRL: Resource_List.List;
      Walkthrough_Generated: Boolean;
      Walkthrough_Stored: Problem_Generator_Syswides.Walkthrough_Info.Map;
    end record;
  procedure Add_Tracked_Resource(Problem: in out Chem_Problem; Name: in String);
  procedure Free_Tracked_Resources(Problem: in out Chem_Problem);
  procedure Finalize(Problem: in out Chem_Problem);

  WT_F_EXTENSION: constant String := ".png";
  WALKTHROUGH_TEMPLATES_PATH: constant String := "walkthrough_templates/";

  package Acidobazic_Suite is
    use Problem_Generator_Syswides.Acidobazic_Suite;

    type Acidobazic_Problem is new Problem_Generator.Chem_Problem with private;
      -- Constructor
      function Create return access Acidobazic_Problem;
      -- Inherited
      function Check_Answer(Problem: in out Acidobazic_Problem; Answer: in Answer_Info.Map; FillIns: in out FillIns_Map.Map; Message: out UB_Text) return Answer_RetCode;
      procedure New_Problem(Problem: in out Acidobazic_Problem);
      function Get_Assignment(Problem: in out Acidobazic_Problem; Assignment: in out Assignment_Info.Map) return RetCode;
      function Get_Parameters(Problem: in out Acidobazic_Problem; Parameters: out Parameters_Info.Map) return RetCode;
      function Get_Walkthrough(Problem: in out Acidobazic_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode;
      function Set_Parameters(Problem: in out Acidobazic_Problem; Parameters: in Parameters_Info.Map) return RetCode;

    private
      type pH_Float is digits 15;
      -- Present dissociation constant as pKa or pKb?
      type Dissociation_Constant_Type is (pKa, pKb);
      -- Is the substance acidic or basic?
      type Substance_Type is (Acid, Base);

      type Acidobazic_Parameters is
	record
	  No_Both_Simplifications: Boolean;
	end record;

      function Autoprotolysis_Limit(Kx: in pH_Float) return pH_Float;
      procedure Calculate_Concentration_Limits(Min: out pH_Float; Max: out pH_Float; Kx: in pH_Float; S: in Simplification);
      function Calculate_Solution(Problem: in Acidobazic_Problem) return pH_Float;
      function Correct_Down(Num: in pH_Float) return pH_Float;
      function Correct_Up(Num: in pH_Float) return pH_Float;
      function Correction_Exponent(Num: in pH_Float) return pH_Float;
      function Dissociation_Limit(Kx: in pH_Float) return pH_Float;
      function pX_To_X(pX: in pH_Float) return pH_Float;
      function Random_cX(Min: in pH_Float; Max: in pH_Float) return pH_Float;
      function Random_Kx return pH_Float;
      function Random_Simplification(Kx: in pH_Float; No_Both_Simplifications: in Boolean) return Simplification;
      function X_To_pX(X: in pH_Float) return pH_Float;
      function Walkthrough_Calculate_Concentration(Problem: in Acidobazic_Problem) return pH_Float;
      function Walkthrough_Calculate_Concentration_With_Dissoc(Problem: in Acidobazic_Problem) return pH_Float;
      function Walkthrough_Check_Autoprotolysis(Problem: in Acidobazic_Problem) return pH_Float;
      function Walkthrough_Check_Dissociation(Problem: in Acidobazic_Problem) return pH_Float;

      type Acidobazic_Problem is new Problem_Generator.Chem_Problem with
	record
	  Answer: pH_Float;
	  cX: pH_Float;
	  Kx: pH_Float;
	  Parameters: Acidobazic_Parameters;
	  Simpl: Simplification;
	  Subst_Type: Substance_Type;
	end record;

      Acidic_Min_pH: constant pH_Float := 1.75;
      Acidic_Max_pH: constant pH_Float := 5.75;
      Basic_Min_pH: constant pH_Float := 9.5;
      Basic_Max_pH: constant pH_Float := 12.5;
      K_W: constant pH_Float := 1.0E-14;
      -- Maximum concentration can be only 1.5 mol/dm3 higher than minimum concentration
      MAX_CONCENTRATION_DIFF: constant pH_Float := 0.75;
      CONCENTRATION_HARD_MIN_LIMIT: constant pH_Float := 1.0E-6;
      DECIMALS: constant Natural := 3;
      --
      CHECK_ATPR_INT_KEY: constant String := "CHECK_ATPR_INT";
      CHECK_ATPR_DEC_KEY: constant String := "CHECK_ATPR_DEC";
      CHECK_ATPR_EXP_KEY: constant String := "CHECK_ATPR_EXP";
      CHECK_DISSOC_INT_KEY: constant String := "CHECK_DISSOC_INT";
      CHECK_DISSOC_DEC_KEY: constant String := "CHECK_DISSOC_DEC";
      CHECK_DISSOC_EXP_KEY: constant String := "CHECK_DISSOC_EXP";
      CONC_ION_INT_KEY: constant String := "CONC_ION_INT";
      CONC_ION_DEC_KEY: constant String := "CONC_ION_DEC";
      CONC_ION_EXP_KEY: constant String := "CONC_ION_EXP";
      CONC_SUBST_INT_KEY: constant String := "CONC_SUBST_INT";
      CONC_SUBST_DEC_KEY: constant String := "CONC_SUBST_DEC";
      CONC_SUBST_EXP_KEY: constant String := "CONC_SUBST_EXP";
      KX_INT_KEY: constant String := "KX_INT";
      KX_DEC_KEY: constant String := "KX_DEC";
      KX_EXP_KEY: constant String := "KX_EXP";
      WT_ANSWER_PH_INT_KEY: constant String := "WT_ANSWER_PH_INT";
      WT_ANSWER_PH_DEC_KEY: constant String := "WT_ANSWER_PH_DEC";
      --
      WT_F_RESULT: constant String := "result";
      WT_F_ION_CONC: constant String := "ion_conc";
      WT_F_CHECK_ATPR: constant String := "check_atpr";
      WT_F_CHECK_DISSOC: constant String := "check_dissoc";
      WT_F_WITH_DISSOC: constant String := "with_dissoc";
      --
      WT_T_PREFIX: constant String := WALKTHROUGH_TEMPLATES_PATH & "acidobazic_suite/";

  end Acidobazic_Suite;

  package Solubility_Suite is
    use Problem_Generator_Syswides.Solubility_Suite;

    type Solubility_Problem is new Problem_Generator.Chem_Problem with private;
    -- Constructor
    function Create return access Solubility_Problem;
    -- Inherited
    function Check_Answer(Problem: in out Solubility_Problem; Answer: in Answer_Info.Map; FillIns: in out FillIns_Map.Map; Message: out UB_Text) return Answer_RetCode;
    procedure New_Problem(Problem: in out Solubility_Problem);
    function Get_Assignment(Problem: in out Solubility_Problem; Assignment: in out Assignment_Info.Map) return RetCode;
    function Get_Parameters(Problem: in out Solubility_Problem; Parameters: out Parameters_Info.Map) return RetCode;
    function Get_Walkthrough(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode;
    function Set_Parameters(Problem: in out Solubility_Problem; Parameters: in Parameters_Info.Map) return RetCode;

  private
    type SS_Float is digits 17;
    subtype Stochiometric_Count is Positive range 1 .. 5;

    type Ion is
      record
	Concentration: SS_Float;
	Charge: SS_Float;
      end record;
    type Ion_List is Array (Positive range <>) of Ion;

    type Solubility_Parameters is
      record
	Ionic_Strength: Boolean;	-- Correct for ionic strength in calculations
	P_Subtype: Problem_Subtype;
      end record;

    type Solubility_Problem_Data(Option: Problem_Subtype := V_FROM_G_KS) is
      record
	M: SS_Float;	-- Number of cations
	N: SS_Float;	-- Number of anions
	case Option is
	  when V_FROM_G_KS =>
	    V_G: SS_Float;  -- Sample mass in grams
	    V_Ks: SS_Float; -- Ks constant
	    V_MW: SS_Float; -- Molar mass of the sample
	  when KS_FROM_G_V =>
	    Ks_G: SS_Float;  -- Sample mass in grams
	    Ks_MW: SS_Float; -- Molar mass of the sample
	    Ks_V: SS_Float;  -- Sample volume in dm3
	  when C_FROM_KS_DIFFERENT_IONS | C_FROM_KS_SHARED_ION =>
	    C_EC: SS_Float; -- Concentration of the other electrolyte in the solution
	    C_Ks: SS_Float; -- Ks constant
	end case;
      end record;

    type Solubility_Problem is new Problem_Generator.Chem_Problem with
      record
	Parameters: Solubility_Parameters;  -- Parameters of the generator
	Prob_Data: Solubility_Problem_Data; -- Data which make the assignment
	Answer_Num: SS_Float;		    -- Answer to the problem
      end record;

    function Calculate_Activity_Coefficient(Charge: in SS_Float; Ionic_Strength: in SS_Float) return SS_Float;
    function Calculate_Ionic_Strength(Ions: in Ion_List) return SS_Float;
    function Calculate_Sat_Concentration(Cation_Count: in SS_Float; Anion_Count: in SS_Float; Ks: in SS_Float;
					 Cation_Gamma: in SS_Float; Anion_Gamma: in SS_Float) return SS_Float;
    function Calculate_Sat_Concentration_Different(Prob_Data: in Solubility_Problem_Data; Ionic_Strength: in Boolean) return SS_Float;
    function Calculate_Sat_Concentration_Shared(Prob_Data: in Solubility_Problem_Data; Ionic_Strength: in Boolean) return SS_Float;
    function Calculate_Sample_Volume(Prob_Data: in Solubility_Problem_Data; Ionic_Strength: in Boolean) return SS_Float;
    function Calculate_Solubility_Product(Prob_Data: in Solubility_Problem_Data; Ionic_Strength: in Boolean) return SS_Float;
    function Gen_Walkthrough_V_FROM_G_KS(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode;
    function Gen_Walkthrough_KS_FROM_G_V(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode;
    function Gen_Walkthrough_C_FROM_KS_DIFFERENT_IONS(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode;
    function Gen_Walkthrough_C_FROM_KS_SHARED_ION(Problem: in out Solubility_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode;
    function Generate_Electrolyte_Concentration return SS_Float;
    function Generate_Sample_Volume return SS_Float;
    function Generate_Solubility_Product return SS_Float;
    procedure Get_Precise_Answer_String(Message: in out UB_Text; P_Subtype: in Problem_Subtype; Answer: in SS_Float);

    ELECTROLYTE_CONCENTRATION_LOG_MAX: constant SS_Float := -2.0;
    ELECTROLYTE_CONCENTRATION_LOG_MIN: constant SS_Float := -4.5;
    MOLAR_WEIGHT_MAX: constant SS_Float := 450.0;
    MOLAR_WEIGHT_MIN: constant SS_Float := 45.0;
    PKS_MAX: constant SS_Float := 54.0;
    PKS_MIN: constant SS_Float := 10.0;
    SAMPLE_VOLUME_LOG_MAX: constant SS_Float := 10.0;
    SAMPLE_VOLUME_LOG_MIN: constant SS_Float := -0.3;
    SAMPLE_WEIGHT_MAX: constant SS_Float := 1.5;
    SAMPLE_WEIGHT_MIN: constant SS_Float := 0.1;
    DECIMALS: constant Natural := 3;
    DECIMALS_MW: constant Natural := 2;
    PRECISION: constant SS_Float := 1.0E2;
    --
    WT_T_PREFIX: constant String := WALKTHROUGH_TEMPLATES_PATH & "solubility_suite/";
    WT_F_ANSWER: constant String := "answer";
    WT_F_V_DISS_C: constant String := "v_diss_c";
    WT_F_V_PLUG_C: constant String := "v_plug_c";
    WT_F_V_PRE_RADEX: constant String := "v_pre_radex";
    WT_F_V_PLUG_VOLUME: constant String := "v_plug_volume";
    --
    WT_DC_INT_KEY: constant String := "WT_DC_INT";
    WT_DC_DEC_KEY: constant String := "WT_DC_DEC";
    WT_DC_EXP_KEY: constant String := "WT_DC_EXP";
    WT_G_INT_KEY: constant String := "WT_G_INT";
    WT_G_DEC_KEY: constant String := "WT_G_DEC";
    WT_KDMN_INT_KEY: constant String := "WT_KDMN_INT";
    WT_KDMN_DEC_KEY: constant String := "WT_KDMN_DEC";
    WT_KDMN_EXP_KEY: constant String := "WT_KDMN_EXP";
    WT_KS_INT_KEY: constant String := "WT_KS_INT";
    WT_KS_DEC_KEY: constant String := "WT_KS_DEC";
    WT_KS_EXP_KEY: constant String := "WT_KS_EXP";
    WT_M_KEY: constant String := "WT_M";
    WT_N_KEY: constant String := "WT_N";
    WT_MPN_KEY: constant String := "WT_MPN";
    WT_MW_INT_KEY: constant String := "WT_MW_INT";
    WT_MW_DEC_KEY: constant String := "WT_MW_DEC";
    WT_RES_INT_KEY: constant String := "WT_RES_INT";
    WT_RES_DEC_KEY: constant String := "WT_RES_DEC";
    WT_RES_EXP_KEY: constant String := "WT_RES_EXP";

  end Solubility_Suite;

  package Titration_Curve_Suite is
    use Problem_Generator_Syswides.Titration_Curve_Suite;
    use Glib;

    type Titration_Curve_Problem is new Problem_Generator.Chem_Problem with private;
    -- Constructor
    function Create return access Titration_Curve_Problem;
    -- Inherited
    function Check_Answer(Problem: in out Titration_Curve_Problem; Answer: in Answer_Info.Map; FillIns: in out FillIns_Map.Map; Message: out UB_Text) return Answer_RetCode;
    function Get_Assignment(Problem: in out Titration_Curve_Problem; Assignment: in out Assignment_Info.Map) return RetCode;
    function Get_Parameters(Problem: in out Titration_Curve_Problem; Parameters: out Parameters_Info.Map) return RetCode;
    function Get_Walkthrough(Problem: in out Titration_Curve_Problem; Walkthrough: out Walkthrough_Info.Map) return RetCode;
    procedure New_Problem(Problem: in out Titration_Curve_Problem);
    function Set_Parameters(Problem: in out Titration_Curve_Problem; Parameters: in Parameters_Info.Map) return RetCode;

  private
    Math_Limitation: exception;
    type T_Float is digits 15;

    type Titration_Curve_Problem is new Problem_Generator.Chem_Problem with
      record
	SType: Sample_Type;
	pKx1: T_Float;
	pKx2: T_Float;
	Sample_Volume: T_Float;
	Sample_Concentration: T_Float;
	T_Concentration: T_Float;
      end record;

    function Calculate_1st_Diff_Acid(cA: in T_Float; Ka1: in T_Float; Ka2: in T_Float; cB: in T_Float; Kb: in T_Float; Xn: in T_Float) return T_Float;
    function Calculate_2nd_Diff_Acid(cA: in T_Float; Ka1: in T_Float; Ka2: in T_Float; cB: in T_Float; Kb: in T_Float; Xn: in T_Float) return T_Float;
    function Calculate_1st_Diff_Base(cB: in T_Float; Kb1: in T_Float; Kb2: in T_Float; cA: in T_Float; Ka: in T_Float; Xn: in T_Float) return T_Float;
    function Calculate_2nd_Diff_Base(cB: in T_Float; Kb1: in T_Float; Kb2: in T_Float; cA: in T_Float; Ka: in T_Float; Xn: in T_Float) return T_Float;
    function Calculate_Full_Acid(cA: in T_Float; Ka1: in T_Float; Ka2: in T_Float; cB: in T_Float; Kb: in T_Float; Xn: in T_Float) return T_Float;
    function Calculate_Full_Base(cB: in T_Float; Kb1: in T_Float; Kb2: in T_Float; cA: in T_Float; Ka: in T_Float; Xn: in T_Float) return T_Float;
    function Calculate_Target_End_Value(Num: in T_Float) return T_Float;
    procedure Draw_Chart_Circle(Ctx: in out Cairo.Cairo_Context; X_Raw: in T_Float; X_Range_Raw: in T_Float; Y_Raw: in T_Float; Y_Range_Raw: in T_Float := 14.0);
    procedure Draw_Chart_Crosshair(Ctx: in out Cairo.Cairo_Context; X_Raw: in T_Float; X_Range_Raw: in T_Float; Y_Raw: in T_Float; Y_Range_Raw: in T_Float := 14.0);
    procedure Draw_Titration_Curve(Ctx: in out Cairo.Cairo_Context; SType: in Sample_Type; Kx1: in T_Float; Kx2: in T_Float; c0_Sample: in T_Float; c0_TS: in T_Float; V_Sample: in T_Float; V_Final: in T_Float; First_Guess: in T_Float);
    procedure Draw_Titration_Curve_Point(Ctx: in out Cairo.Cairo_Context; X_Raw: in T_Float; Y_Raw: in T_Float; X_Range_Raw: in T_Float; Y_Range_Raw: in T_Float := 14.0);
    procedure Prepare_Chart(Ctx: in out Cairo.Cairo_Context; V_Final: in T_Float);
    function Solve_Halleys_Approximation(SType: in Sample_Type; c_Sample: in T_Float; Kx1: in T_Float; Kx2: in T_Float; c_TS: in T_Float; Kz: in T_Float; First_Guess: in T_Float) return T_Float;

    MIN_PKX_1: constant T_Float := 2.0;
    MAX_PKX_1: constant T_Float := 4.2;
    MAX_PKX_2: constant T_Float := 8.3;
    PKX_STEP: constant T_Float := 4.0;
    MIN_VOLUME_ML: constant T_Float := 5.0;
    MAX_VOLUME_ML: constant T_Float := 60.0;
    MIN_SAMPLE_CONCENTRATION: constant T_Float := 1.0E-3;
    MAX_SAMPLE_CONCENTRATION: constant T_Float := 1.0;
    --
    TITR_ACID_KA: constant T_Float := 0.0001; -- Corresponds to HCl
    --TITR_BASE_KB: constant T_Float := 0.631; -- Corresponds to NaOH
    TITR_BASE_KB: constant T_Float := 0.0001; -- Corresponds to NaOH
    KW: constant T_Float := 1.0E-14;
    --
    DECIMALS: constant Natural := 3;
    MAX_DIFFERENCE: constant := 3.0E-14;
    --
    IMAGE_WIDTH: constant Glib.GInt := 800;
    IMAGE_HEIGHT: constant Glib.GInt := 800;
    IMAGE_RIGHT_BORDER_WIDTH: constant Glib.GDouble := 50.0;
    IMAGE_BOTTOM_BORDER_HEIGHT: constant Glib.GDouble := 55.0;
    X_OFFSET: constant Glib.GDouble := 45.0;
    Y_OFFSET: constant Glib.GDouble := 60.0;
    IMAGE_CHART_WIDTH: constant Glib.GDouble := Glib.GDouble(IMAGE_WIDTH) - X_OFFSET - IMAGE_RIGHT_BORDER_WIDTH;
    IMAGE_CHART_HEIGHT: constant Glib.GDouble := Glib.GDouble(IMAGE_HEIGHT) - Y_OFFSET - IMAGE_BOTTOM_BORDER_HEIGHT;
    CROSSHAIR_LENGTH: constant Glib.GDouble := 12.0;
    BIG_TICK_LENGTH: constant Glib.GDouble := 8.0;
    BIG_TICK_X_MIN_STEP: constant Glib.GDouble := 40.0;
    AXIS_LINE_THICKNESS: constant Glib.GDouble := 3.0;
    CROSSHAIR_THICKNESS: constant Glib.GDouble := 1.0;
    TC_LINE_THICKNESS: constant Glib.GDouble := 2.0;
    FONT_SIZE: constant Glib.GDouble := 18.0;
    TEXT_SPACE: constant Glib.GDouble := 5.0;
    --
    LEGEND_Y_COORD: constant Glib.GDouble := 30.0;
    LEGEND_Y_TEXT_COORD: constant Glib.GDouble := 38.0;
    LEGEND_X_USER_COORD: constant Glib.GDouble := 100.0;
    LEGEND_X_CALCD_COORD: constant Glib.GDouble := 500.0;
    LEGEND_MARK_TEXT_X_OFFSET: constant Glib.GDouble := 30.0;
    --
    X_AXIS_UNITS_TEXT: constant String := "V (odměrný roztok) [mL]";
    Y_AXIS_UNITS_TEXT: constant String := "pH";
    --
    TITRATION_CURVE_FILENAME: constant String := "curve.png";

  end Titration_Curve_Suite;


end Problem_Generator;
