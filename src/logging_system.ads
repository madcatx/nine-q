with Ada.Finalization;
with Ada.Text_IO;

with Global_Types;

use Global_Types;
package Logging_System is
  Logger_Not_Initialized: exception;
  type Log_Level is (DEBUG, WARNING, ERROR);

  procedure Close;
  function Initialize return RetCode;
  procedure Log(Message: in String; Level: in Log_Level; To_Console: in Boolean := False);

private
  type Logger is limited new Ada.Finalization.Limited_Controlled with
    record
      File_Handle: Ada.Text_IO.File_Type;
    end record;
  overriding procedure Finalize(This: in out Logger);
  type Logger_All_Access is access all Logger;

  protected Logging_Internal is
    procedure Delete_Logger;
    procedure Initialize(Ret: out RetCode);
    procedure Log(Message: in String; Level: in Log_Level; To_Console: in Boolean := False);

  private
    The_Logger: Logger_All_Access;
  end Logging_Internal;

  LOG_FILE_NAME: constant String := "./nine_q_log.log";

end Logging_System;
