with Global_Types;
with Problem_Generator;
with Problem_Generator_Syswides;
with Ada.Containers.Ordered_Maps;
with AWS.Session;

use Global_Types;
package Problem_Manager is

  type Problem_Category is (Invalid, Acidobazic, Solubility, Titration_Curve);

  function Display_Checked_Answer(UID: in Unique_ID; Answer: in Problem_Generator_Syswides.Answer_Info.Map; HTML: out HTML_Code;
				  Pr_ID: in Problem_ID) return RetCode;
  function Display_Assignment(UID: in Unique_ID; HTML: out HTML_Code) return RetCode;
  function Display_Walkthrough(UID: in Unique_ID; HTML: out HTML_Code; Pr_ID: in Problem_ID) return RetCode;
  --function Display_Next_Assignment(UID: in Unique_ID;
  --				   Problem_Parameters: in Problem_Generator_Syswides.Parameters_Info.Map;
  --				   HTML: out HTML_Code) return Boolean;
  function Get_UID(Raw_UID: in String; UID: out Unique_ID) return Boolean;
  function Prepare_Problem(UID: in Unique_ID; Raw_P_Cat: in String;
			   Parameters: in Problem_Generator_Syswides.Parameters_Info.Map := Problem_Generator_Syswides.Parameters_Info.Empty_Map) return RetCode;
  function Register_UID(UID: out Unique_ID) return RetCode;
  procedure Session_Expired(SID: AWS.Session.ID);

private
  type Chem_Problem_All_Access is access all Problem_Generator.Chem_Problem'Class;
  procedure Free_Chem_Problem(Problem: in out Chem_Problem_All_Access);

  type Stored_Problem is
    record
      Mutex: Simple_Mutex;
      Problem: Chem_Problem_All_Access;
      Category: Problem_Category;
    end record;
  type Stored_Problem_All_Access is access all Stored_Problem;
  procedure Free_Stored_Problem(Problem: in out Stored_Problem_All_Access);

  function Build_Resource_Prefix(UID: in Unique_ID; Pr_Cat: in Problem_Category; Pr_ID: in Problem_ID) return String;

  package Problem_Storage is new Ada.Containers.Ordered_Maps(Key_Type => Problem_ID, Element_Type => Stored_Problem_All_Access);

  type User_Session_Data is
    record
      Problems: Problem_Storage.Map;
      Last_Problem_ID: Problem_ID := Problem_ID'First;
    end record;
  type User_Session_All_Access is access all User_Session_Data;

  ERRMSG_GET_ASSIGNMENT: constant String := "Unable to get assignment";
  ERRMSG_GET_PARAMETERS: constant String := "Unable to get problem parameters";
  ERRMSG_UNHANDLED_EXCEPTION: constant String := "Unhandled exception occured";

  package Session_Keeping is new Ada.Containers.Ordered_Maps(Key_Type => Unique_ID, Element_Type => User_Session_All_Access);

  protected Active_Sessions is
    procedure Add_Problem(UID: in Unique_ID; Problem: in Stored_Problem_All_Access; Pr_ID: out Problem_ID; Ret: out RetCode);
    function Contains(UID: in Unique_ID) return Boolean;
    function Get_Problem(UID: in Unique_ID; Pr_ID: in Problem_ID) return Stored_Problem_All_Access;
    function Get_Latest_Problem_With_ID(UID: in Unique_ID; Pr_ID: out Problem_ID) return Stored_Problem_All_Access;
    procedure Register_UID(UID: out Unique_ID; Ret: out RetCode);
    procedure Remove_Session(UID: in Unique_ID);

  private
    procedure Check_Free_And_Register(UID: in Unique_ID; Ret: out RetCode; Stop: out Boolean);
    procedure Free_Session_Data(Data: in out User_Session_All_Access);

    Sessions: Session_Keeping.Map;
    Last_UID: Unique_ID := Unique_ID'First;
  end Active_Sessions;

  MAX_STORED_PROBLEMS: Problem_ID := 20;
end Problem_Manager;
