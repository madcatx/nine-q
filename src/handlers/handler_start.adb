with AWS.Messages;
with AWS.MIME;
with AWS.Response;
with AWS.Session;
with AWS.Status;
with Face_Generator_Static;
with Global_Types;
with Problem_Manager;

use Global_Types;
package body Handler_Start is

  function Handle(Request: AWS.Status.Data) return AWS.Response.Data is
    SID: constant AWS.Session.ID := AWS.Status.Session(Request);
    Req_Type: constant AWS.Status.Request_Method := AWS.Status.Method(Request);

    Raw_UID: constant String := AWS.Session.Get(SID, "UID");
    HTML: HTML_Code;
  begin

    case Req_Type is
      when AWS.Status.GET =>
	declare
	  Raw_Problem_Category: constant String := AWS.Status.Parameter(Request, "problem_category");
	  UID: Unique_ID;
	  Ret: RetCode;
	  Success: Boolean;
	begin
	  -- Check that the problem category is not empty
	  if Raw_Problem_Category'Length = 0 then
	    return AWS.Response.URL(Location => "/");	    
	  end if;

	  -- Register new UID if necessary and create a first problem
	  Success := Problem_Manager.Get_UID(Raw_UID, UID);
	  if Success = False then
	    -- UID stored within this session is not valid, register a new one
	    Ret := Problem_Manager.Register_UID(UID);
	    if Ret /= OK then
	      -- UID could not have been registered
	      -- TODO: Print some sensible error message, for now just redirect to index
	      return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
					Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Error_UID_Registration),
					Status_Code => AWS.Messages.S200);
	    end if;
	    -- Save the new UID
	    AWS.Session.Set(SID, "UID", Unique_ID'Image(UID));
	    AWS.Session.Set_Callback(Problem_Manager.Session_Expired'Access);
	  end if;

	  -- We're all set, create a new problem
	  Ret := Problem_Manager.Prepare_Problem(UID, Raw_Problem_Category);
	  if Ret /= OK then
	    -- Something went wrong when generating the problem
	    -- TODO: Print some sensible error message, for now just redirect to index
	    return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				      Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Error_Prepare_Problem),
				      Status_Code => AWS.Messages.S200);
	  end if;

	  Ret := Problem_Manager.Display_Assignment(UID, HTML);
	  if Ret /= OK then
	    return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				      Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Error_Display_Assignment),
				      Status_Code => AWS.Messages.S200);
	  end if;
	  return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				    Message_Body => HTML_To_Fixed_String(HTML),
				    Status_Code => AWS.Messages.S200);
	end;
      when others =>
	return AWS.Response.URL(Location => "/");
    end case;
  end Handle;

  function Callback return AWS.Dispatchers.Callback.Handler is
  begin
    return AWS.Dispatchers.Callback.Create(Handle'Access);
  end Callback;

end Handler_Start;
