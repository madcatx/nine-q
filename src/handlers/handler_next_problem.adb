with AWS.Containers.Tables;
with AWS.Messages;
with AWS.MIME;
with AWS.Parameters;
with AWS.Response;
with AWS.Session;
with AWS.Status;

with Ada.Text_IO;

with Face_Generator_Static;
with Global_Types;
with Problem_Generator_Syswides;
with Problem_Manager;

use Global_Types;

package body Handler_Next_Problem is

  function Handle(Request: AWS.Status.Data) return AWS.Response.Data is
    SID: constant AWS.Session.ID := AWS.Status.Session(Request);

    HTML: HTML_Code;
    Raw_UID: constant String := AWS.Session.Get(SID, "UID");
    Req_Method : constant AWS.Status.Request_Method := AWS.Status.Method(Request);
    UID: Unique_ID;
  begin
    case Req_Method is
      when AWS.Status.POST =>
	declare
	  use Problem_Generator_Syswides;
	  use Problem_Generator_Syswides.Parameters_Info;

	  Problem_Parameters: Parameters_Info.Map;
	  POST_Data: constant AWS.Parameters.List := AWS.Status.Parameters(Request);
	  Ret: RetCode;
	  Success: Boolean;
	begin
	  -- Get UID
	  Success := Problem_Manager.Get_UID(Raw_UID, UID);
	  if Success = False then
	    -- This UID is invalid, redirect to index
	    return AWS.Response.URL(Location => "/");
	  end if;

	  -- UID OK, read problem parameters
	  for Idx in 1 .. POST_Data.Count loop
	    declare
	      C: Parameters_Info.Cursor;
	      E: constant AWS.Containers.Tables.Element := POST_Data.Get(Idx);
	      Success: Boolean;
	    begin
	      Problem_Parameters.Insert(E.Name, E.Value, C, Success);
	      -- TODO: Handle error
	    end;
	  end loop;

	  -- Create a new problem
	  if Problem_Parameters.Find(Problem_Generator_Syswides.RESERVED_PROBLEM_CATEGORY_KEY) = Parameters_Info.No_Element then
	    return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				      Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Error_Problem_Category),
				      Status_Code => AWS.Messages.S200);
	  end if;

	  declare
	    Raw_P_Cat: constant String := Problem_Parameters.Element(Problem_Generator_Syswides.RESERVED_PROBLEM_CATEGORY_KEY);
	  begin
	    Ret := Problem_Manager.Prepare_Problem(UID, Raw_P_Cat, Problem_Parameters);
	    if Ret /= OK then
	      return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
					Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Error_Prepare_Problem),
					Status_Code => AWS.Messages.S200);
	    end if;
	    -- Display new problem
	    Ret := Problem_Manager.Display_Assignment(UID, HTML);
	    if Ret /= OK then
	      return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
					Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Error_Display_Assignment),
					Status_Code => AWS.Messages.S200);
	    end if;

	    return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				      Message_Body => HTML_To_Fixed_String(HTML),
				      Status_Code => AWS.Messages.S200);
	  end;
	end;
      when others =>
	return AWS.Response.URL(Location => "/");
    end case;
  end Handle;

  function Callback return AWS.Dispatchers.Callback.Handler is
  begin
    return AWS.Dispatchers.Callback.Create(Handle'Access);
  end Callback;

end Handler_Next_Problem;
