with Handler_Check_Answer;
with Handler_Default;
with Handler_Images;
with Handler_Next_Problem;
with Handler_Resources;
with Handler_Show_Walkthrough;
with Handler_Start;
with Handler_Static;
with Handler_Styles;

package body Handlers is
  function Get_Dispatchers return AWS.Services.Dispatchers.URI.Handler is
    Handler: AWS.Services.Dispatchers.URI.Handler;
  begin
    Handler.Register_Default_Callback(Action => Handler_Default.Callback);
    Handler.Register(URI => "/check_answer",
		     Action => Handler_Check_Answer.Callback);
    Handler.Register(URI => "/images",
		     Action => Handler_Images.Callback,
		     Prefix => True);
    Handler.Register(URI => "/next_problem",
		     Action => Handler_Next_Problem.Callback);
    Handler.Register(URI => "/start",
		     Action => Handler_Start.Callback);
    Handler.Register(URI => "/static",
		     Action => Handler_Static.Callback,
		     Prefix => True);
    Handler.Register(URI => "/main_stylesheet",
		     Action => Handler_Styles.Main_Callback);
    Handler.Register(URI => "/resources",
		     Action => Handler_Resources.Callback,
		     Prefix => True);
    Handler.Register(URI => "/show_walkthrough",
		     Action => Handler_Show_Walkthrough.Callback);

    return Handler;
  end Get_Dispatchers;
end Handlers;
