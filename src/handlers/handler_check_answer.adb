with AWS.Containers.Tables;
with AWS.Messages;
with AWS.MIME;
with AWS.Parameters;
with AWS.Response;
with AWS.Session;
with AWS.Status;

with Face_Generator_Static;
with Global_Types;
with Problem_Manager;
with Problem_Generator_Syswides;

use Global_Types;
package body Handler_Check_Answer is

  function Handle(Request: AWS.Status.Data) return AWS.Response.Data is
    SID: constant AWS.Session.ID := AWS.Status.Session(Request);
    Req_Type: constant AWS.Status.Request_Method := AWS.Status.Method(Request);

    Raw_UID: constant String := AWS.Session.Get(SID, "UID");
    UID: Unique_ID;
    HTML: HTML_Code;
  begin
    case Req_Type is
      when AWS.Status.POST =>
	declare
	  use Problem_Generator_Syswides;
	  use Problem_Generator_Syswides.Answer_Info;

	  POST_Data: constant AWS.Parameters.List := AWS.Status.Parameters(Request);
	  Answer: Answer_Info.Map;
	  Pr_ID: Problem_ID;
	  Ret: RetCode;
	  Success: Boolean;
	begin
	  -- Get UID
	  Success := Problem_Manager.Get_UID(Raw_UID, UID);
	  if Success = False then
	    -- This UID is invalid, redirect to index
	    return AWS.Response.URL(Location => "/");
	  end if;

	  for Idx in 1 .. POST_Data.Count loop
	    declare
	      C: Answer_Info.Cursor;
	      E: constant AWS.Containers.Tables.Element := POST_Data.Get(Idx);
	      Success: Boolean;
	    begin
	      Answer.Insert(E.Name, E.Value, C, Success);
	      -- TODO: Handle error
	    end;
	  end loop;

	  -- Get problem ID
	  if Answer.Find(Problem_Generator_Syswides.RESERVED_PROBLEM_ID_KEY) = Answer_Info.No_Element then
	    return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				      Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Error_Problem_ID),
				      Status_Code => AWS.Messages.S200);
	  end if;

	  begin
	    Pr_ID := Problem_ID'Value(Answer.Element(Problem_Generator_Syswides.RESERVED_PROBLEM_ID_KEY));
	  exception
	    when Constraint_Error =>
	      return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
					Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Error_Problem_ID),
					Status_Code => AWS.Messages.S200);
	  end;

	 Ret := Problem_Manager.Display_Checked_Answer(UID, Answer, HTML, Pr_ID);
	  if Ret /= OK then
	    return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				      Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Error_Display_Answer),
				      Status_Code => AWS.Messages.S200);
	  end if;

	  return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				    Message_Body => HTML_To_Fixed_String(HTML),
				    Status_Code => AWS.Messages.S200);
	end;
      when others =>
	return AWS.Response.URL(Location => "/");
    end case;
  end Handle;

  function Callback return AWS.Dispatchers.Callback.Handler is
  begin
    return AWS.Dispatchers.Callback.Create(Handle'Access);
  end Callback;

end Handler_Check_Answer;
