with AWS.Messages;
with AWS.MIME;
with AWS.Response;
with AWS.Status;
with Face_Generator;
with Global_Types;

use Global_Types;
package body Handler_Default is

  function Handle(Request: AWS.Status.Data) return AWS.Response.Data is
    HTML: HTML_Code;
    Ret: RetCode;
  begin
    Ret := Face_Generator.Generate_Index_Face(HTML);
    if Ret /= OK then
      return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				Message_Body => "Internal server error occured, we're sorry about that...",
				Status_Code => AWS.Messages.S503);
    else
      return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				Message_Body => HTML_To_Fixed_String(HTML),
				Status_Code => AWS.Messages.S200);
    end if;
  end Handle;

  function Callback return AWS.Dispatchers.Callback.Handler is
  begin
    return AWS.Dispatchers.Callback.Create(Handle'Access);
  end Callback;

end Handler_Default;
