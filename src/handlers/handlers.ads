with AWS.Services.Dispatchers.URI;

package Handlers is
  function Get_Dispatchers return AWS.Services.Dispatchers.URI.Handler;
end Handlers;