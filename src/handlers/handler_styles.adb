with AWS.Messages;
with AWS.MIME;
with AWS.Templates;
with AWS.Response;
with AWS.Status;

package body Handler_Styles is

  function Main_Handle(Request: AWS.Status.Data) return AWS.Response.Data is
  begin
    return AWS.Response.Build(Content_Type => AWS.MIME.Text_CSS,
			      Message_Body => AWS.Templates.Parse(Filename => "styles/main.css", Cached => True),
			      Status_Code => AWS.Messages.S200);
  end Main_Handle;

  function Main_Callback return AWS.Dispatchers.Callback.Handler is
  begin
    return AWS.Dispatchers.Callback.Create(Main_Handle'Access);
  end Main_Callback;

end Handler_Styles;
