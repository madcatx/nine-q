with Ada.Strings.Fixed;
with AWS.Messages;
with AWS.MIME;
with AWS.Response;
with AWS.Status;
with Ada.Text_IO;
with Face_Generator_Static;
with Global_Types;

use Global_Types;
package body Handler_Static is

  function File_Exists(Path: in String) return Boolean is
    File: Ada.Text_IO.File_Type;
  begin
    begin
      Ada.Text_IO.Open(File => File, Mode => Ada.Text_IO.In_File, Name => Path);
      Ada.Text_IO.Close(File);
      return True;
    exception
      when Ada.Text_IO.Name_Error =>
	return False;
    end;
  end File_Exists;

  function Handle(Request: AWS.Status.Data) return AWS.Response.Data is
    use Ada.Strings.Fixed;

    URI: constant String := AWS.Status.URI(Request);
    Idx: Positive;
  begin
    Idx := Index(Source => URI, Pattern => "/", From => URI'Last, Going => Ada.Strings.Backward);
    declare
      File_Path: constant String := "templates/static/" & URI(Idx + 1 .. URI'Last);
    begin
      if File_Exists(File_Path) = False then
	return AWS.Response.URL(Location => "/");
      else
	return AWS.Response.Build(Content_Type => AWS.MIME.Text_HTML,
				  Message_Body => HTML_To_Fixed_String(Face_Generator_Static.Display_Static_Page(File_Path)),
				  Status_Code => AWS.Messages.S200);
      end if;
    end;
  end Handle;

  function Callback return AWS.Dispatchers.Callback.Handler is
  begin
    return AWS.Dispatchers.Callback.Create(Handle'Access);
  end Callback;

end Handler_Static;
