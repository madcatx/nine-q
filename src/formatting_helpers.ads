with Global_Types;

use Global_Types;
generic
type FH_Float is digits <>;
package Formatting_Helpers is

  function String_To_Float(S: in String) return FH_Float;
  function Round_To_Valid_Nums(Num: in FH_Float; Decimals: Natural) return FH_Float;
  procedure Split_Integer_Decimal_Unscaled_Strs(Num: in FH_Float; Decimals: in Natural; Integer_Part: out UB_Text; Decimal_Part: out UB_Text);
  procedure Split_Integer_Decimal_Exponent_Nums(Num: in FH_Float; Decimals: in Natural; Integer_Part: out Integer; Decimal_Part: out FH_Float;
						Exponent_Part: out Integer);
  procedure Split_Integer_Decimal_Exponent_Strs(Num: in FH_Float; Decimals: in Natural; Integer_Part: out UB_Text; Decimal_Part: out UB_Text;
					        Exponent_Part: out UB_Text);

private
  function Get_Integer_Part(Num: in out FH_Float; Decimals: in Natural) return FH_Float;
  procedure Prepend_Zeros_To_Text(Num: in FH_Float; Decimals: in Natural; Text: in out UB_Text);

end Formatting_Helpers;
