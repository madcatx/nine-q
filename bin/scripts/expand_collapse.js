
<script type="text/javascript">

function show_hide_item(element_id, button_id, disp_style, show_icon, hide_icon)
{ 
  elem = document.getElementById(element_id);
  btn = document.getElementById(button_id);

  if (elem.style.display == 'none') {
    elem.style.display = disp_style;
    btn.src = hide_icon;
  } else {
    elem.style.display = 'none';
    btn.src = show_icon;
  }
}

</script>

